﻿using UnityEngine;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using LitJson;

/* HCAnimatedBanner Version1.1
 * 
 * 変更履歴
 * 2015/10/14
 *  - スクリプト名変更
 *  - アニメーション機能追加
 *  
 * Hiroki Miyada
 */

[RequireComponent(typeof(RawImage))]
public class HCAnimatedBanner : MonoBehaviour {

    [SerializeField]
    [Header("【データリソースアドレス】")]
    private string jsonLink = "";

    public class AnimBannerData
    {
        public string textureIdName;
        public string textureUrl;
        public int textureWidth;
        public int textureHeight;
        public int spriteWidth;
        public int spriteHeight;
        public int spriteCount;
        public double animWaitTime;
        public string transitionUrlAndroid;
        public string transitionUrlIOS;
        public double waitForNextAnimSec;
    }
    private List<AnimBannerData> bannerDataList;

    private const string HCAwakeCount = "HCAwakeCount";
    private const string HCCurrentIndex = "HCCurrentIndex";

    //General
    private string textureIdName;
    private string textureUrl;

    // ----- TransitionSettings -----
    private string _transitionLink;
    public string transitionLink
    {
        get
        {
            if (string.IsNullOrEmpty(_transitionLink))
            {
#if UNITY_ANDROID
                _transitionLink = defaultTLinkAndroid;
#elif UNITY_IOS
                _transitionLink = defaultTLinkIOS;
#endif
            }
            return _transitionLink;
        }
        private set
        {
            _transitionLink = value;
        }
    }

    [Header("【遷移先の初期値】")]
    [SerializeField] private string defaultTLinkAndroid = "https://play.google.com/store/apps/developer?id=Hautecouture+Inc.";
    [SerializeField] private string defaultTLinkIOS = "https://itunes.apple.com/jp/developer/hautecouture-inc./id901721933";

    // ----- InternalProperty -----
    private string texturePath;
    private double count;

    IEnumerator jCoroutine, lCoroutine;

    private GameObject thisObject;
  
    // ---------- Method -------------
    void Awake()
    {
        GetComponent<RawImage>().color = Color.clear;

        //コルーチンの登録.
        jCoroutine = LoadJsonData();
        lCoroutine = LoadTexture();
    }



    //オブジェクト有効化に合わせてコルーチン開始.
    void OnEnable()
    {
       

        if (bannerDataList == null)
        {
            StartCoroutine(jCoroutine);
            Debug.Log("AAAAAAAAAAAAAAAA");
        }
        else
        {
            StartCoroutine(lCoroutine);
            Debug.Log("ssssssssssssss");
        }
    }
    //オブジェクト無効化に合わせてコルーチン停止.
    void OnDisable()
    {
        if (bannerDataList == null)
        {
            StopCoroutine(jCoroutine);
        }
        else
        {
            StopCoroutine(lCoroutine);
        }
    }

    IEnumerator LoadJsonData()
    {
        using (WWW www = new WWW(jsonLink))
        {
            yield return www;
            if (www.error == null)
            {
                bannerDataList = JsonMapper
                    .ToObject<AnimBannerData[]>(www.text)
                    .ToList();
            }
        }

        if(bannerDataList == null) yield break;

        StartCoroutine(lCoroutine);
    }

    IEnumerator LoadTexture()
    {
        //初期化.
        int index = PlayerPrefs.GetInt(HCCurrentIndex, 0);
        if (index >= bannerDataList.Count) index = 0;

        //参照情報の切り替え.
        textureIdName = bannerDataList[index].textureIdName;
        textureUrl = bannerDataList[index].textureUrl;

        //新規テクスチャの適用.
        texturePath = Application.persistentDataPath + "/" + textureIdName;
        int textureWidth = bannerDataList[index].textureWidth;
        int textureHeight = bannerDataList[index].textureHeight;
        Texture2D texture = new Texture2D(textureWidth, textureHeight);

        //ネットワーク接続時はテクスチャダウンロード.
        do
        {
            if (File.Exists(texturePath)) break;
            if (Application.internetReachability == NetworkReachability.NotReachable) break;
            using (WWW www = new WWW(textureUrl))
            {
                yield return www;
                if (www.error == null)
                {
                    File.WriteAllBytes(texturePath, www.bytes);
                }
            }
        } while (false);

        yield return new WaitForSeconds(0.2f);

        if(!File.Exists(texturePath))
        {
            yield break;
        }
        else
        {
            byte[] imageBytes = File.ReadAllBytes(texturePath);
            Texture2D tex2D = new Texture2D(textureWidth, textureHeight);

            if (tex2D.LoadImage(imageBytes))
            {
                texture = tex2D;
            }
            //読み取りエラーが起こっているので処理を抜ける.
            else
            {
                yield break;
            }
        }

        ApplyTexture(texture);

        //遷移先の変更.
#if UNITY_ANDROID
        transitionLink = bannerDataList[index].transitionUrlAndroid;
#elif UNITY_IOS
        transitionLink = bannerDataList[index].transitionUrlIOS;
#endif
        //バナーをアニメーションさせる.
        StartCoroutine(AnimateBanner(index));

        //時間指定の場合.
        count = bannerDataList[index].waitForNextAnimSec;
        yield return new WaitForSeconds((float)count);
        StopCoroutine(AnimateBanner(index));
        if (++index >= bannerDataList.Count) index = 0;
        PlayerPrefs.SetInt(HCCurrentIndex, index);

        //要素が1つ以上かつ、時間指定の場合は再帰的に呼び出す.
        if (bannerDataList.Count >= 2)
            StartCoroutine(LoadTexture());
    }

    /// <summary>
    /// 読み込んだテクスチャを元に
    /// </summary>
    /// <returns></returns>
    IEnumerator AnimateBanner(int index)
    {
        //元のテクスチャの幅、高さ.
        int textureWidth = bannerDataList[index].textureWidth;
        int textureHeight = bannerDataList[index].textureHeight;
        //スプライト一枚の幅、高さ.
        int spriteWidth = bannerDataList[index].spriteWidth;
        int spriteHeight = bannerDataList[index].spriteHeight;
        //スプライトの数.
        int horCount = Mathf.FloorToInt(textureWidth / spriteWidth);
        int verCount = Mathf.FloorToInt(textureHeight / spriteHeight);
        int spriteCount = bannerDataList[index].spriteCount;

        Rect[] spriteRects = new Rect[bannerDataList[index].spriteCount];
        for (int y = 0; y < verCount; y++){
            for (int x = 0; x < horCount; x++)
            {
                if(x  + horCount * y > spriteCount - 1) break;
                spriteRects[x + horCount * y] = new Rect(x * spriteWidth, textureHeight - (y + 1) * spriteHeight, spriteWidth, spriteHeight);
            }
        }

        var uvRects = spriteRects
            .Select(s => new Rect(
                s.x / textureWidth, s.y / textureHeight,
                s.width / textureWidth, s.height / textureHeight))
                .ToArray();

        var rImage = GetComponent<RawImage>();
        int rIndex = 0;
        float wait = (float)bannerDataList[index].animWaitTime;
        while(true)
        {
            rImage.uvRect = uvRects[rIndex];
            if (++rIndex > spriteRects.Length - 1) rIndex = 0;
            yield return new WaitForSeconds(wait);
        }
    }

    void ApplyTexture(Texture2D texture)
    {
        var rImage = GetComponent<RawImage>();
        if (rImage.color.a == 0)
        {
            rImage.color = Color.white;
        }
        if (rImage)
        {
            rImage.texture = texture;
            return;
        }
    }

    public void OpenTransitionURL()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
            Application.OpenURL(transitionLink);
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}
