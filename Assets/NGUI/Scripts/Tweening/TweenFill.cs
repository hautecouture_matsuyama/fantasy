﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2014 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Tween the object's fill. Works with both UI widgets as well as renderers.
/// </summary>

[AddComponentMenu("NGUI/Tween/Tween Fill")]
public class TweenFill : UITweener
{
	[Range(0f, 1f)] public float from = 0f;
	[Range(0f, 1f)] public float to = 1f;
	
	bool mCached = false;
	UIRect mRect;
	Material mMat;
	SpriteRenderer mSr;

	UIBasicSprite muSprite;
	
	[System.Obsolete("Use 'value' instead")]
	public float fill { get { return this.value; } set { this.value = value; } }
	
	void Cache ()
	{
		mCached = true;
		mRect = GetComponent<UIRect>();
		mSr = GetComponent<SpriteRenderer>();
		muSprite = GetComponent<UIBasicSprite>();
		
		if (mRect == null && mSr == null)
		{
			Renderer ren = GetComponent<Renderer>();
			if (ren != null) mMat = ren.material;
			if (mMat == null) mRect = GetComponentInChildren<UIRect>();
		}
	}
	
	/// <summary>
	/// Tween's current value.
	/// </summary>
	
	public float value
	{
		get
		{
			if (!mCached) Cache();
			if (muSprite != null) return muSprite.fillAmount;
			if (mRect != null) return mRect.alpha;
			if (mSr != null) return mSr.color.a;
			return mMat != null ? mMat.color.a : 1f;
		}
		set
		{
			if (!mCached) Cache();
			if(muSprite != null) 
			{
				muSprite.fillAmount = value;
				return;
			}
			if (mRect != null)
			{
				//mRect.alpha = value;
				mRect.alpha  = value;
			}
			else if (mSr != null)
			{
				Color c = mSr.color;
				c.a = value;
				mSr.color = c;
			}
			else if (mMat != null)
			{
				Color c = mMat.color;
				c.a = value;
				mMat.color = c;
			}
		}
	}
	
	/// <summary>
	/// Tween the value.
	/// </summary>
	
	protected override void OnUpdate (float factor, bool isFinished) { value = Mathf.Lerp(from, to, factor); }
	
	/// <summary>
	/// Start the tweening operation.
	/// </summary>
	
	static public TweenFill Begin (GameObject go, float duration, float fill)
	{
		TweenFill comp = UITweener.Begin<TweenFill>(go, duration);
		comp.from = comp.value;
		comp.to = fill;
		
		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}
	
	public override void SetStartToCurrentValue () { from = value; }
	public override void SetEndToCurrentValue () { to = value; }
}