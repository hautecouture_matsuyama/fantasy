﻿using UnityEngine;
using System.Collections;

public class Account : MonoBehaviour {

    [SerializeField]
    private GameObject CreateAccoutCanvas;

    private int cheak;

    void Awake()
    {
        //Editorか
        if (Debug.isDebugBuild)
            return;
        //ネット接続されているか
        if (Application.internetReachability == NetworkReachability.NotReachable)
            return;
        //アカウントは存在するか
        if (RankingManager.Instance.CheckAccount())
            return;
		
		StartCoroutine(ShowCanvas());
        
                
    }

    private IEnumerator ShowCanvas()
    {
        yield return new WaitForSeconds(0.5f);

        CreateAccoutCanvas.SetActive(true);

    }

}
