﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugBtn : MonoBehaviour {

    [SerializeField]
    private Text account;


    void Awake()
    {
        if(RankingManager.Instance.CheckAccount())
        {
            account.text = "登録済み";
        }

        else if (!RankingManager.Instance.CheckAccount())
        {
            account.text = "未登録";
        }

        else
        {
            account.text = "error";
        }
    }


	public void Score()
    {
        RankingManager.Instance.SendRanking(100);
    }

    public void Name()
    {
        RankingManager.Instance.SignUp("たけし");
    }

    public void Show()
    {
        RankingManager.Instance.ShowRanking();
    }
}
