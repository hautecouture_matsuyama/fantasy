﻿using UnityEngine;
using System.Collections;

public class HelpButton : MonoBehaviour {

    public GameObject HelpPanel;

    public void ShowHelpPanel()
    {
        HelpPanel.active = true;
    }

}
