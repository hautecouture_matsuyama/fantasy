﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;


public class EditOKButton : MonoBehaviour {

    [SerializeField]
    private Text nameText;
    
    [SerializeField]
    private GameObject CreateNameCanvas;


    public void CreateAccount()
    {
        string strAfter = "";

        //入力されているか？
        if (nameText.text == string.Empty)
            return;


        if (nameText.text.Length > 5) strAfter = nameText.text.Remove(5);

        else strAfter = nameText.text;

        //ユーザー名登録
        RankingManager.Instance.SignUp(strAfter);

        CreateNameCanvas.SetActive(false);
    }

    

    public static bool IsKana(string target)
    {
        foreach (var chara in target)
        {
            var charaData = Encoding.UTF8.GetBytes(chara.ToString());

            if (charaData.Length != 3)
            {
                return false;
            }

            var charaInt = (charaData[0] << 16) + (charaData[1] << 8) + charaData[2];

            if (charaInt < 0xe38181 || charaInt > 0xe38293)
            {
                return false;
            }
        }

        return true;
    }


}
