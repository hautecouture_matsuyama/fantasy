﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class RankingButton : MonoBehaviour {

    public GameObject RankingCanvas;


    public void ShowRanking()
    {
        RankingManager.Instance.GetRankingData();
        RankingCanvas.SetActive(true);
    }

	
}
