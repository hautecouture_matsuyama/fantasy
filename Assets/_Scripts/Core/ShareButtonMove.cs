﻿using UnityEngine;
using System.Collections;

public class ShareButtonMove : MonoBehaviour {
    public Vector3 moveToPosition;
    public Vector3 returnToPosition;

    void OnEnable()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash(
            "isLocal", true,
            "position", moveToPosition,
            "time", 0.2,
            //"oncomplete", "complete",
            //"oncompletetarget", this.gameObject,
            "easeType", "linear"
        ));
    }

    public void ReturnPosition()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash(
            "isLocal", true,
            "position", returnToPosition,
            "time", 0.2,
            "oncomplete", "ReturnComplete",
            "oncompletetarget", this.gameObject,
            "easeType", "linear"
        ));
    }

    void ReturnComplete()
    {
        this.gameObject.SetActive(false);
    }
}
