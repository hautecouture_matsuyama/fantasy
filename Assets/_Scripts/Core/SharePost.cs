﻿using UnityEngine;
using System.Collections.Generic;
    
public class SharePost : MonoBehaviour 
{
    public Texture2D shareTexture;
    

    public void ShareTwitter()
    {
        string url = "https://play.google.com/store/apps/details?id=com.hautecouture.snow";
        //UM_ShareUtility.TwitterShare("FANTASY SWIPEで" + IngameManager.GetInstance().mI_Score + "フロアまで到達！\n" + url, shareTexture);
        Debug.Log(url);
    }

    public void ShareLINE()
    {
        string msg = "FANTASY SWIPEで" + IngameManager.GetInstance().mI_Score + "フロアまで到達！\n" + "https://play.google.com/store/apps/details?id=com.hautecouture.snow";
        string url = "http://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

    string FeedLink = "https://play.google.com/store/apps/details?id=com.hautecouture.snow";
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {

            FeedLinkName = "FANTASY SWIPEで ";
            FeedLinkDescription = IngameManager.GetInstance().mI_Score + "フロアまで到達！\n";

            FB.Feed(
                link: FeedLink,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
                picture: "http://100apps.s3.amazonaws.com/000_Original/mentori_jump_header.jpg",
                properties: FeedProperties
            );
        }
    }


    
}