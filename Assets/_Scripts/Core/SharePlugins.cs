﻿using UnityEngine;
using System.Collections;

public class SharePlugins : MonoBehaviour
{
    public SharePost _sharePost;

    public void OnClick()
    {
        if(gameObject.name=="share_facebook")
        {
            _sharePost.ShareFacebook();
        }

        if (gameObject.name == "share_line")
        {
            _sharePost.ShareLINE();
        }

        if (gameObject.name == "share_twitter")
        {
            _sharePost.ShareTwitter();
        }
    }

}
