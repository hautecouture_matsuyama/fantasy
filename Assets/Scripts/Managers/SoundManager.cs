﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GlobalData;

/*
 *  INSTRUCCIONES
 *  1 - Debe ser instanciado, no colocado en el mapa, ahorita puse su inicializacion en InitGame
 *  2 - Comente la linea que iniciaba con la cancion Map, mejor llamen manualmente la cancion que quieran.
 *  3 - La funcion ReinitializeAudioSources() no esta bien del todo ( en mi version quedo incompleta), asi que no destruye y crea los nuevos audio sources, mejor creen muchos varios 
 *      AudioSources desde un principio.
 * 
 * 
 *  
 *  NOTA : Un error que habia (que no se porque estaba ahi siendo la version 1.1 del codigo es que se mandaba a llamar l_AudioSource[n].aSource.audio.Play(),
 *         esto no se hace porque para eso hay varios audiosSources, si se usa el ".audio" se aloca. La forma correcta ( por lo menos en esta version de Unity + MonoDevelop es 
 *		   l_AudioSource[n].aSource.Play()
 *
 *  NOTA : Voy a comentar y describir lo que hace cada una de las funciones
 */



public class SoundManager : Singleton<SoundManager> 
{	
	protected SoundManager() {}

	//Only one audioclip can be played at the time, so a child Object is used to play simultaneous
	//private GameObject SoundPlayer = null; 
	
	//Multiple AudioSources in the same GameObject
	private int i_MusicSources    = 2;
	private int i_SoundsFXSources = 5;
	private int i_AudioSources    = 22;
	public  List<ChildSources> l_AudioSources = new List<ChildSources>();
	public struct ChildSources
	{
		public AudioSource aSource;
		public bool        inUse;
	};

	
	public void InitInstance()
	{
		/* Multiple audiosources from the same gameobject approach */ 
		int totalSources  = CONSTANTS.MusicSources + CONSTANTS.SFX_Sources;
		i_MusicSources    = CONSTANTS.MusicSources;
		i_SoundsFXSources = CONSTANTS.SFX_Sources;
		for (int i = 0; i < totalSources; i++) 
		{
			this.gameObject.AddComponent<AudioSource>();
		}
		this.gameObject.AddComponent<AudioListener> ();	
		AddAudioSources ();
	}

	void AddAudioSources()
	{
		//Load from json the list of the music and sound for each level
		//load from resources the corresponding sounds and music
		//load last volume configurations
		AudioSource[] tempList = this.gameObject.GetComponents<AudioSource> ();
		for (int i = 0; i < tempList.Length; i++) 
		{
			ChildSources cS;
			tempList[i].playOnAwake = false;
			cS.aSource = tempList[i];
			cS.inUse = false;
			l_AudioSources.Add (cS);
		}
		//SoundPlayer = this.gameObject.transform.GetChild (0).gameObject;
		dicMusic = new Dictionary<string,AudioClip>();
		dicSounds = new Dictionary<string,AudioClip>();
		
		Dictionary<string,AudioClip> l_music = new Dictionary<string,AudioClip> ();
		Dictionary<string,AudioClip> l_sound = new Dictionary<string,AudioClip> ();
		Dictionary<string,string>    l_keys  = new Dictionary<string, string>();
		JSONDefinitionManager.LoadAudioClipsJSON("Audio",out l_music, out l_sound,out l_keys);
		dicMusic = l_music;
		dicSounds = l_sound;

		KeysOfAudio = l_keys;
	}
	
	Dictionary<string,AudioClip>	dicMusic;
	Dictionary<string,AudioClip>	  dicSounds;

	Dictionary<string,string> KeysOfAudio = new Dictionary<string, string>();

	public bool isMusicPaused
	{
		get;
		private set;
	}
	public bool isSFXPaused
	{
		get;
		private set;
	}

	public bool isMusicStoped
	{
		get;
		private set;
	}
	public bool isSFXStoped
	{
		get;
		private set;
	}
	
	public float  musicVolume = 100.0f;
	public float  soundsVolume = 100.0f;
	
	void Start () 
	{
		//Load from json the list of the music and sound for each level
		//load from resources the corresponding sounds and music
		//load last volume configurations
		/*
		AudioSource[] tempList = this.gameObject.GetComponents<AudioSource> ();
		for (int i = 0; i < tempList.Length; i++) 
		{
			ChildSources cS;
			cS.aSource = tempList[i];
			cS.inUse = false;
			l_AudioSources.Add (cS);
		}
		//SoundPlayer = this.gameObject.transform.GetChild (0).gameObject;
		dicMusic = new Dictionary<string,AudioClip>();
		dicSounds = new Dictionary<string,AudioClip>();
		
		Dictionary<string,AudioClip> l_music = new Dictionary<string,AudioClip> ();
		Dictionary<string,AudioClip> l_sound = new Dictionary<string,AudioClip> ();
		JSONDefinitionManager.LoadAudioClipsJSON("Audio",out l_music, out l_sound);
		dicMusic = l_music;
		dicSounds = l_sound;
		//this.PlayMusic ("Map");
		*/
	}



/**
Destroys and creates newAudioSources ( NOT FUNCTIONAL )
@PARAMS
int num_Audios = New total AudioSources created
int MusicChan  = New dedicated Music Channels
int SoundChan  = New dedicated Sound Channels
 */
	public void ReinitializeAudioSources(int num_Audios,int MusicChan,int SoundChan)
	{
		//Stop all Audios
		for (int k = 0; k < l_AudioSources.Count; k++) 
		{
			l_AudioSources[k].aSource.GetComponent<AudioSource>().Stop();
		}
		SetMusicChannels (MusicChan);
		SetSoundsFXChannels (SoundChan);
		AudioSource[] tempList = this.gameObject.GetComponents<AudioSource> ();
		int numOfAudioSources = tempList.Length;
		//Destroy AudioSources in the GameObject and clear the list
		if (l_AudioSources.Count > 0) 
		{
			for (int j= 0; j < numOfAudioSources; j++) 
			{
				Destroy (tempList [j]);
			}
			l_AudioSources.Clear ();
			//tempList.Clear();
		}
		//Create new AudioSourcesComponents
		for (int i = 0; i < num_Audios; i++) 
		{
			this.gameObject.AddComponent<AudioSource>();
		}
		tempList = this.gameObject.GetComponents<AudioSource>();
		//Include them in the Manager
		for(int i = 0; i < num_Audios; i++) 
		{
			ChildSources cS;
			cS.aSource = tempList[i];
			cS.inUse = false;
			l_AudioSources.Add (cS);
		}
	}

/**
Adds a Sound to the library 
@PARAMS
string name    = Name of the new Audio
AudioClip clip = Audio clip to be added
 */ 
	public void AddSound(string name,AudioClip clip)
	{	dicSounds.Add (name, clip);}

/**
Adds a Song/Music to the library 
@PARAMS
string name    = Name of the new Audio
AudioClip clip = Audio clip to be added
 */ 
	public void AddMusic(string name,AudioClip clip)
	{	dicMusic.Add (name, clip);}

/**
Sets the Music Channels
@PARAMS
	int channels = New audio Channels
 */ 
	public void SetMusicChannels(int channels)
	{	i_MusicSources = channels;}

/**
Sets the Sounds/FX Channels
@PARAMS
	int channels = New audio Channels
*/
	public void SetSoundsFXChannels(int channels)
	{	i_SoundsFXSources = channels;}

/**
Resumes the Music Channels if in use
NOTE: If the Music was paused, it is going to resume where it paused,
			if they were stoped, they will being again.
 */ 
	public void ResumeMusic()
	{
		isMusicPaused = false;
		for (int i = 0; i < i_MusicSources; i++) 
		{
			if(l_AudioSources[i].inUse)
			{	
				print("Audio Source "+i+" Resumed");
				l_AudioSources[i].aSource.Play();
			}
			else
			{

			}
		}
	}

/**
Resumes the Sounds/FX Channels if in use
NOTE: If the Sounds/FX were paused, they are going to resume where they paused,
			if they were stoped, they will being again.
 */ 
	public void ResumeSounds()
	{
		isSFXPaused = false;
		for (int i = 0; i < i_MusicSources; i++) 
		{
			if(l_AudioSources[i+i_MusicSources].inUse)
			{	
				print("Audio Source "+(i+i_MusicSources)+" Resumed");
				l_AudioSources[i+i_MusicSources].aSource.Play();
			}
		}
	}
/**Stops a specific Sound
 * PARAMS
 * string soundToStop = the name of the sound that its wanted to be stoped
 */ 
	public void StopSound(string soundToStop)
	{
		string sound = KeysOfAudio[soundToStop];
		for(int i = 0; i < i_SoundsFXSources ; i++)
		{
			if(l_AudioSources[i+i_MusicSources].inUse &&
			   l_AudioSources[i+i_MusicSources].aSource.clip.name == sound)//soundToStop
			{
				l_AudioSources[i+i_MusicSources].aSource.Stop();
				ChildSources cS;
				cS.inUse = false;
				cS.aSource = l_AudioSources[i+i_MusicSources].aSource;
				l_AudioSources [i+i_MusicSources] = cS;
				return;
			}
		}
		Debug.LogWarning("Sound ( "+soundToStop+" ) Couldnt be Stoped because it wasnt found in Sounds AudioSources" );
	}
		
	/** Returns Bool if the Audio Name is currently being played
	 * PARAMS:
	 * string audio = Audio Name to check
	 */ 
	public bool isAudioPlaying(string audio)
	{
		string _sound = KeysOfAudio[audio];
		for(int i = 0 ; i < i_AudioSources;i++)
		{
			if(l_AudioSources[i].inUse && l_AudioSources[i].aSource.clip.name == _sound)
			{
				Debug.LogWarning("Audio is Being Played");
				return true;
			}
		}
		Debug.LogWarning("Audio wasnt Playing");
		return false;
	}


/**Stops a specific Music
 * PARAMS
 * string musicToStop = the name of the music that its wanted to be stoped
 */ 
	public void StopMusic(string musicToStop)
	{
		string music = KeysOfAudio[musicToStop];
		for(int i = 0; i < i_SoundsFXSources ; i++)
		{
			if(l_AudioSources[i].inUse &&
			   l_AudioSources[i].aSource.clip.name == music)//musicToStop
			{
				l_AudioSources[i].aSource.Stop();
				ChildSources cS;
				cS.inUse = false;
				cS.aSource = l_AudioSources[i].aSource;
				l_AudioSources [i] = cS;
				return;
			}
		}
		Debug.LogWarning("Music ( "+musicToStop+" ) Couldnt be Stoped because it wasnt found in Sounds AudioSources" );
	}

	public void PauseMusic(string musicToPause)
	{
		string music = KeysOfAudio[musicToPause];

		isMusicPaused = true;
		for (int i = 0; i < i_MusicSources; i++)
		{
			if(l_AudioSources[i].inUse && l_AudioSources[i].aSource.clip.name == music)
			{
				print("AudioSource "+i+" Paused");
				l_AudioSources[i].aSource.Pause();
			}
		}
	}

/**
Plays a specific Sound
@PARAMS
string name = name for the desired sound to be played
boool  loop = Loop the sound
 */ 
	public void PlaySound(string name,bool _loop = false,float pitch = 1f)
	{
		for (int i = 0; i < i_SoundsFXSources; i++) 
		{
			if(!l_AudioSources[i+i_MusicSources].inUse)//i_SoundsFXSources
			{
				ChildSources cS;
				cS.inUse = true;
				cS.aSource = l_AudioSources[i+i_MusicSources].aSource;
				l_AudioSources[i+i_MusicSources] = cS;
				l_AudioSources[i+i_MusicSources].aSource.clip = dicSounds[name];
				l_AudioSources[i+i_MusicSources].aSource.pitch = pitch;
				l_AudioSources[i+i_MusicSources].aSource.loop = _loop;
				if(!_loop)
					StartCoroutine(CoroutineAudioSourceRestart( dicSounds[name].length ,i+i_MusicSources));
				l_AudioSources[i+i_MusicSources].aSource.Play();
				return;
			}
		}
		//Debug.Log ("No audioSource avaible SOUND");
	}

/**
Plays a specific Song/Music
@PARAMS
string name = name for the desired song to be played
boool  loop = Loop the music
 */ 
	public void PlayMusic(string name , bool _loop = false)
	{
		for (int i = 0; i < i_MusicSources; i++) 
		{
				if(!l_AudioSources[i].inUse)
				{
					ChildSources cS;
					cS.inUse = true;
					cS.aSource = l_AudioSources[i].aSource;
					l_AudioSources[i] = cS;
					l_AudioSources[i].aSource.clip = dicMusic[name];
					l_AudioSources[i].aSource.loop = _loop;
					if( !_loop )
						StartCoroutine(CoroutineAudioSourceRestart( dicMusic[name].length,i));
					l_AudioSources[i].aSource.Play();
					
					return;
				}
		}
		//Debug.Log ("No audioSource avaible MUSIC");
	}
	
/**
Stops the music completely, if the Sounds or Music is resumed, it will start from the beginning of the audioclip.
*/ 
	public void StopMusic()
	{
		isMusicPaused = true;
		for (int i = 0; i < i_MusicSources; i++) 
		{
			//print("Audio Source "+i+" Stoped");
			l_AudioSources [i].aSource.Stop ();

			ChildSources cS;
			cS.inUse = false;
			cS.aSource = l_AudioSources [i].aSource;
			cS.aSource.loop = false;
			l_AudioSources [i] = cS;
		}
	}

/**
Stops the sounds completely, if the Sounds or Music is resumed, it will start from the beginning of the audioclip.
And new clips can be set
*/
	public void StopSounds()
	{
		isSFXPaused = true;
		for (int i = 0; i < i_MusicSources; i++) //i_SoundsFXSources
		{
			print("Audio Source "+ (i+i_MusicSources) +" Stoped");
			l_AudioSources [i + i_MusicSources].aSource.Stop ();
			
			ChildSources cS;
			cS.inUse = false;
			cS.aSource = l_AudioSources [i + i_MusicSources].aSource;
			cS.aSource.loop = false;
			l_AudioSources [i + + i_MusicSources] = cS;
		}
	}
	
/**
Pauses the music, but if the music is resumed, it will continue where it paused
*/
	public void PauseMusic()
	{	
		isMusicPaused = true;
		for (int i = 0; i < i_MusicSources; i++)
		{
			if(l_AudioSources[i].inUse)
			{
				print("AudioSource "+i+" Paused");
				l_AudioSources [i].aSource.Pause ();
			}
		}
	}

/**
Pauses the sound, but if the sound is resumed, it will continue where it paused
*/
	public void PauseSound()
	{	
		isSFXPaused = true;
		for (int i = 0; i < i_SoundsFXSources; i++) 
		{
			if(l_AudioSources[i + i_MusicSources].inUse)
			{
				l_AudioSources [i + i_MusicSources].aSource.Pause ();//i_SoundsFXSources
			}
		}
		//SoundPlayer.audio.Pause ();
	}


	/// <summary>
	/// Fades in the music.
	/// </summary>
	/// <param name="name">Key of the AudioClip.</param>
	/// <param name="time">Time to Execute the fade in.</param>
	public void FadeInMusic(string name,float time = 1f,bool loop = false)
	{
		for (int i = 0; i < i_MusicSources; i++) 
		{
			if(!l_AudioSources[i].inUse)
			{
				ChildSources cS;
				cS.inUse = true;
				cS.aSource = l_AudioSources[i].aSource;
				l_AudioSources[i] = cS;
				l_AudioSources[i].aSource.clip = dicMusic[name];
				
				
				float from = 0f;
				float to   = musicVolume * 0.01f;
				
				if(time > dicMusic[name].length)
					time = dicMusic[name].length;
				
				float factor = Mathf.Lerp(from,to,time);
				l_AudioSources[i].aSource.volume = from;
				to *= 100f;
				StartCoroutine(CoroutineFadeInSound(i,factor,Time.fixedDeltaTime,to));
				if(!loop)
					StartCoroutine(CoroutineAudioSourceRestart( dicMusic[name].length ,i));
				l_AudioSources[i].aSource.loop = loop;
				l_AudioSources[i].aSource.Play();
				return;
			}
		}
	}

	/// <summary>
	/// Fades out the music.
	/// </summary>
	/// <param name="name">Name.</param>
	/// <param name="time">Time.</param>
	public void FadeOutMusic(string name,float time = 1f)
	{
		if(!isAudioPlaying(name))
		{
			Debug.LogWarning("The audio ( "+name+" )wasn playing so it cant be fade out");
			return;
		}
		for (int i = 0; i < i_MusicSources; i++) 
		{
			if(l_AudioSources[i].inUse && l_AudioSources[i].aSource.clip.name == KeysOfAudio[name])
			{
				float from = l_AudioSources[i].aSource.volume;
				float to   = 0f;
				
				if(time > dicMusic[name].length)
					time = dicMusic[name].length;
				
				float factor_f = from / ( time / Time.fixedDeltaTime );
				
				StartCoroutine(CoroutineFadeOut(i,factor_f,Time.fixedDeltaTime));
				return;
			}
		}
	}

/**Sets volume music from a scale of 0 to 1.
@PARAMS
float volume = Volume to be set ( it has to be on a scale of 0 to 100
*/
	public void SetMusicVolume(float volume)
	{
		musicVolume = volume * 0.01f;
		for (int i = 0; i < i_MusicSources; i++) 
		{
			l_AudioSources[i].aSource.volume = musicVolume;
		}
		//audio.volume = volume * 0.01f;
	}
/** Sets Sound Volume 
@PARAMS
float volume = Volume to be set ( it has to be from 0 to 100)
*/
	public void SetSoundVolume(float volume)
	{
		soundsVolume = volume * 0.01f;
		for (int i = 0; i < i_SoundsFXSources; i++) 
		{
			l_AudioSources[i + i_MusicSources].aSource.volume = soundsVolume;
		}
	}
	
	#region Coroutines
	IEnumerator CoroutineAudioSourceRestart(float time,int index)
	{
		yield return new WaitForSeconds(time);
		ChildSources cS;
		cS.inUse = false;
		cS.aSource = l_AudioSources [index].aSource;
		l_AudioSources [index] = cS;
	}


	
	IEnumerator CoroutineFadeOut(int index,float cFactor,float updateRate)
	{
		yield return new WaitForSeconds(updateRate);
		l_AudioSources[index].aSource.volume -= cFactor;
		
		if(l_AudioSources[index].aSource.volume <= 0f)
		{
			ChildSources cS;
			l_AudioSources[index].aSource.Stop();
			cS.inUse = false;
			cS.aSource = l_AudioSources[index].aSource;
			l_AudioSources[index] = cS;
			yield return true;
		}
		
		if(l_AudioSources[index].inUse)
		{
			StartCoroutine(CoroutineFadeOut(index,cFactor,updateRate) );
		}
	}

	IEnumerator CoroutineFadeInSound(int index,float cFactor,float updateRate,float To)
	{
		yield return new WaitForSeconds(updateRate);
		//Debug.LogWarning("Called FadeIn");
		l_AudioSources[index].aSource.volume += cFactor;
		
		if(l_AudioSources[index].inUse && l_AudioSources[index].aSource.volume < To)
		{
			//Debug.LogWarning("Called Coroutine");
			StartCoroutine(CoroutineFadeInSound(index,cFactor,updateRate,To) );
		}
		else
		{
			Debug.LogWarning("Fade In of AudioChild Source = "+index.ToString()+" Finished");
		}
	}
	#endregion

	#region CUSTOM
	public void StartMusicChange()
	{
		//120f
		StartCoroutine(ChangeMusicAfterTime(100f));
	}
	int Index=0;
	IEnumerator ChangeMusicAfterTime(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		Index++;
		int music = Index%2;
		switch(music)
		{
		case 0:
			FadeInMusic("MainTheme",3f,true);
			FadeOutMusic("ArcadeTheme",3f);
			break;
		case 1:
			FadeInMusic("ArcadeTheme",3f,true);
			FadeOutMusic("MainTheme",3f);
			break;
		}
		StartCoroutine(ChangeMusicAfterTime(waitTime));
	}


	#endregion
}

