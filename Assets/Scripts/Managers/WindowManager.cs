﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GlobalData;

public class WindowManager : Singleton<WindowManager> 
{
	protected WindowManager() {}

	public List<WindowBase> ml_WindowsList = new List<WindowBase>();

	public List<WindowBase> ml_ActiveWindows;

	private Dictionary<WINDOWSTYPES,WindowBase> md_Windows = new Dictionary<WINDOWSTYPES, WindowBase>();

	public static string Modal_MSG = string.Empty;
	public ModalWindow modalWindow;

	public static bool NoTweens = false;

	#region LOCALS_TO_PROJECT
	public PickCharacter_Window AllCharactersWindow;

	public TutorialManager TutorialWindow;
	#endregion

	public void Initialize()
	{
		ml_ActiveWindows = new List<WindowBase>();
		for(int it=0;it < ml_WindowsList.Count;it++)
		{
			if(md_Windows.ContainsKey( ml_WindowsList[it].myWindowType ) )
			{
				Debug.LogWarning("Window Type _"+ml_WindowsList[it].myWindowType.ToString()+"_ has already been added");
				continue;
			}
			ml_WindowsList[it].Initialize();
			md_Windows.Add(ml_WindowsList[it].myWindowType,ml_WindowsList[it]);
		}
	}

	/// <summary>
	/// Shows the window with its tweens.
	/// </summary>
	/// <param name="window">Window.</param>
	public void ShowWindow(WINDOWSTYPES window)
	{
		if(!md_Windows.ContainsKey(window))
		{
			Debug.LogError("Window _"+window.ToString()+"_ wasnt found");
			return;
		}
		if(ml_ActiveWindows.Contains(md_Windows[window]))
		{
			Debug.LogWarning("Window _"+window.ToString()+"_ is already shown");
			return;
		}
		ml_ActiveWindows.Add(md_Windows[window]);
		md_Windows[window].OnEnter();
	}

	/// <summary>
	/// Hides the window.
	/// </summary>
	/// <param name="window">Window.</param>
	public void HideWindow(WINDOWSTYPES window)
	{
		if(!md_Windows.ContainsKey(window))
		{
			Debug.LogError("Window _"+window.ToString()+"_ wasnt found");
			return;
		}
		if(!ml_ActiveWindows.Contains(md_Windows[window]))
		{
			Debug.LogWarning("Window _"+window.ToString()+"_ isnt shown");
			return;
		}
		ml_ActiveWindows.Remove(md_Windows[window]);
		md_Windows[window].OnExit();
	}

	#region LOCAL_FUNCTIONS_PROJECT
	#endregion
}
