﻿using UnityEngine;

public class DailyGiftManager : MonoBehaviour {

    const string DailyGiftKey = "DailyGiftTimestamp";

    public GameObject PrizeWheel;
	public float HoursBetweenPrizes;

	void Start ()
    {
        CheckAvailability();
	}

    void CheckAvailability()
    {
        if(!PlayerPrefs.HasKey(DailyGiftKey))
        {
            PopPrizeWheel();
        }
        else
        {
            var lastTimestamp = PlayerPrefs.GetString(DailyGiftKey);
            var lastDate = System.DateTime.Parse(lastTimestamp);
            var diffSpan = System.DateTime.Now - lastDate;
			if (diffSpan.TotalHours >= HoursBetweenPrizes)
            {
                PopPrizeWheel();
            }
            else
            {
                HidePrizeWheel();
            }
			Debug.Log("Diff span: " + diffSpan.TotalHours);
        }
    }

    void PopPrizeWheel()
    {
        PlayerPrefs.SetString(DailyGiftKey, System.DateTime.Now.ToString());
        PrizeWheel.SetActive(true);
    }

    void HidePrizeWheel()
    {
        PrizeWheel.SetActive(false);
    }
}
