﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GlobalData;

public class InputManager : Singleton<InputManager> 
{

	protected InputManager() {}
		
	//Enable sorting collision
	public bool EnableSortingLayerCollision;

	// Enable 2.5 or 3D
	public bool _3dHits;

	List<int> m_lSortingLayerID = new List<int>();
	List<int> m_lSortingOrderArray = new List<int>();
	int topSortingLayer = 0;
	//int indexOfTopSortingLayer = 0;
	int topSortingOrder = 0;
	int indexOfTopSortingOrder = 0;
	SpriteRenderer component2D = null;

	public List<string> SortingLayersID = new List<string>();
	public Dictionary<string,int> mD_IDS = new Dictionary<string, int>();

	enum UserAction
	{
		UA_INACTIVE,
		UA_TOUCHED,
		UA_DRAGGED,
		UA_DROPED
	};

	RaycastHit hit;
	RaycastHit2D hit2d;
	RaycastHit2D[] hits2d;

	RaycastHit[] hits;
	Ray rayTraced;
	Vector2 InputPosition;
	UserAction UserBehavior = UserAction.UA_INACTIVE;
		
	public void Initialize()
	{
		int it=0;
		foreach(string data in SortingLayersID)
		{
			mD_IDS.Add(data,it);
			it++;
		}
		if( Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsPlayer )
			return;
		else
		{
			#if UNITY_IPHONE || UNITY_ANDROID
			Input.multiTouchEnabled = true;
			#endif
		}
	}

	TimeSpan ts;
	DateTime TouchBeganTime;
	DateTime TouchEndTime; 
	bool Touching;

	// Update is called once per frame
	void Update( )
	{				
		//if(LevelManager.GetInstance().isPause)
		//	return;
		// in Editor Mouse Click
		if( Application.platform == RuntimePlatform.WindowsEditor || 
		   Application.platform == RuntimePlatform.OSXEditor || 
		   Application.platform == RuntimePlatform.WindowsPlayer )
		{
			if( Input.GetButtonDown("Fire1") )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_TOUCHED;
			}
			else if( Input.GetMouseButton(0)   )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_DRAGGED;
			}
			else if( Input.GetMouseButtonUp(0) )
				UserBehavior = UserAction.UA_DROPED;
		}
		else // iOS Touch Event
		{
			#if UNITY_IOS || UNITY_ANDROID
			foreach( Touch touch in Input.touches )
			{
				if ( touch.phase == TouchPhase.Began )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_TOUCHED;
				}
				else if( touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_DRAGGED;
				}
				else if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled ) 
				{
					UserBehavior = UserAction.UA_DROPED;
				}
			}
			#endif
		}
		if( UserBehavior != UserAction.UA_INACTIVE )
		{
			if(Camera.main == null)
			{
				Debug.Log("Theres no main camera");
				return;
			}

			Vector3 vec = Camera.main.ScreenToWorldPoint(InputPosition);
			rayTraced = Camera.main.ScreenPointToRay( InputPosition );
			if(EnableSortingLayerCollision)
			{
				hits2d = Physics2D.RaycastAll(rayTraced.origin,rayTraced.direction);

				if(_3dHits)
					hits = Physics.RaycastAll(rayTraced.origin,rayTraced.direction);
			}
			else
			{
				hit2d = Physics2D.Raycast(rayTraced.origin,rayTraced.direction);

			}

			int gamedimension = 2;
			switch(gamedimension)
			{
			case 2:
				/*
				if(UICamera.isOverUI)
				{
					//Debug.Log("over UI");
					return;
				}
				*/
				if(EnableSortingLayerCollision)
				{
					if(hits2d.Length>0 || _3dHits)
					{
						if(_3dHits)
						{
							if(EnableSortingLayerCollision)
							{
								if(hits.Length<=0)
									return;
							}
							else
							{

							}

						}
						m_lSortingLayerID.Clear();
						m_lSortingOrderArray.Clear();
						topSortingLayer = 0;
						//indexOfTopSortingLayer = 0;
						topSortingOrder = 0;
						indexOfTopSortingOrder = 0;
						component2D = null;
						if(_3dHits)
						{
							for (int i = 0; i < hits.Length; i++)
							{
								component2D = hits[i].transform.GetComponent<SpriteRenderer>();
								//All Colliders that has no SpriteRenderer are not considered in the input
								if(component2D==null)
									continue;
								string key = component2D.sortingLayerName;
								if(mD_IDS.ContainsKey(key))
									m_lSortingLayerID.Add( mD_IDS[key] );
								else
									m_lSortingLayerID.Add(component2D.sortingLayerID);
								m_lSortingOrderArray.Add(component2D.sortingOrder);
							}
						}
						else
						{
							for (int i = 0; i < hits2d.Length; i++)
							{
								component2D = hits2d[i].transform.GetComponent<SpriteRenderer>();
								//All Colliders that has no SpriteRenderer are not considered in the input
								if(component2D==null)
									continue;
								string key = component2D.sortingLayerName;
								if(mD_IDS.ContainsKey(key))
									m_lSortingLayerID.Add( mD_IDS[key] );
								else
									m_lSortingLayerID.Add(component2D.sortingLayerID);
								m_lSortingOrderArray.Add(component2D.sortingOrder);
							}
						}
						for (int j = 0; j < m_lSortingLayerID.Count; j++)
						{
							if (m_lSortingLayerID[j] >= topSortingLayer)
							{
								topSortingLayer = m_lSortingLayerID[j];
								//indexOfTopSortingLayer = j; 
							}
						}
						for (int k = 0; k < m_lSortingOrderArray.Count; k++)
						{
							//if (m_lSortingOrderArray[k] >= topSortingOrder && m_lSortingLayerID[k] == topSortingLayer)
							if (m_lSortingOrderArray[k] > topSortingOrder && m_lSortingLayerID[k] == topSortingLayer)
							{
								topSortingOrder = m_lSortingOrderArray[k];
								indexOfTopSortingOrder = k;
							}
						}
						if(_3dHits)
							hit = hits[indexOfTopSortingOrder];
						else
							hit2d = hits2d[indexOfTopSortingOrder];

						bool condition=false;
						if(_3dHits)
							if(hit.collider != null)
								condition = true;
						else
							if(hit2d.collider != null)
								condition = true;

						if(condition)
						{
							switch( UserBehavior )
							{
							case UserAction.UA_TOUCHED:
								if(_3dHits)
									Touch3D(hit,vec:vec);
								else
									ProcessTouch(hit2d,vec:vec);
								break;
							case UserAction.UA_DRAGGED:
								if(_3dHits)
									Dragged3D(hit,vec:vec);
								else
									ProcessDragged( hit2d,vec:vec);
								break;
							case UserAction.UA_DROPED:
								if(_3dHits)
									Dropped3D(hit,vec:vec);
								else
									ProcessDropped( hit2d,vec:vec);
								break;
							default:
								ProcessInput(hit2d,vec:vec);
								break;
							}
						}
						else
						{
							//No Collider during the raycast
							Touching=false;
						}
					}
				}
				else
				{
					if(hit2d.collider != null)
					{
						switch( UserBehavior )
						{
						case UserAction.UA_TOUCHED:
							ProcessTouch(hit2d,vec:vec);
							break;
						case UserAction.UA_DRAGGED:
							ProcessDragged( hit2d,vec:vec);
							break;
						case UserAction.UA_DROPED:
							ProcessDropped( hit2d,vec:vec);
							break;
						default:
							ProcessInput(hit2d,vec:vec);
							break;
						}
					}
					else
					{
						//No Collider during the raycast
						Touching=false;
					}
				}
				break;
			case 3:
				rayTraced = Camera.main.ScreenPointToRay( InputPosition );
				hit = UICamera.lastHit;
				if(hit.collider != null)
				{
					switch( UserBehavior )
					{
					case UserAction.UA_TOUCHED:
						ProcessTouch( hit, rayTraced );
						break;
					case UserAction.UA_DRAGGED:
						ProcessDragged( hit, rayTraced );
						break;
					case UserAction.UA_DROPED:
						ProcessDropped( hit, rayTraced );
						break;
					default:
						ProcessInput( hit, rayTraced );
						break;
					}
				}
				break;
			}
			UserBehavior = UserAction.UA_INACTIVE;
		}
	}
		
	void ProcessInput( RaycastHit hit, Ray ray )
	{
		// Process Here anything when is not Touched, Dragged  or Dropped
	}
		
	void ProcessTouch( RaycastHit hit, Ray ray )
	{
		GameObject touchedObj = hit.collider.transform.gameObject;
		//make sure that component passcript exist

		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase!=null)
		{
			objBase.OnTouched(hit.collider.transform.position);	
		}
	}

	void ProcessDragged( RaycastHit hit, Ray ray )
	{
		GameObject touchedObj = hit.collider.transform.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			objBase.OnDragged(hit.point);
		}
	}
		
	void ProcessDropped( RaycastHit hit, Ray ray )
	{
		GameObject touchedObj = hit.collider.transform.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			objBase.OnDropped(hit.point);
		}
	}


	void ProcessInput( RaycastHit2D hit,Ray ray = default(Ray), Vector2 vec = default(Vector2) )
	{
		// Process Here anything when is not Touched, Dragged  or Dropped
	}

	void Touch3D(RaycastHit hit,Ray ray = default(Ray),Vector3 vec = default(Vector3) )
	{
		Transform trs = hit.collider.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		
		if(objBase!=null)
		{
			Touching=true;
			objBase.OnTouched(vec);	
		}
	}

	void ProcessTouch( RaycastHit2D hit,Ray ray = default(Ray), Vector2 vec = default(Vector2) )
	{	
		Transform trs = hit.collider.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();

		if(objBase!=null)
		{
			Touching=true;
			objBase.OnTouched(vec);	
		}
	}

	void Dragged3D(RaycastHit hit,Ray ray = default(Ray), Vector3 vec = default(Vector3) )
	{
		Transform trs = hit.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			objBase.OnDragged(vec);
		}
	}

	void ProcessDragged( RaycastHit2D hit,Ray ray = default(Ray), Vector2 vec = default(Vector2) )
	{
		Transform trs = hit.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			objBase.OnDragged(vec);
		}
	}

	void Dropped3D(RaycastHit hit,Ray ray = default(Ray), Vector3 vec = default(Vector3) )
	{
		Transform trs = hit.collider.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			if(Touching)
			{
				objBase.OnClick();
			}
			Touching=false;
			objBase.OnDropped(vec);
		}
	}

	void ProcessDropped( RaycastHit2D hit,Ray ray = default(Ray), Vector2 vec = default(Vector2) )
	{
		Transform trs = hit.collider.transform;
		if(trs == null)
			return;
		GameObject touchedObj = trs.gameObject;
		//make sure that component passcript exist
		BaseEntity objBase = touchedObj.GetComponent<BaseEntity>();
		//ObjectBase objBase =touchedObj.GetComponent<ObjectBase>();
		if(objBase != null)
		{
			if(Touching)
			{
				objBase.OnClick();
			}
			Touching=false;
			objBase.OnDropped(vec);
		}
	}
}
