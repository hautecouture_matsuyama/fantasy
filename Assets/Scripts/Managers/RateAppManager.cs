﻿using UnityEngine;
using System.Collections;

public class RateAppManager : Singleton<RateAppManager> {

    public string androidStoreLink;
    public string iOSStoreLink;

    public int firstPopupTimer; // in seconds
    public float laterPopupTimer; // in seconds

    public GameObject rateAppPanel;

    private string ratedAppKey = "RatedAppKey";

    private string minuteCounterKey = "MinuteCounterKey";

    private string laterKey = "RateAppLaterKey";

    private int timeBetweenChecks = 1; // seconds

    Coroutine ratedAppTimer;

    void Start()
    {

        if (!PlayerPrefs.HasKey(ratedAppKey))
        {
            PlayerPrefs.SetInt(ratedAppKey, 0);
        }

        if (!PlayerPrefs.HasKey(minuteCounterKey))
        {
            PlayerPrefs.SetInt(minuteCounterKey, 0);
        }

        if (!PlayerPrefs.HasKey(laterKey))
        {
            PlayerPrefs.SetInt(laterKey, 0);
        }

        if (PlayerPrefs.GetInt(ratedAppKey) == 0)
        {
            ratedAppTimer = StartCoroutine(CountDownTimer(timeBetweenChecks));
        }
    }

    IEnumerator CountDownTimer(int timeBetweenChecks)
    {
        while (PlayerPrefs.GetInt(ratedAppKey) == 0)
        {
            //check every minute
            yield return new WaitForSeconds(timeBetweenChecks);
            PlayerPrefs.SetInt(minuteCounterKey, PlayerPrefs.GetInt(minuteCounterKey) + timeBetweenChecks);
        }
    }

    public void ShowPanel()
    {
        //rateAppPanel.SetActive(true);
    }

    public void HidePanel()
    {
        rateAppPanel.SetActive(false);
    }

    public bool CheckTimer()
    {
        //Debug.LogError(PlayerPrefs.GetInt(minuteCounterKey));
        return PlayerPrefs.GetInt(laterKey) == 0 ? PlayerPrefs.GetInt(minuteCounterKey) >= firstPopupTimer : PlayerPrefs.GetInt(minuteCounterKey) >= laterPopupTimer;
    }

    public void Later()
    {
        //Debug.Log("later");
        PlayerPrefs.SetInt(minuteCounterKey, 0);
        PlayerPrefs.SetInt(laterKey, 1);
        HidePanel();
    }

    public void RateApp()
    {
        //Debug.Log("rated");
        PlayerPrefs.SetInt(ratedAppKey, 1);

        if (ratedAppTimer != null)
            StopCoroutine(ratedAppTimer);

        PlayerPrefs.SetInt(minuteCounterKey, 0);
        HidePanel();
#if UNITY_ANDROID
        Application.OpenURL(androidStoreLink);
#else
        Application.OpenURL(iOSStoreLink);
#endif
    }

    [ContextMenu("DeletePrefs")]
    void DeleteAllPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}