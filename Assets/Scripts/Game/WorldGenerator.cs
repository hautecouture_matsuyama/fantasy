using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldGenerator : MonoBehaviour
{
	public bool mB_PositionsCalculated;

	public BoxCollider2D[] mA_WallColliders;

	public RoomInfo mC_InitialRoom;

	public int mI_RoomIt;
	public RoomInfo[] mA_Rooms;

	public RoomInfo currentRoom;
	public RoomInfo nextRoom;

	public List<Sprite> mL_AllSprites = new List<Sprite>();

	public LavaBehaviour mC_Lava;
	private int mI_CoinIT;
	private	 int mI_CointRange;
	public Coin mC_Coins;

	private int mI_RoomId = -1;

	public ObstacleHandler[] mA_LargeRoomObstacles;

	public ObstacleHandler mC_PickedObstacle = null;

	public ObstacleHandler mC_ActiveHandler = null;

	public Sprite[] mA_DecorationSprites;

	public static float x_Threshold;
	public static float y_Threshold;

	public int mI_RoomBgIt = 0;

	public bool mB_RoomBgState = true;
	public int mI_RoomBgMax = 10;

	/// <summary>
	/// Calculates the positions of doors according to the screen size
	/// </summary>
	void CalculatePositions()
	{
		Vector3 souPos = Camera.main.ViewportToWorldPoint(new Vector3(1.5f,1.5f,0f));

		x_Threshold = souPos.x;//(float)(IngameManager.GetInstance().mI_DeviceWidth)  * 0.01f;
		y_Threshold = souPos.y;

		Debug.Log("xTreshold="+x_Threshold);
		Debug.Log("yTreshold="+y_Threshold);

		Vector3 positionNorth = mC_InitialRoom.NorthPosition.position;
		Vector3 positionSouth = mC_InitialRoom.SouthPosition.position;

		int it=0;
		foreach(Collider2D wall in mA_WallColliders)
		{
			Vector3 position = Vector3.zero;
			if(it==0)//North
			{
				//position = wall.transform.position;
				position = Camera.main.WorldToViewportPoint(positionNorth);
				position.y += 0.1f;
				position = Camera.main.ViewportToWorldPoint(position);
				Hidder_Upward.transform.position = position;
				position = Camera.main.WorldToViewportPoint(positionNorth);
				position.y += 0.05f;
				position = Camera.main.ViewportToWorldPoint(position);
			}
			else if(it==1)//East
			{
				Vector3 viewPort = new Vector3(1.05f,0.5f,0f);
				position = Camera.main.ViewportToWorldPoint( viewPort );
				viewPort.x += 0.1f;
				Hidder_Right.transform.position = Camera.main.ViewportToWorldPoint( viewPort );
			}
			else if(it==2)//South
			{
				position = Camera.main.WorldToViewportPoint(positionSouth);
				position.y -= 0.1f;
				position = Camera.main.ViewportToWorldPoint(position);
				Hidder_Downward.transform.position = position;
				position = Camera.main.WorldToViewportPoint(positionSouth);
				position.y -= 0.05f;
				position = Camera.main.ViewportToWorldPoint(position);
			}
			else if(it==3)//West
			{
				Vector3 viewPort = new Vector3(-0.05f,0.5f,0f);
				position = Camera.main.ViewportToWorldPoint( viewPort );
				viewPort.x -= 0.1f;
				Hidder_Left.transform.position = Camera.main.ViewportToWorldPoint( viewPort );
			}
			wall.transform.position = position;
			it++;
		}

		mB_PositionsCalculated = true;
	}

	DoorSequence NorthSeq;
	DoorSequence EastSeq;
	DoorSequence SouthSeq;
	DoorSequence WestSeq;

	public GameObject Hidder_Upward;
	public GameObject Hidder_Downward;
	public GameObject Hidder_Left;
	public GameObject Hidder_Right;

	public bool mB_IsLargeRoom = false;
	public int  mI_LargeRoomLenght = 0;
	public int  mI_LargeRoomIt = 0;

	/// <summary>
	/// The m a_ wall sprites.
	/// 0 is closed
	/// 1 = Start/End largeRoom
	/// 2 = Half large room
	/// </summary>
	public Sprite[] mA_WallSprites;

	/// <summary>
	/// The m a_ floor sprites.
	/// </summary>
	public Sprite[] mA_FloorSprites;

	public void Initialize()
	{
		mI_RoomIt=0;

		mB_RoomBgState = true;
		mI_RoomBgIt = 0;
		mI_RoomBgMax = 10;

		if(!mB_PositionsCalculated)
		{
			CalculatePositions();
		}
		// Position Everything at 0,0,0

		mI_RoomId = 0;

		//Create a room with a unique constan door
		{
			NorthSeq = new DoorSequence();
			EastSeq = new DoorSequence();
			SouthSeq = new DoorSequence();
			WestSeq = new DoorSequence();

			NorthSeq.mC_doorSprite = mL_AllSprites[0];
			NorthSeq.mF_OpenTimer  = -1f;
			NorthSeq.mB_Active = true;
			NorthSeq.mS_DoorKey = "WorkshopDoor";//Door
			EastSeq.mC_doorSprite =  null;
			EastSeq.mF_OpenTimer  = 0f;
			EastSeq.mB_Active = false;
			SouthSeq.mC_doorSprite = null;
			SouthSeq.mF_OpenTimer  = 0f;
			SouthSeq.mB_Active = false;
			WestSeq.mC_doorSprite = null;
			WestSeq.mF_OpenTimer  = 0f;
			WestSeq.mB_Active = false;

			mC_Lava.transform.position = Vector3.left * 20f;

			mC_InitialRoom.WallsSprite.color = Color.white;

			mC_InitialRoom.mB_IsInitialRoom = true;

			mC_InitialRoom.SetState(StateTween.ENTER);
			mC_InitialRoom.transform.position = Vector3.zero;
			mC_InitialRoom.Initialize(NorthSeq,EastSeq,SouthSeq,WestSeq,Vector3.zero);
			mC_InitialRoom.FinishTween();

			mC_InitialRoom.SetRoomSprites( mA_WallSprites[5], mA_FloorSprites[5] );
			//mC_InitialRoom.SetRoomSprites( mA_WallSprites[0], mA_FloorSprites[0] );
			mC_InitialRoom.SetRoomOrientation(Vector3.zero);

			mB_IsLargeRoom = false;
			if(mC_ActiveHandler != null)
			{
				mC_ActiveHandler = null;
			}

			mI_CoinIT = 0;
			mI_CointRange = 4;

			currentRoom = mC_InitialRoom;
			currentRoom.mI_CurrentRoomId = mI_RoomId;

			if(GameManager.ShowTutorial)
			{
				// Show TutorialStep
				WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.TUTORIAL);
				//TutorialManager.TutorialStep++;
			}

			GenerateNextRoom(DIRECTION.NORTH);
		}

	}

	public void FreezeLava()
	{
		mC_Lava.StopMovement();
		Invoke("ResumeLavaMovement",2f);
	}

	DIRECTION largeRoomInitialDirection;

	List<DoorCreationStruct> TutorialRoom()
	{
		int step = TutorialManager.TutorialStep;

		List<DoorCreationStruct> tempList = new List<DoorCreationStruct>();
		
		mB_IsLargeRoom = false;
		
		DIRECTION mappedDirection = DIRECTION.NORTH;
		DIRECTION door = DIRECTION.NORTH;

		if(step == 0)
		{
			DoorCreationStruct newDoor2 = new DoorCreationStruct();
			newDoor2.myDirection = DIRECTION.EAST;
			newDoor2.mySequence.mB_Active = true;
			newDoor2.mySequence.mB_IsTrap = false;
			newDoor2.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor2.mySequence.mF_OpenTimer  = 1f;
			newDoor2.mySequence.mS_DoorKey = "WorkshopDoor";

			DoorCreationStruct newDoor3 = new DoorCreationStruct();
			newDoor3.myDirection = DIRECTION.NORTH;
			newDoor3.mySequence.mB_Active = true;
			newDoor3.mySequence.mB_IsTrap = true;
			newDoor3.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor3.mySequence.mF_OpenTimer  = 1f;
			newDoor3.mySequence.mS_DoorKey = "WorkshopFake_Door";

			DoorCreationStruct newDoor4 = new DoorCreationStruct();
			newDoor4.myDirection = DIRECTION.WEST;
			newDoor4.mySequence.mB_Active = true;
			newDoor4.mySequence.mB_IsTrap = true;
			newDoor4.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor4.mySequence.mF_OpenTimer  = 1f;
			newDoor4.mySequence.mS_DoorKey = "WorkshopFake_Door";

			tempList.Add(newDoor2);
			tempList.Add(newDoor3);
			tempList.Add(newDoor4);
		}
		else if(step == 1)
		{
			DoorCreationStruct newDoor2 = new DoorCreationStruct();
			newDoor2.myDirection = DIRECTION.EAST;
			newDoor2.mySequence.mB_Active = true;
			newDoor2.mySequence.mB_IsTrap = true;
			newDoor2.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor2.mySequence.mF_OpenTimer  = 1f;
			newDoor2.mySequence.mS_DoorKey = "WorkshopFake_Door";
			
			DoorCreationStruct newDoor3 = new DoorCreationStruct();
			newDoor3.myDirection = DIRECTION.NORTH;
			newDoor3.mySequence.mB_Active = true;
			newDoor3.mySequence.mB_IsTrap = true;
			newDoor3.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor3.mySequence.mF_OpenTimer  = 1f;
			newDoor3.mySequence.mS_DoorKey = "WorkshopFake_Door";
			
			DoorCreationStruct newDoor4 = new DoorCreationStruct();
			newDoor4.myDirection = DIRECTION.SOUTH;
			newDoor4.mySequence.mB_Active = true;
			newDoor4.mySequence.mB_IsTrap = false;
			newDoor4.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor4.mySequence.mF_OpenTimer  = 1f;
			newDoor4.mySequence.mS_DoorKey = "WorkshopDoor";
			
			tempList.Add(newDoor2);
			tempList.Add(newDoor3);
			tempList.Add(newDoor4);
		}
		else if(step == 2)
		{
			DoorCreationStruct newDoor2 = new DoorCreationStruct();
			newDoor2.myDirection = DIRECTION.WEST;
			newDoor2.mySequence.mB_Active = true;
			newDoor2.mySequence.mB_IsTrap = false;
			newDoor2.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor2.mySequence.mF_OpenTimer  = 1f;
			newDoor2.mySequence.mS_DoorKey = "WorkshopDoor";
			
			DoorCreationStruct newDoor3 = new DoorCreationStruct();
			newDoor3.myDirection = DIRECTION.EAST;
			newDoor3.mySequence.mB_Active = true;
			newDoor3.mySequence.mB_IsTrap = true;
			newDoor3.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor3.mySequence.mF_OpenTimer  = 1f;
			newDoor3.mySequence.mS_DoorKey = "WorkshopFake_Door";
			
			DoorCreationStruct newDoor4 = new DoorCreationStruct();
			newDoor4.myDirection = DIRECTION.SOUTH;
			newDoor4.mySequence.mB_Active = true;
			newDoor4.mySequence.mB_IsTrap = true;
			newDoor4.mySequence.mC_doorSprite = mL_AllSprites[0];//1
			newDoor4.mySequence.mF_OpenTimer  = 1f;
			newDoor4.mySequence.mS_DoorKey = "WorkshopFake_Door";
			
			tempList.Add(newDoor2);
			tempList.Add(newDoor3);
			tempList.Add(newDoor4);

		}

		if(step == 3)
		{
			tempList = CreateAndLocateDoors(DIRECTION.WEST);
		}
		else if(step == 4)
		{
			Debug.LogWarning("SHOULDNT BE HERE");
		}
		return tempList;
	}

	/// <summary>
	/// Generates the next room.
	/// </summary>
	/// <param name="previousDirection">Direction of the previous room.</param>
	public void GenerateNextRoom(DIRECTION previousDirection)
	{
		/*if(TutorialManager.doingTutorial)
		{
			TutorialRoom();
			return;
		}*/
		Vector3 position = Vector3.zero;

		NorthSeq = new DoorSequence();
		EastSeq = new DoorSequence();
		SouthSeq = new DoorSequence();
		WestSeq = new DoorSequence();

		List<DoorCreationStruct> tempList = new List<DoorCreationStruct>();

		string key="";
		if(mB_RoomBgState)
			key = "Workshop";

		if(!mB_IsLargeRoom)
		{  
			//North
			if( (int)previousDirection == 0)
			{
				// Poner siguiente cuarto sobre el actual
				position = Vector3.up * y_Threshold;
				// Poner trampa en la puerta INFERIOR
				SouthSeq.mC_doorSprite = mL_AllSprites[0];
				SouthSeq.mF_OpenTimer  = -1f;
				SouthSeq.mB_IsTrap     = true;
				SouthSeq.mB_Active     = true;
				SouthSeq.mS_DoorKey    = key+"Fake_Door";
			}
			// East
			else if( (int)previousDirection == 1)
			{
				// Poner el siguiente cuarto a la derecha del atual
				position = Vector3.right * x_Threshold;
				// Poner trampa en la puerta IZQUIERDA
				WestSeq.mC_doorSprite = mL_AllSprites[0];
				WestSeq.mF_OpenTimer  = -1f;
				WestSeq.mB_IsTrap     = true;
				WestSeq.mB_Active     = true;
				WestSeq.mS_DoorKey    = key+"Fake_Door";
			}
			// South
			else if( (int)previousDirection == 2)
			{
				// Poner el siguiente cuarto abajo del actual
				position = Vector3.down * y_Threshold;
				// Poner trampa en la puerta SUPERIOR
				NorthSeq.mC_doorSprite = mL_AllSprites[0];
				NorthSeq.mF_OpenTimer  = -1f;
				NorthSeq.mB_IsTrap     = true;
				NorthSeq.mB_Active     = true;
				NorthSeq.mS_DoorKey    = key+"Fake_Door";
			}
			// West
			else
			{
				// Poner el siguiente cuarto a la izquierda del actual
				position = Vector3.left * x_Threshold;
				// Poner trampa en la puerta DERECHA
				EastSeq.mC_doorSprite = mL_AllSprites[0];
				EastSeq.mF_OpenTimer  = -1f;
				EastSeq.mB_IsTrap     = true;
				EastSeq.mB_Active     = true;
				EastSeq.mS_DoorKey    = key+"Fake_Door";
			}

			if(TutorialManager.doingTutorial)
			{
				tempList = TutorialRoom();

			}
			else
			{
				if(Random.Range(0,10)<5 ||  //< 5 <1
				   (previousDirection == DIRECTION.NORTH || previousDirection == DIRECTION.SOUTH ) ||
				   mB_RoomBgState )
				{
					/*
					if(previousDirection == DIRECTION.WEST || previousDirection == DIRECTION.EAST)
						tempList = CreateLargeRoom(previousDirection);
					else*/
					//NORMAL ROOM
					tempList = CreateAndLocateDoors(previousDirection);
				}
				else
				{
					Debug.Log("&&&&& CREATING LARGE ROOM &&&&&");
					tempList = CreateLargeRoom(previousDirection);
				}
			}
			mI_RoomId++;
		}
		else
		{
			tempList = LargeRoom(previousDirection);
			if((int)previousDirection == 0)
				position = Vector3.up * y_Threshold;
			else if((int)previousDirection == 2)
				position = Vector3.down * y_Threshold;
			else if( (int)previousDirection == 1)
				position = Vector3.right * x_Threshold;
			else
				position = Vector3.left * x_Threshold;
		}

		foreach(DoorCreationStruct data in tempList)
		{
			DoorSequence tempSequence = new DoorSequence();
			tempSequence = data.mySequence;
			if(data.myDirection == DIRECTION.NORTH)
				NorthSeq = tempSequence;
			else if(data.myDirection == DIRECTION.EAST)
				EastSeq = tempSequence;
			else if(data.myDirection == DIRECTION.SOUTH)
				SouthSeq = tempSequence;
			else if(data.myDirection == DIRECTION.WEST)
				WestSeq = tempSequence;
		}

		int it = mI_RoomIt % mA_Rooms.Length;

		mA_Rooms[it].Initialize(NorthSeq,EastSeq,SouthSeq,WestSeq,position);
		Sprite bR,bL,tR,tL;
		bR = bL = tR = tL = null;
		DecorateRoom(ref tL,ref tR,ref bL,ref bR);
		mA_Rooms[it].SetDecoration(tR,tL,bR,bL);

		//TODO: ARRANGE AND SET FLOOR SPRITES

		int bgSum = 0;
		if(mB_RoomBgState)
		{
			bgSum = 5;
		}
		mI_RoomBgIt++;
		if(mI_RoomBgIt >= mI_RoomBgMax)
		{
			mI_RoomBgMax = Random.Range(10,11);
			mB_RoomBgState = !mB_RoomBgState;
			mI_RoomBgIt=0;
		}

		if(mB_IsLargeRoom)
		{
			if(previousDirection == DIRECTION.NORTH || previousDirection == DIRECTION.SOUTH)
			{

			}
			else
			{
				// We need to check if it is West or East orientation
				Vector3 orientation = Vector3.zero;
				if(mI_LargeRoomIt == 0)
				{
					mA_Rooms[it].SetRoomSprites( mA_WallSprites[1+bgSum], mA_FloorSprites[1+bgSum] );
					if(previousDirection == DIRECTION.WEST)
						orientation = Vector3.zero;//mA_Rooms[it].SetRoomOrientation( currentRoom.doorDirection );// Vector3.zero ? 
					else if(previousDirection == DIRECTION.EAST)
						orientation = Vector3.up * 180f;
				}
				else if(mI_LargeRoomIt==mI_LargeRoomLenght)
				{
					mA_Rooms[it].SetRoomSprites( mA_WallSprites[1+bgSum], mA_FloorSprites[1+bgSum] );
					if(previousDirection == DIRECTION.WEST)
						orientation = Vector3.up * 180f;//mA_Rooms[it].SetRoomOrientation( currentRoom.doorDirection );// Vector3.zero ? 
					else if(previousDirection == DIRECTION.EAST)
						orientation = Vector3.zero;
					//mA_Rooms[it].SetRoomOrientation( GetReversedDirection( currentRoom.doorDirection ) );
				}
				else
				{
					mA_Rooms[it].SetRoomSprites( mA_WallSprites[2+bgSum], mA_FloorSprites[2+bgSum] );
					//mA_Rooms[it].SetRoomOrientation( currentRoom.doorDirection );
					//mA_Rooms[it].SetRoomOrientation(Vector3.zero);
				}
				mA_Rooms[it].SetRoomOrientation(orientation);
			}
		}
		else
		{
			mA_Rooms[it].SetRoomSprites( mA_WallSprites[0+bgSum], mA_FloorSprites[0+bgSum] );
			mA_Rooms[it].SetRoomOrientation(Vector3.zero);
		}

		/*
		if(mI_LargeRoomIt == mI_LargeRoomLenght)
			mB_IsLargeRoom = false;
		*/

		if(mB_IsLargeRoom)
			mA_Rooms[it].mB_isLarge = true;
		else
			mA_Rooms[it].mB_isLarge = false;

		nextRoom = mA_Rooms[it];

		nextRoom.mI_CurrentRoomId = mI_RoomId;

		mI_RoomIt++;
	}

	void DecorateRoom(ref Sprite topLeft,ref Sprite topRight,ref Sprite botLeft,ref Sprite botRight)
	{
		int chance = -1;
		List<Sprite> tempList = new List<Sprite>();
		int bgSum = 0;
		int max = 4;
		if(mB_RoomBgState)
		{
			bgSum = 4;
			max = 8;
		}
		for(int i =bgSum;i<max;i++)
		//foreach(Sprite data in mA_DecorationSprites)
			tempList.Add(mA_DecorationSprites[i]);

		for(int i = 0; i < 4 ; i++)
		{
			chance = Random.Range(0,4);
			if(chance == 0)
			{
				if(mB_IsLargeRoom)
				{

					if(currentRoom.doorDirection == DIRECTION.WEST
					   || currentRoom.doorDirection == DIRECTION.NORTH)
						continue;
				}
				chance = Random.Range(0,3);
				if(chance < 4) //==1
				{
					chance = Random.Range(0,tempList.Count);
					topLeft = tempList[chance];
					tempList.RemoveAt(chance);
					i++;
				}
			}
			else if(chance == 1)
			{
				if(mB_IsLargeRoom)
				{
					if(currentRoom.doorDirection == DIRECTION.EAST
					   || currentRoom.doorDirection == DIRECTION.NORTH)
						continue;
				}
				chance = Random.Range(0,3);
				if(chance < 4) //==1
				{
					chance = Random.Range(0,tempList.Count);
					topRight = tempList[chance];
					tempList.RemoveAt(chance);
					i++;
				}
			}
			else if(chance == 2)
			{
				if(mB_IsLargeRoom)
				{
					if(currentRoom.doorDirection == DIRECTION.WEST
					   || currentRoom.doorDirection == DIRECTION.SOUTH)
						continue;
				}
				chance = Random.Range(0,3);
				if(chance < 4) //==1
				{
					chance = Random.Range(0,tempList.Count);
					botLeft = tempList[chance];
					tempList.RemoveAt(chance);
					i++;
				}
			}
			else if(chance == 3)
			{
				if(mB_IsLargeRoom)
				{
					if(currentRoom.doorDirection == DIRECTION.EAST
					   || currentRoom.doorDirection == DIRECTION.SOUTH)
						continue;
				}
				chance = Random.Range(0,3);
				if(chance < 4) //==1
				{
					chance = Random.Range(0,tempList.Count);
					botRight = tempList[chance];
					tempList.RemoveAt(chance);
					i++;
				}
			}
		}
	}

	/// <summary>
	/// Creates the and locate doors.
	/// </summary>
	/// <returns>The door creation list.</returns>
	/// <param name="door">Direction of the fake Door.</param>
	List<DoorCreationStruct> CreateAndLocateDoors(DIRECTION door)
	{
		List<DoorCreationStruct> tempList = new List<DoorCreationStruct>();

		mB_IsLargeRoom = false;

		bool falseDoor = false;
		//if(Random.Range(0,4)==0)//0,4 == 0
		//	falseDoor = true;

		DIRECTION mappedDirection = DIRECTION.NORTH;
		if(door == DIRECTION.NORTH)
			mappedDirection = DIRECTION.SOUTH;
		else if(door==DIRECTION.EAST)
			mappedDirection = DIRECTION.WEST;
		else if(door==DIRECTION.SOUTH)
			mappedDirection = DIRECTION.NORTH;
		else
			mappedDirection = DIRECTION.EAST;

		int doors = 3;;//Random.Range(1,3);//1,3
		List<DIRECTION> mL_Directions = new List<DIRECTION>();

		int it=0;
		bool pickedDoor=false;
		string key = "";
		if(mB_RoomBgState)
			key = "Workshop";
		while(it < doors)
		{
			int location = Random.Range(0,4);
			if(mL_Directions.Contains( (DIRECTION)location) || location == (int)mappedDirection)
			{
				//REPEAT
			}
			else
			{
				DoorCreationStruct newDoor = new DoorCreationStruct();
				if(!pickedDoor)
				{
					if(falseDoor)
					{
						newDoor.myDirection = (DIRECTION)location;
						newDoor.mySequence.mB_Active = true;
						newDoor.mySequence.mB_IsTrap = false;
						newDoor.mySequence.mC_doorSprite = mL_AllSprites[0];//1
						newDoor.mySequence.mF_OpenTimer  = 0.85f;
						newDoor.mySequence.mS_DoorKey = key+"Door";//Wall_Door
					}
					else
					{
						newDoor.myDirection = (DIRECTION)location;
						newDoor.mySequence.mB_Active = true;
						newDoor.mySequence.mB_IsTrap = false;
						newDoor.mySequence.mC_doorSprite = mL_AllSprites[0];
						newDoor.mySequence.mF_OpenTimer  = 0.85f;
						newDoor.mySequence.mS_DoorKey = key+"Door";
					}
					pickedDoor = true;
				}
				else
				{
					newDoor.myDirection = (DIRECTION)location;
					newDoor.mySequence.mB_Active = true;
					newDoor.mySequence.mB_IsTrap = true;
					newDoor.mySequence.mC_doorSprite = mL_AllSprites[0];//1
					newDoor.mySequence.mF_OpenTimer  = 1f;
					newDoor.mySequence.mS_DoorKey = key+"Fake_Door";
				}
				tempList.Add( newDoor );
				mL_Directions.Add( (DIRECTION)location );
				it++;
			}
		}
		return tempList;
	}

	List<DoorCreationStruct> CreateLargeRoom(DIRECTION prevDir)
	{
		List<DoorCreationStruct> tempList = new List<DoorCreationStruct>();

		mI_LargeRoomLenght = Random.Range(2,3);//1,3
		mI_LargeRoomIt     = 0;
		mB_IsLargeRoom = true;

		DoorCreationStruct newDoor = new DoorCreationStruct();

		if(prevDir == DIRECTION.NORTH)
			newDoor.myDirection = DIRECTION.NORTH;
		else if(prevDir == DIRECTION.WEST)
			newDoor.myDirection = DIRECTION.WEST;
		else if(prevDir == DIRECTION.EAST)
			newDoor.myDirection = DIRECTION.EAST;
		else if(prevDir == DIRECTION.SOUTH)
			newDoor.myDirection = DIRECTION.SOUTH;


			newDoor.mySequence.mB_Active = true;
			newDoor.mySequence.mB_IsTrap = false;
			// This doors are invincible
			newDoor.mySequence.mC_doorSprite = null;
			newDoor.mySequence.mF_OpenTimer = -1f;
			newDoor.mySequence.mS_DoorKey = "Invisible_Door";

		tempList.Add(newDoor);

		PickLargeObstacle();

		return tempList;
	}

	/// <summary>
	/// Picks a large obstacle.
	/// </summary>
	void PickLargeObstacle()
	{
		if(mA_LargeRoomObstacles.Length == 0)
		{
			Debug.LogError("There isnt any Obstacle in the array");
			return;
		}
		int random = Random.Range(0,mA_LargeRoomObstacles.Length);

		mC_PickedObstacle = mA_LargeRoomObstacles[random];
	}

	List<DoorCreationStruct> LargeRoom(DIRECTION prevDir)
	{
		List<DoorCreationStruct> tempList = new List<DoorCreationStruct>();
		DoorCreationStruct newDoor = new DoorCreationStruct();

		mI_LargeRoomIt++;

		string key = "";
		if(mB_RoomBgState)
			key = "Workshop";

		newDoor.myDirection = prevDir;
		newDoor.mySequence.mB_Active = true;
		newDoor.mySequence.mB_IsTrap = false;
		newDoor.mySequence.mF_OpenTimer = -1f;
		if(mI_LargeRoomIt == mI_LargeRoomLenght)
		{
			newDoor.mySequence.mC_doorSprite = mL_AllSprites[0];
			newDoor.mySequence.mS_DoorKey    = key+"Door";
		}
		else
		{
			newDoor.mySequence.mC_doorSprite = null;
			newDoor.mySequence.mS_DoorKey    = "Invisible_Door";
		}

		DoorCreationStruct deathDoor = new DoorCreationStruct();
		deathDoor.myDirection = GetReversedDirection(prevDir);
		deathDoor.mySequence.mB_Active = true;
		deathDoor.mySequence.mB_IsTrap = true;
		deathDoor.mySequence.mF_OpenTimer = -1f;
		deathDoor.mySequence.mC_doorSprite = null;
		deathDoor.mySequence.mS_DoorKey    = "Invisible_FakeDoor";

		tempList.Add( newDoor );
		tempList.Add( deathDoor );

		return tempList;
	}

	public void TurnHidders(bool state)
	{
		Hidder_Upward.SetActive(state);
		Hidder_Downward.SetActive(state);
		Hidder_Left.SetActive(state);
		Hidder_Right.SetActive(state);
	}
	
	public void NextRoom(int room)
	{
		TurnHidders(false);

		OPENING_DOORS = false;

		if(mC_ActiveHandler != null)
		{
			mC_ActiveHandler.Despawn();
		}

		mC_Lava.StopMovement();

		mI_RoomIt = room;

		mI_CoinIT++;
		if(mI_CoinIT >= mI_CointRange)
		{
			mC_Coins.myAnimator.Play("Idle");
			mC_Coins.transform.parent = nextRoom.transform;
			mC_Coins.transform.localPosition = Vector3.zero;
			mC_Coins.Initialize();

			mI_CointRange = Random.Range(3,6);
			mI_CoinIT=0;
		}

		currentRoom.Exit(-nextRoom.myTween.from);
		nextRoom.Enter();

		if(currentRoom.mI_CurrentRoomId != nextRoom.mI_CurrentRoomId)
		{
			//Debug.Log("Cleared Power Up");
			IngameManager.GetInstance().ClearPowerUps();
		}

		if(mB_IsLargeRoom && mC_PickedObstacle != null)
		{
            //GameObject obs = EntityManager.GetInstance().Pools[mC_PickedObstacle.mS_EntityKey].Spawn();
            GameObject obs = EntityManager.GetInstance().Pools[mC_PickedObstacle.mS_EntityKey].Spawn();
            mC_ActiveHandler = obs.GetComponent<ObstacleHandler>();

			Vector3 originPoint = nextRoom.GetTrapPosition(nextRoom.doorDirection);

			mC_ActiveHandler.myTweenPosition.from = originPoint;

			Vector3 destinyPoint = nextRoom.myTween.to + nextRoom.GetLocalTrapPosition(nextRoom.doorDirection);
			mC_ActiveHandler.myTweenPosition.to   = destinyPoint;


			if(nextRoom.doorDirection == DIRECTION.NORTH || 
			   nextRoom.doorDirection == DIRECTION.SOUTH)
			{
				obs.transform.eulerAngles = Vector3.zero;//GetRoomRotation(nextRoom.doorDirection);
				mC_ActiveHandler.Initialize(AXIS.HORIZONTAL);
			}
			else
			{
				//TODO: This is a test
				if(IngameManager.GetInstance().previousDirection == DIRECTION.EAST)
					obs.transform.eulerAngles = Vector3.back * 90f;
				else
					obs.transform.eulerAngles = Vector3.forward * 90f;
				mC_ActiveHandler.Initialize(AXIS.VERTICAL);
			}
		}

		if(mI_LargeRoomIt == mI_LargeRoomLenght)
		{
			mB_IsLargeRoom = false;
			mC_PickedObstacle = null;
		}

		currentRoom = nextRoom;

		Debug.Log("Current room ="+currentRoom.name);

		GenerateNextRoom(nextRoom.doorDirection);//DIRECTION.EAST
	}

	public void TurnWallsColliders(bool state)
	{
		foreach(BoxCollider2D data in mA_WallColliders)
			data.enabled = state;
	}

	public void StartRoomBehaviour()
	{
		if(currentRoom == null)
		{
			Debug.LogWarning("Current room is null");
			return;
		}
		if(currentRoom.mB_IsInitialRoom)
		{
			Debug.LogWarning("Current room is initial room");
			return;
		}
		//currentRoom.StartBehaviour();
		if(mC_ActiveHandler!=null)
		{
			if(mC_ActiveHandler.gameObject.activeInHierarchy)
				mC_ActiveHandler.Activate();
			else
				mC_ActiveHandler = null;
			/*
			if(currentRoom.mB_isLarge)//mB_IsLargeRoom
				mC_ActiveHandler.Activate();
			else
			{
				mC_ActiveHandler = null;
			}*/
		}
		if(mI_RoomIt > 0)
		{
			float curve = 1f;
			if(IngameManager.GetInstance().mI_Score < 20)
			{
				curve = 0.85f;
			}
			float speed = 1.05f;
			// IF is room large make it slower?
			if(mB_IsLargeRoom)
			{
				if( IngameManager.GetInstance().mB_DoingPowerUp ) 
				{
					if( IngameManager.GetInstance().mB_DoingFreezePowerUp)
						speed = 0f;
				}
				else
					speed = 0.95f;
			}

			speed *= curve;
			mC_Lava.Initialize( IngameManager.GetInstance().previousDirection,0f,speed, currentRoom.gameObject);
		}
	}

	public static bool OPENING_DOORS = false;
	public void StartDoorOpening()
	{
		if(currentRoom != null)
		{
			OPENING_DOORS = true;
			currentRoom.StartBehaviour();
		}
	}

	static public DIRECTION GetReversedDirection(DIRECTION direction)
	{
		if(direction == DIRECTION.NORTH)
			return DIRECTION.SOUTH;
		if(direction == DIRECTION.SOUTH)
		   return DIRECTION.NORTH;
		if(direction == DIRECTION.EAST)
			return DIRECTION.WEST;

		return DIRECTION.EAST;
	}

	static public Vector3 GetRoomRotation(DIRECTION direction)
	{
		if(direction == DIRECTION.NORTH)
			return Vector3.zero;
		if(direction == DIRECTION.SOUTH)
			return Vector3.forward*180f;
		if(direction == DIRECTION.EAST)
			return Vector3.forward*90f;
		
		return Vector3.back*90f;
	}
}

