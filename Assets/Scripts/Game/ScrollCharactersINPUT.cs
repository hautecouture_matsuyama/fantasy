﻿using UnityEngine;
using System.Collections;

public class ScrollCharactersINPUT : MonoBehaviour 
{
	enum UserAction
	{
		UA_INACTIVE,
		UA_TOUCHED,
		UA_DRAGGED,
		UA_DROPED
	};

	public PickCharacter_Window myWind;

	Vector2 InputPosition;
	UserAction UserBehavior = UserAction.UA_INACTIVE;
	Ray rayTraced;
	RaycastHit2D hit2d;

	public Vector3 TouchBeganPos = Vector3.one * 200f;
	public Vector3 TouchDragPos = Vector3.one * 200f;

	public Vector3 deltaPos = Vector3.one * 200f;

	public Vector3 MinimumCharPosition;
	public Vector3 MaximumCharPostion;

	void Update( )
	{				
		//if(LevelManager.GetInstance().isPause)
		//	return;
		// in Editor Mouse Click
		if( Application.platform == RuntimePlatform.WindowsEditor || 
		   Application.platform == RuntimePlatform.OSXEditor || 
		   Application.platform == RuntimePlatform.WindowsPlayer )
		{
			if( Input.GetButtonDown("Fire1") )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_TOUCHED;
			}
			else if( Input.GetMouseButton(0)   )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_DRAGGED;
			}
			else if( Input.GetMouseButtonUp(0) )
				UserBehavior = UserAction.UA_DROPED;
		}
		else // iOS Touch Event
		{
			#if UNITY_IOS || UNITY_ANDROID
			foreach( Touch touch in Input.touches )
			{
				if ( touch.phase == TouchPhase.Began )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_TOUCHED;
				}
				else if( touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_DRAGGED;
				}
				else if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled ) 
				{
					UserBehavior = UserAction.UA_DROPED;
				}
			}
			#endif
		}

		if(UICamera.isOverUI)
		{
			//Debug.Log("over UI");
			//return;
		}

		switch( UserBehavior )
		{
		case UserAction.UA_TOUCHED:
			//ProcessTouch(hit2d,vec:vec);
			TouchBeganPos = InputPosition;
			break;
		case UserAction.UA_DRAGGED:
			TouchBeganPos = TouchDragPos;
			TouchDragPos = InputPosition;

			if(TouchBeganPos != (Vector3.one * 200f))
			{
				deltaPos = TouchBeganPos - TouchDragPos;
				myWind.UpdateScroll(deltaPos);
			}
			//ProcessDragged( hit2d,vec:vec);
			break;
		case UserAction.UA_DROPED:
			TouchBeganPos = Vector3.one * 200f;
			TouchDragPos = Vector3.one * 200f;
			deltaPos     = Vector3.one * 200f;

			myWind.DropScroll();

			UserBehavior = UserAction.UA_INACTIVE;
			//ProcessDropped( hit2d,vec:vec);
			break;
		default:
			//ProcessInput(hit2d,vec:vec);
			break;
		}
	}
}
