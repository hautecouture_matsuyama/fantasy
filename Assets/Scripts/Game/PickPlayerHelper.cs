﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PickPlayerHelper : MonoBehaviour 
{

	public PickCharacter_Window myRef;

    [SerializeField]
    private Text charaNameText;


	public List<PickPlayerInfo> mL_Players = new List<PickPlayerInfo>();

	void OnTriggerEnter(Collider other) 
	{
		if(other.GetComponent<PickPlayerInfo>() != null)
		{
			PickPlayerInfo info = other.GetComponent<PickPlayerInfo>();

            //myRef.PickedCharacterLabel.text = info.myAnimator.runtimeAnimatorController.name;
            //myRef.PickedCharacterLabel.text = info.myAnimator.gameObject.GetComponent<SpriteRenderer>().sprite.name;
            charaNameText.text = info.myAnimator.gameObject.GetComponent<SpriteRenderer>().sprite.name;


            WindowManager.GetInstance().AllCharactersWindow.CanPick = info.mB_IsEnabled;
            info.TurnAnimator(info.mB_IsEnabled);
            myRef.mI_CharIndex = info.mI_PlayerIndex;
            if (info.mB_IsEnabled)
			{
				myRef.PickButton.spriteName = "play_button";
				myRef.PickButtonButton.normalSprite = "play_button";
				myRef.PickButton.width = 100;
				myRef.PickButton.height = 110;
			}
			else
			{
				
				myRef.PickButton.spriteName = "buy_character_button";
				myRef.PickButtonButton.normalSprite = "buy_character_button";
				myRef.PickButton.width = 137;
				myRef.PickButton.height = 95;
			}

			if(!mL_Players.Contains(info))
				mL_Players.Add(info);
		}
	}

	void OnTriggerExit(Collider other) 
	{
		if(other.GetComponent<PickPlayerInfo>() != null)
		{
			PickPlayerInfo info = other.GetComponent<PickPlayerInfo>();

			info.TurnAnimator(false);

			if(mL_Players.Contains(info))
				mL_Players.Remove(info);
		}
	}


}
