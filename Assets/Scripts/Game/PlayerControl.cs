using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	public Rigidbody2D myRigidbody;
	//public RuntimeAnimatorController

	public float MovementSpeed;
	public Vector2 speed;

	public bool mB_CanInteract = true;
	public Vector3 mV3_InitialPosition = Vector2.zero;

	public Vector3 mV3_PositionCenter;

	public bool mB_ObservePosition = false;
	public RectVector destinyArea;

    public GameObject playerGraphics;
	public Animator myAnimator;

	public TweenScale myTweenScale;


	public SpriteRenderer myRenderer;
	public int NormalLayer;
	public int DeadLayer;

	public Animator mA_ShieldAnimator;

    //	public GoogleAnalyticsV3 googleAnalytics;

    public bool isMan;
    public AudioSource charaVoice;

    public AudioClip charaVoiceMan;
    public AudioClip charaVoiceWoman;

    public void Initialize(Sprite spriteControl)
	{
		if(myRigidbody==null)
			myRigidbody = this.gameObject.GetComponent<Rigidbody2D>();
		if(destinyArea==null)
			destinyArea = new RectVector(0f,0f,0f,0f);
		if(myAnimator == null)
			myAnimator = playerGraphics.GetComponent<Animator>();
		if(myRenderer == null)
			myRenderer = playerGraphics.GetComponentInChildren<SpriteRenderer>();

		destinyArea.SetNewRect(mV3_PositionCenter.x - 0.5f,
		                       mV3_PositionCenter.x + 0.5f,
		                       mV3_PositionCenter.y + 0.5f,
		                       mV3_PositionCenter.y -0.5f);
		if(spriteControl == null)
		{
            // Use default
            //myAnimator.runtimeAnimatorController = IngameManager.GetInstance().mC_DefaultAnimator;
            Debug.Log("?????" + IngameManager.GetInstance().mC_DefaultSprite);
            myRenderer.sprite = IngameManager.GetInstance().mC_DefaultSprite;
		}
		else
		{
            // Change animator according to the key gived
            //myAnimator.runtimeAnimatorController = animControl;
            myRenderer.sprite = spriteControl;
        }

        myRenderer.material.mainTexture = myRenderer.sprite.texture;

        mB_CanInteract = true;
		//
		mB_CorrectDoor = false;
		mB_IgnoreColliders = false;
		mB_IsSaved  = false;
		mB_ObservePosition = false;

        myRenderer.sortingOrder = NormalLayer;
        myAnimator.enabled = true;

		myAnimator.Play("Idle");

        if (myRenderer.sprite.name == "Eva" || myRenderer.sprite.name == "Sari" || myRenderer.sprite.name == "Maria")
        {
            charaVoice.clip = charaVoiceWoman;
        }
        else
        {
            charaVoice.clip = charaVoiceMan;
        }

		this.transform.position = mV3_PositionCenter;

        GameManager.GetInstance().SavePlayerInfo();
	}

	public void Move(Vector2 direction)
	{
		if(mB_CanInteract && IngameManager.GetInstance().mB_isPlaying)
		{
			speed = direction*MovementSpeed;
			myRigidbody.velocity = speed;
			mB_CanInteract = false;
			mV3_InitialPosition = mV3_PositionCenter;//this.transform.position;
		}
	}

	public void Bounce()
	{
		Debug.Log("Bouncing!!!");
		myRigidbody.velocity *= -1f;
		mB_ObservePosition=true;
		destinyArea.SetNewRect( mV3_InitialPosition.x-0.5f,mV3_InitialPosition.x+0.5f,
		                        mV3_InitialPosition.y+0.5f,mV3_InitialPosition.y-0.5f);
	}


    //ゲームオーバー
	void KillPlayer()
	{
        myRenderer.sortingOrder = DeadLayer;
        myAnimator.Play("Dead");
		SwapAudio.GetInstance().Play("Death");
		

		myRigidbody.velocity = Vector3.zero;

#if !UNITY_EDITOR
        RankingManager.Instance.GetMyRankingDate();
        RankingManager.Instance.GetRankingData();
#endif

		// Notify Ingame Manager
		IngameManager.GetInstance().EndGame();
	}

	public void RevivePlayer() {
		Initialize(IngameManager.GetInstance ().mC_CurrentSprite);
	}


	public bool mB_CorrectDoor = false;
	public DIRECTION doorCollisionDirection;
	public bool mB_IgnoreColliders = false;

	public bool mB_IsSaved = false;

	void OnTriggerEnter2D(Collider2D other) 
	{
		if(mB_IgnoreColliders)
			return;

		//Door & Obstacle
		if(other.gameObject.layer == 8)
		{
			SwapDoor door = other.GetComponent<SwapDoor>();
			if(door.mB_Transitable)
			{
				if(door.mB_Obstacle)
				{
                    if (!mB_IsSaved)
                    // Kill Player
                    KillPlayer();
                    else
                        Bounce();
				}
				else
				{
					// Enter Room succesfully
					mB_CorrectDoor = true;
					doorCollisionDirection = door.doorDirection;
					//Now, the transition is made when the player is completly hidded
					//IngameManager.GetInstance().GoToNextRoom();
				}
			}

		}
		else if(other.gameObject.layer == 9)
		{
			if(mB_IsSaved)
				return;
			Debug.Log("Obstacle HIT");
			KillPlayer();
			//TEST : This fixed the game over when player touches lava while changing room?
			mB_IgnoreColliders = true;
			return;
		}
		//Wall
		else if(other.gameObject.layer == 10)
		{
			if(!mB_CorrectDoor)
			{
				//Debug.Log("Walls, its bouncing time");
				Bounce();
			}
		}
		//Hidder
		else if(other.gameObject.layer == 11)
		{
			SetScale(Vector3.zero);
			myAnimator.enabled = false;//Stop();

			//TEST : This fixed the game over when player touches lava while changing room?
			mB_IgnoreColliders = true;
			if(TutorialManager.doingTutorial)
			{
				TutorialManager.TutorialStep++;
				if(TutorialManager.TutorialStep>=4)
					WindowManager.GetInstance().TutorialWindow.EndTutorial();
			}
			IngameManager.GetInstance().GoToNextRoom(doorCollisionDirection);
			return;
		}
		// Saver
		else if(other.gameObject.layer == 12)
		{
			mB_IsSaved = true;
		}
	}

	public void SetScale(Vector3 scale)
	{
		this.transform.localScale = scale;
	}

	void FixedUpdate()
	{
		if(mB_ObservePosition)
		{
			if(destinyArea.IsInside(this.transform.position))
			{
				this.transform.position = mV3_InitialPosition;
				mB_CanInteract = true;
				mB_ObservePosition=false;
				speed = Vector2.zero;
				myRigidbody.velocity = speed;

				if(mB_IgnoreColliders)
				{
					mB_IgnoreColliders = false;
					mB_CorrectDoor     = false;
					IngameManager.GetInstance().FinishPlayerEnteringRoom();

					//
					if(!IngameManager.GetInstance().mB_Inmunity)
						mB_IsSaved = false;
				}
				myAnimator.Play("Idle");
			}
		}
	}

	[ContextMenu("DeletePrefs")]
	void DeletePrefs() {
		PlayerPrefs.DeleteAll ();
		Debug.Log ("Deleting prefs");
	}
}

