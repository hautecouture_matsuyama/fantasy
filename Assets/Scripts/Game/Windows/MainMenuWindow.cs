﻿using UnityEngine;
using System.Collections;

public class MainMenuWindow : WindowBase 
{
	public GameObject SplashScreenGO;

	public LeTweensBase LogoTween;

	public SwapButton PlayButton;

	public LeTweensBase[] ButtonsLeTween;

	public TweenPosition RemoveAdsTween;


    [SerializeField]
    private GameObject StartCanvas;



	public void ResetAll()
	{
		LogoTween.transform.localScale = Vector3.zero;
		short it=0;
		foreach(LeTweensBase data in ButtonsLeTween)
		{
			if(it==0)
			{
				PlayButton.CanTween = false;
				data.transform.localScale = Vector3.zero;
			}
			else
				data.transform.position = data.PositionsPoins[0];
			it++;
		}
		RemoveAdsTween.transform.position = RemoveAdsTween.from;
	}

	public override void Initialize ()
	{
		base.Initialize ();
	}

	bool oneShoot = true;
	EventDelegate del;
	public override void OnEnter ()
	{
		base.OnEnter ();
		ResetAll();
		SplashScreenGO.SetActive(true);

		if(oneShoot)
			del = new EventDelegate(ButtonsLeTween[0],"LeTween");
		oneShoot = false;
		ButtonsLeTween[0].mT_Scale.AddOnFinished( del );

		StartCoroutine("Tweens");
	}

	IEnumerator Tweens()
	{
		LogoTween.OnEnter();
		yield return new WaitForSeconds(0.4f);
		ButtonsLeTween[0].OnEnter();
		yield return new WaitForSeconds(0.3f);
		ButtonsLeTween[1].OnEnter();
		yield return new WaitForSeconds(0.3f);
		ButtonsLeTween[2].OnEnter();
		yield return new WaitForSeconds(0.2f);
		if(!RemoveAdsTween.playedReverse)
			RemoveAdsTween.ResetToBeginning();
		RemoveAdsTween.PlayForward();

		PlayButton.myTweener.from = PlayButton.transform.localScale;
		PlayButton.myTweener.to   = Vector3.one * 0.65f;
		if(!PlayButton.myTweener.playedReverse)
			PlayButton.myTweener.ResetToBeginning();
		PlayButton.CanTween = true;
	}

	public void EndTween()
	{
		ButtonsLeTween[0].mT_Scale.RemoveOnFinished(del);
	}

	public override void OnExit ()
	{
		SplashScreenGO.SetActive(false);
		base.OnExit();
	}

	public override void Close ()
	{
		base.Close ();
	}

	public void OpenCharSelection()
	{
		if(GameManager.HasFinishedLoading)
		{
			//SwapAudio.GetInstance().Play("Button");
			WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.SHOP);
			Close();
		}
	}

	public void OpenOptions()
	{
		if(GameManager.HasFinishedLoading)
		{
            //SwapAudio.GetInstance().Play("Button");
            //WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.OPTIONS);

            //AudioListener.volume = 0;

		}
	}

	bool firstRun = true;
	public void PlayGame()
	{
		if(GameManager.HasFinishedLoading)
		{
            Debug.Log("Start");
            StartCanvas.SetActive(false);

            BannerScripts.Instance.HideBanner();

            //SwapAudio.GetInstance().Play("Button");
            if (!firstRun)
			{
				EntityManager.GetInstance().DespawnAllPools();
				IngameManager.GetInstance().ResetWorld();
			}
			firstRun = false;
			IngameManager.GetInstance().Initialize();
			Close();
		}
	}
}
