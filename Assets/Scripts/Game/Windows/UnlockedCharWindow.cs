﻿using UnityEngine;
using System.Collections;

public class UnlockedCharWindow : WindowBase 
{

	public Animator NewCharAnimator;
	public UILabel  NewCharNameLabel;

	public TweenColor EntryColor;

	public override void Initialize ()
	{
		base.Initialize ();
		this.gameObject.SetActive(false);
	}

	public override void OnEnter ()
	{
		base.OnEnter ();

		EntryColor.gameObject.GetComponent<UISprite>().color = EntryColor.from;
		if(!EntryColor.playedReverse)
			EntryColor.ResetToBeginning();
		EntryColor.PlayForward();

		// Get New Character

		// Set New Character
		NewCharAnimator.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
		LeanTween.alpha(NewCharAnimator.gameObject,1f,EntryColor.duration);
        //NewCharAnimator.runtimeAnimatorController = GameManager.lastUnlockedCharacter;
        NewCharAnimator.gameObject.GetComponent<SpriteRenderer>().sprite = GameManager.lastUnlockedCharacter;
        NewCharAnimator.Play("Idle");

		string name = NewCharAnimator.gameObject.GetComponent<SpriteRenderer>().sprite.name;
		/*
		int it=0;
		foreach(char c in name)
		{
			if(char.IsUpper(c))
			{
				if(it == 0)
				{
					it++;
					continue;
				}
			}
			it++;
		}
		*/
		NewCharNameLabel.text = name.ToUpper();
	}

	public override void OnExit ()
	{
		base.OnExit ();
	}

	public override void Close ()
	{
		base.Close ();
	}

	public void ReplayGame()
	{
		EntityManager.GetInstance().DespawnAllPools();
		Close();
		IngameManager.GetInstance().ResetWorld();
		//WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);
		IngameManager.GetInstance().Initialize();
	}
}
