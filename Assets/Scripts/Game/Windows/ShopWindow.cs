﻿using UnityEngine;
using System.Collections;

//using SantaAssets;

public class ShopWindow : WindowBase 
{
	public TweenColor TWEEN_BG;

	public override void Initialize ()
	{
		base.Initialize ();
	}

	public override void OnEnter ()
	{
		base.OnEnter ();
		if(!TWEEN_BG.playedReverse)
			TWEEN_BG.ResetToBeginning();
		TWEEN_BG.PlayForward();
	}

	public override void OnExit ()
	{
		base.OnExit ();
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);
	}

	public void BuyPowerUpPack()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().CallStore( CONSTANTS.POWER_PACK );
	}

	public void RemoveAds()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().CallStore( CONSTANTS.NO_ADS );
	}

	public void BuyAllChars()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().CallStore( CONSTANTS.ALL_CHARS );
	}

	public void WomboCombo()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().CallStore( CONSTANTS.ALL_CHARS_NO_ADS );
	}

	public override void Close ()
	{
		//SwapAudio.GetInstance().Play("Button");
		base.Close ();
	}

	public void RestorePurchases()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().RestorePurchases();
	}
}
