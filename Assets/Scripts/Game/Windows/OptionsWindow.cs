﻿using UnityEngine;
using System.Collections;

public class OptionsWindow : WindowBase 
{
	public TweenPosition BackButtonTween;
	public TweenPosition AllButtonsTween;
	public TweenColor BlackBg;

	//public UISprite MusicButton;
	public UISprite SoundsButton;

	public UIButton Music;
	public UIButton Sound;

    [SerializeField]
    private AudioListener audio;


    string musicKey;
    string soundKey;

    public override void Initialize ()
	{
		base.Initialize ();
		CloseDel = new EventDelegate(this,"Close");

		//Calculate screen coordinates
		Vector3 from = UICamera.mainCamera.ViewportToWorldPoint(new Vector3(-0.15f,0.85f,0f));
		Vector3 to   = UICamera.mainCamera.ViewportToWorldPoint(new Vector3(0.15f,0.85f,0f));

//		Debug.Log(from);
		BackButtonTween.transform.position = to;

		BackButtonTween.to   = BackButtonTween.transform.localPosition;
		BackButtonTween.transform.position = from;
		BackButtonTween.from = BackButtonTween.transform.localPosition;

		from = UICamera.mainCamera.ViewportToWorldPoint(new Vector3(0.5f,1.5f,0f));
		AllButtonsTween.transform.position = from;
		AllButtonsTween.from = AllButtonsTween.transform.localPosition;

	}

	public override void OnEnter ()
	{
		base.OnEnter ();

		if(!BlackBg.playedReverse)
			BlackBg.ResetToBeginning();
		BlackBg.PlayForward();
		if(!BackButtonTween.playedReverse)
			BackButtonTween.ResetToBeginning();
		BackButtonTween.PlayForward();
		if(!AllButtonsTween.playedReverse)
			AllButtonsTween.ResetToBeginning();
		AllButtonsTween.PlayForward();

        if (GameManager.GetInstance().mB_Music)
        {
			Debug.Log("Music On");
			//MusicButton.spriteName = "music_on";
			//Music.normalSprite = "music_on";
			Music.pressedSprite= "music_on";
		}
		else
		{
			//MusicButton.spriteName = "music_off";
			//Music.normalSprite = "music_off";
			Music.pressedSprite= "music_off";
        }
        
		if(GameManager.GetInstance().mB_Sounds)
		{
			Debug.Log("Sound On");
			SoundsButton.spriteName = "sound_on";
			Sound.normalSprite = "sound_on";
			Sound.pressedSprite= "sound_on";
		}
		else
		{
			SoundsButton.spriteName = "sound_off";
			Sound.pressedSprite= "sound_off";
			Sound.pressedSprite= "sound_off";
		}
        
	}

	public override void OnExit ()
	{
		AllButtonsTween.RemoveOnFinished( CloseDel );
	}

	public override void Close ()
	{
		base.Close ();
	}

	EventDelegate CloseDel;
	public void StartHiding()
	{
		BlackBg.PlayReverse();
		BackButtonTween.PlayReverse();

		AllButtonsTween.AddOnFinished( CloseDel );
		AllButtonsTween.PlayReverse();
	}


    public void MusicButton()
    {
        audio.enabled = false;
    } 


	public void TurnMusic()
	{
		if(GameManager.GetInstance().TurnMusic())
		{
            PlayerPrefs.SetString("musicKey", "on");
            PlayerPrefs.SetString("soundKey", "on");
            musicKey = "on";
            soundKey = "on";
        }
        else
        {
            //GameManager.GetInstance().mB_Music = true;
            PlayerPrefs.SetString("musicKey", "off");
            PlayerPrefs.SetString("soundKey", "off");
            soundKey = "off";
            musicKey = "off";
        }

        //MusicButton.spriteName = "music_"+ musicKey;
		//Music.normalSprite = "music_"+ musicKey;
		//sMusic.pressedSprite= "music_"+ musicKey;


    }
    
	public void TurnSounds()
	{
        if (GameManager.GetInstance().TurnSounds())
        {
            PlayerPrefs.SetString("soundKey", "on");
            soundKey = "on";
        }
        else
        {
            //GameManager.GetInstance().mB_Music = true;
            PlayerPrefs.SetString("soundKey", "off");
            soundKey = "off";
        }

        SoundsButton.spriteName = "sound_"+ soundKey;
		Sound.normalSprite = "sound_"+ soundKey;
		Sound.pressedSprite= "sound_"+ soundKey;
	}
    
}
