using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameOverWindow : WindowBase 
{

	public const int VIDEO_REWARD = 10;
	public const int FIRST_VIDEO_REWARD = 50;

	public static GameOverWindow Instance;

	public Animator mC_CharAnimator;

	public TweenColor BLACK_TWEEN;

	public LeTweensBase ScoreLeTween;
	public TweenScale mT_CharArea;
	//public TweenScale mT_ScoreAre

	public TweenScale mT_HighScoreScale;
	public TweenRotation mT_HighScoreRotation;

	public UILabel mL_Score;
	public UILabel mL_HighScore;

	public UILabel mLabel_CoinsLabel;
    public UILabel mLabel_PickCoinsLabel;

    public LeTweensBase NewHighscoreAnimationLeTween;

	public BoxCollider NewCharCollider;
	public LeTweensBase CoinsScaleTween;
	public TweenScale mT_CoinsTween;
	public TweenPosition mT_CoinProgressPosition;
	public TweenFill mT_CoinsFill;
	public UISprite mUI_UnlockButton;
    /*
	public BoxCollider mC_GiftCollider;
	public LeTweensBase GiftLeTween;
	public LeTweensBase GiftLeTween2;
	public TweenScale mT_GiftScale;
	public TweenScale mT_GiftScale2;
	public UILabel    mL_GiftLabel;
	public UISprite   mS_GiftSprite;
	public TweenScale mT_GiftButtonScale;
    */
	public TweenScale    NewCharEffectScale;
	public TweenRotation NewCharEffectRotation;

	public LeTweensBase[] mT_ButtonsTween;

	public PROMOTION_TYPE giftType = PROMOTION_TYPE.NONE;

	public Transform MaxPosition;
	public float progressBaseOrigin;
	public float progressBaseMax;

	public UILabel UnlockLabel;

    //ゲームオーバーのCanvas(uGUI製)
    [SerializeField]
    private GameObject GameOverCanvas;

    [SerializeField]
    private Text BestText;

    [SerializeField]
    private Text NowText;


	bool newHighscore = false;
    public bool isGameOver;

    void OnEnable()
    {
        mLabel_CoinsLabel.text = mLabel_PickCoinsLabel.text;
        currMoney = IngameManager.GetInstance().mI_Coins;
        isGameOver = true;

        GameOverCanvas.SetActive(true);
        //BannerScripts.Instance.ShowBanner();
    }


	public override void Initialize ()
	{
		base.Initialize ();
		Instance = this;
		this.gameObject.SetActive(false);

		progressBaseOrigin	= mT_CoinsFill.gameObject.transform.localPosition.x;
		progressBaseMax	 	= MaxPosition.transform.localPosition.x;

		mT_CoinProgressPosition.from = mT_CoinsFill.transform.position;
		mT_CoinProgressPosition.to   = MaxPosition.position;
	}

	void SetAllTweensToOriginalPosition()
	{
		mT_CharArea.transform.localScale		= mT_CharArea.from; 
		//mT_ScoreArea.transform.localScale		= mT_ScoreArea.from;
		mT_CoinsTween.transform.localScale		= mT_CoinsTween.from;
		//mT_GiftScale.transform.localScale		= Vector3.zero;//mT_GiftScale.from;
		//mT_GiftScale2.transform.localScale		= Vector3.zero;//mT_GiftScale.from;
		mT_CoinsTween.transform.localScale      = Vector3.zero;

		NewCharEffectScale.transform.localScale = NewCharEffectScale.from;

		NewHighscoreAnimationLeTween.transform.localScale = Vector3.zero;

		foreach(LeTweensBase data in mT_ButtonsTween)
		{
			data.transform.position = data.PositionsPoins[0];
		}
	}

	int   prevMoney = 0;
	int   currMoney = 0;
	float from = 0f;
	float to   = 0f;
	public override void OnEnter ()
	{
		base.OnEnter ();

		SwapAudio.GetInstance().Resume("Menu");
		SwapAudio.GetInstance().Stop("Gameplay");


        if (WindowManager.NoTweens)
		{

			WindowManager.NoTweens = false;
			return;
		}

		if(!BLACK_TWEEN.playedReverse)
			BLACK_TWEEN.ResetToBeginning();
		BLACK_TWEEN.PlayForward();

		NewCharCollider.enabled = false;
		SetAllTweensToOriginalPosition();

        //temporary x01
		if(IngameManager.GetInstance().mC_CurrentSprite == null)
			mC_CharAnimator.gameObject.GetComponent<SpriteRenderer>().sprite = IngameManager.GetInstance().mC_DefaultSprite;
		else
			mC_CharAnimator.gameObject.GetComponent<SpriteRenderer>().sprite = IngameManager.GetInstance().mC_CurrentSprite;
		mC_CharAnimator.Play("Idle");

		/*
		mT_CharArea.from = Vector3.zero;
		mT_CharArea.to   = Vector3.one * 1.15f;
		mT_CharArea.AddOnFinished( new EventDelegate(this,"ScoreBounce") );
		if(!mT_CharArea.playedReverse)
			mT_CharArea.ResetToBeginning();
		mT_CharArea.PlayForward();
		*/

		//if(!mT_ScoreArea.playedReverse)
		//	mT_ScoreArea.ResetToBeginning();
		//mT_ScoreArea.PlayForward();

		//mT_HighScoreRotation.enabled = false;
		//mT_HighScoreScale.gameObject.transform.localScale = Vector3.zero;

		int score = IngameManager.GetInstance().mI_Score;

#if !UNITY_EDITOR
        RankingManager.Instance.SendRanking(score);
#endif        
        
        //mL_Score.text = score.ToString();
        NowText.text = score.ToString();
		int temp = IngameManager.GetInstance().mI_HighScore;
		newHighscore = false;
		if(score > temp)
		{
			newHighscore = true;
			IngameManager.GetInstance().mI_HighScore = score;
			temp = score;
			//TODO: PLAY NEW HIGHSCORE ANIMATION
			//mT_HighScoreScale.ResetToBeginning();
			//mT_HighScoreScale.PlayForward();

			GameManager.GetInstance().UpdateHighScore(temp);
		}
        //mL_HighScore.text  = "BEST: "+temp.ToString();

        BestText.text = temp.ToString();

        //UnlockLabel.text = GameManager.UNLOCK_MONEY.ToString();

        //prevMoney = IngameManager.GetInstance().mI_PrevMoney;
        //currMoney = IngameManager.GetInstance().mI_Coins;
        //from = (float)prevMoney/(float)GameManager.UNLOCK_MONEY;//(float)((1 / CONSTANTS.UNLOCK_MONEY
        //to   = (float)currMoney/(float)GameManager.UNLOCK_MONEY;//(float)((1 / CONSTANTS.UNLOCK_MONEY

        //mLabel_CoinsLabel.text = prevMoney.ToString();

        //mT_CoinsFill.gameObject.GetComponent<UISprite>().fillAmount = from;
        //mT_CoinsFill.from	= from;
        //mT_CoinsFill.to		= to;

        /*
		if(prevMoney >= GameManager.UNLOCK_MONEY)//CONSTANTS.UNLOCK_MONEY
		{
			mUI_UnlockButton.spriteName = "progress_bar_button_gift";
			mT_GiftButtonScale.PlayForward();
			NewCharCollider.enabled = true;
			NewCharEffectScale.PlayForward();
			//
			mT_CoinProgressPosition.gameObject.SetActive(false);
		}
		else
		{
			mUI_UnlockButton.spriteName = "progress_bar_button_empty";
			mT_CoinProgressPosition.gameObject.SetActive(true);
			NewCharCollider.enabled = false;
			mT_GiftButtonScale.enabled = false;
			mT_GiftButtonScale.transform.localScale = mT_GiftButtonScale.from;
		}
		if(GameManager.UNLOCK_MONEY != 0) {
			float proFrom = Math2D.Map(prevMoney,0f,(float)GameManager.UNLOCK_MONEY,progressBaseOrigin,progressBaseMax);
			float proTo   = Math2D.Map(currMoney,0f,(float)GameManager.UNLOCK_MONEY,progressBaseOrigin,progressBaseMax);
			mT_CoinProgressPosition.from.x = proFrom;
			mT_CoinProgressPosition.to.x   = proTo;
			Debug.Log ("proFrom: " + proFrom + " proTo: " + proTo + " prevMoney: " + prevMoney + " curMoney: " + currMoney + " unlockMoney: " + GameManager.UNLOCK_MONEY +
				" pbo: " + progressBaseOrigin + " pbm: " + progressBaseMax);
			
			mT_CoinProgressPosition.gameObject.transform.localPosition = mT_CoinProgressPosition.from;
		}
        */

        //mT_NewCharColorTween.enabled = false;
        //showGiftUI = AdManagementSystem.GetInstance ().VideoAvailable ();

        StartCoroutine("FirstBlockTween");

	}

	bool showGiftUI = true;

	IEnumerator FirstBlockTween()
	{
		SwapAudio.GetInstance().Play("Menu");
		ScoreLeTween.OnEnter();
		yield return new WaitForSeconds(0.6f);
		CoinsScaleTween.OnEnter();
	}

	public void EndScoreTweens()
	{
		Debug.LogWarning("End Score Tween");
		if(newHighscore)
		{
			SwapAudio.GetInstance().Play("Record");
			NewHighscoreAnimationLeTween.OnEnter();
		}
		//CoinsScaleTween.OnEnter();
	}

	void DelayedCoinProgressAudio()
	{
		SwapAudio.GetInstance().Play("CoinProgress");
	}

	public void EndCoinsScale()
	{
        /*
		Debug.LogWarning("End Coins Scale Tween");
		if(!mT_CoinsFill.playedReverse)
			mT_CoinsFill.ResetToBeginning();
		if(mT_CoinsFill.to != mT_CoinsFill.from)
			Invoke("DelayedCoinProgressAudio",0.3f);
		mT_CoinsFill.PlayForward();

		if(!mT_CoinProgressPosition.playedReverse)
			mT_CoinProgressPosition.ResetToBeginning();
		mT_CoinProgressPosition.PlayForward();

		//mLabel_CoinsLabel.text = GameManager.GetInstance().mI_Money.ToString();
		executeNextTween = true;
                */

        float updateRate = ( (float)currMoney - (float)prevMoney )/ 1.35f;
		updateRate = 1.35f / updateRate;
		//float updateCount = 0.75f / 0.02f;
		increment = 1;//Mathf.FloorToInt(  ((float)currMoney - (float)prevMoney ) / updateCount);
		counter = prevMoney;
		InvokeRepeating("UpdateCoinsFillLabel",0.3f,updateRate);
        

		StartCoroutine("AfterFill");
	}

	IEnumerator AfterFill()
	{
		//if(!showGiftUI)
		//{
			yield return new WaitForSeconds(0.8f);
			EndGiftTweens();
		/*
        }
		else
		{
			yield return new WaitForSeconds(0.8f);
			GiftLeTween.OnEnter();
			yield return new WaitForSeconds (0.8f);
			GiftLeTween2.OnEnter ();
			//if(!mT_GiftScale.playedReverse)
			//	mT_GiftScale.ResetToBeginning();
			//mT_GiftScale.PlayForward();
		}
        */
	}

	bool executeNextTween = false;
	public void EndCoinsFill()
	{
		Debug.LogWarning("End Coins Fill Tween");
		//SHOW OTHER STUFF
		if(!executeNextTween)
			return;

		if(IngameManager.GetInstance().mI_Coins >= GameManager.UNLOCK_MONEY)
		{
			NewCharCollider.enabled = true;
			mUI_UnlockButton.spriteName = "progress_bar_button_gift";
			mT_CoinProgressPosition.gameObject.SetActive(false);
			//mT_GiftButtonScale.PlayForward();
			NewCharEffectScale.PlayForward();
			//mT_NewCharColorTween.PlayForward();
		}

		/*
		if(!showGiftUI)
		{
			EndGiftTweens();
		}
		else
		{
			GiftLeTween.OnEnter();
			//if(!mT_GiftScale.playedReverse)
			//	mT_GiftScale.ResetToBeginning();
			//mT_GiftScale.PlayForward();
		}
		*/
	}

	public void EndGiftTweens()
	{
        /*
		if (mT_GiftScale.playedReverse)
			return;
		Debug.LogWarning("End Gift Tween");
		mC_GiftCollider.enabled = true;
		//if(!mT_ButtonsTween.playedReverse)
		//	mT_ButtonsTween.ResetToBeginning();
		//mT_ButtonsTween.PlayForward();
        */

		StartCoroutine("ShowButtonsDelayed");
	}

	IEnumerator ShowButtonsDelayed()
	{
		mT_ButtonsTween[0].OnEnter();
		yield return new WaitForSeconds(0.3f);
		mT_ButtonsTween[1].OnEnter();
		yield return new WaitForSeconds(0.3f);
		mT_ButtonsTween[2].OnEnter();
		yield return new WaitForSeconds (0.3f);

        if (RateAppManager.GetInstance().CheckTimer())
        {
            RateAppManager.GetInstance().ShowPanel();
        }
	}

	public void EndAllTweens()
	{
		Debug.Log ("SHOW ADS: " + GameManager.SHOW_ADS);
		if(GameManager.SHOW_ADS)
		{
			//GameManager.GetInstance ().ChartboostIntersitial ();
			// Called when the user can leave this scene
//			AdManagementSystem.GetInstance().PlayInterstitialFromRandomProvider();
		}
	}

	public void TakePromo()
	{
        /*
		//SwapAudio.GetInstance().Play("Button");
		if(giftType == PROMOTION_TYPE.GIFT)
		{
			// SHOW TOMBOLA
			// * Show tombola game object

			// * Invoke ShowTombolaPrize when tombola animation ends
			int amount = VIDEO_REWARD;
			if(GameManager.FIRST_VIDEO == 0)
			{
				amount = FIRST_VIDEO_REWARD;
				GameManager.FIRST_VIDEO = 1;
			}
			UpdateMoney( amount );

			GiftLeTween.OnExit();
			mC_GiftCollider.enabled = false;
		}
		else if(giftType == PROMOTION_TYPE.RATE)
		{
			GameManager.GetInstance().SendToRate();
			UpdateMoney(150);
			mT_GiftScale.PlayReverse();
		}
		else if(giftType == PROMOTION_TYPE.WATCH)
		{
			
			if(!GameManager.GetInstance().CanSeeVideoReward())
			{
				Debug.LogError("No video is avaible now :(");
				//Invoke Modal Window
				return;
			}
			
			Debug.Log("Watched video");
			mC_GiftCollider.enabled = false;
			GameManager.GetInstance().SetDelegate( new EventDelegate(this,"RewardVideo") );

			//Chartbost videos were used before
			//GameManager.GetInstance().ChartboostVideoReward();

			//AdManagementSystem.GetInstance().PlayVideoFromRandomProvider();

			GiftLeTween.OnExit();
			//mT_GiftScale.PlayReverse();
		}
		else//NONE
		{
			Debug.LogError("This buttons shouldnt be enable if no promo can be picked");
		}
        */
	}

	void ShowTombolaPrize()
	{
		int randomCoinsPrize = Random.Range(20,51) * 5;
		Debug.Log("Money prize = "+randomCoinsPrize);
		UpdateMoney(randomCoinsPrize);
		GameManager.GetInstance().SetNotification("You can win more money now!");
	}
    /*
	public void PlayVideo(VideoRewardType rewardType) {
		if(rewardType == VideoRewardType.Coins) {
			GiftLeTween2.OnExit();
		}
		if(rewardType == VideoRewardType.Life) {
			GiftLeTween.OnExit();
		}
		AdProvidersEditor.SetRewardForVideo (rewardType);
		AdManagementSystem.GetInstance().PlayVideoFromRandomProvider();
	}
    */

	public void RewardVideo(int ordinary, int first)
	{
        /*
		Debug.Log("Video reward has been gived");
		int amount = ordinary;
		if(GameManager.FIRST_VIDEO == 0)
		{
			amount = first;
			GameManager.FIRST_VIDEO = 1;
		}
		UpdateMoney( amount );
        */
	}

	public void UnlockNewChar()
	{
		// SHOW CANDY MACHINE (CHAR)
		//mT_NewCharColorTween.enabled = false;
		//mT_NewCharColorTween.gameObject.GetComponent<UISprite>().color = mT_NewCharColorTween.from;

		NewCharCollider.enabled = false;
		if(GameManager.GetInstance().unlockedChars < CONSTANTS.CHAR_UNLOCK_KEYS.Length)
			IngameManager.GetInstance().SpendMoney(GameManager.UNLOCK_MONEY);
		//UpdateMoney(0);
		//Invoke("NewCharMachine",1f);
		NewCharMachine();
	}

	void NewCharMachine()
	{
		// Unlock new char here
		Dictionary<string,Sprite> tempDic = GameManager.GetInstance().getAvaibleCharacters();
		//List<int> indexes = new List<int>();
		//foreach(KeyValuePair<int,RuntimeAnimatorController> kvp in tempDic)
		//	indexes.Add(kvp.Key);

		//int random = Random.Range(0,indexes.Count);

		if(GameManager.GetInstance().unlockedChars >= CONSTANTS.CHAR_UNLOCK_KEYS.Length)//if(indexes.Count <= 0)
		{
			GameManager.GetInstance().GoogleAnalyticsLog("Unlock","All_Unlocked");
			return;
		}

		string unlockKey = CONSTANTS.CHAR_UNLOCK_KEYS[ GameManager.GetInstance().unlockedChars ];

		Sprite newCharAnimator = tempDic[ unlockKey ];

		GameManager.lastUnlockedCharacter = newCharAnimator;

		string nChar = newCharAnimator.name;
		GameManager.GetInstance().UnlockCharacter(nChar,unlockKey);
		GameManager.GetInstance().GoogleAnalyticsLog("Unlock",nChar);

		SwapAudio.GetInstance().Play("Prize");
		// Set New Char animation in front of the screen
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.UNLOCKED);
		Close();
	}

	void UpdateMoney(int addition)
	{
		prevMoney = IngameManager.GetInstance().mI_Coins;
		IngameManager.GetInstance().AddMoney(addition);
		currMoney = IngameManager.GetInstance().mI_Coins;

        /*
		if(currMoney >= GameManager.UNLOCK_MONEY)
		{
			mUI_UnlockButton.spriteName = "progress_bar_button_gift";
			mT_GiftButtonScale.PlayForward();
			NewCharCollider.enabled = true;
			NewCharEffectScale.PlayForward();
			//
			mT_CoinProgressPosition.gameObject.SetActive(false);
		}
        */

		from = mT_CoinsFill.to;
		to   = Mathf.Clamp01( (float)currMoney/(float)GameManager.UNLOCK_MONEY );
		
		mT_CoinsFill.gameObject.GetComponent<UISprite>().fillAmount = from;
		mT_CoinsFill.from	= from;
		mT_CoinsFill.to		= to;

		float proFrom = Math2D.Map(prevMoney,0f,(float)GameManager.UNLOCK_MONEY,progressBaseOrigin,progressBaseMax);
		float proTo   = Math2D.Map(currMoney,0f,(float)GameManager.UNLOCK_MONEY,progressBaseOrigin,progressBaseMax);
		mT_CoinProgressPosition.from.x = proFrom;
		mT_CoinProgressPosition.to.x   = proTo;

		executeNextTween = false;
		if(!mT_CoinsFill.playedReverse)
			mT_CoinsFill.ResetToBeginning();

		if(!mT_CoinProgressPosition.playedReverse)
			mT_CoinProgressPosition.ResetToBeginning();

		mLabel_CoinsLabel.text = currMoney.ToString();


		if(prevMoney != currMoney)
			Invoke("DelayedCoinProgressAudio",0.3f);
		mT_CoinsFill.PlayForward();
		mT_CoinProgressPosition.PlayForward();
	}

	public override void OnExit ()
	{
		base.OnExit ();
	}

	public override void Close ()
	{
		base.Close ();
	}

	public void OpenStore()
	{
		//SwapAudio.GetInstance().Play("Button");
		Close();
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.SHOP);
	}

	public void Replay()
	{
		//SwapAudio.GetInstance().Play("Button");
		EntityManager.GetInstance().DespawnAllPools();
		Close();
		IngameManager.GetInstance().ResetWorld();
		//WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);
		IngameManager.GetInstance().Initialize();
        isGameOver = false;
        

        GameOverCanvas.SetActive(false);
        BannerScripts.Instance.HideBanner();
    }

	public void Continue() {
		IngameManager.GetInstance ().ContinueGame ();
		IngameManager.GetInstance ().playerController.RevivePlayer ();
		Close ();
//		var score = IngameManager.GetInstance ().mI_Score;
//		IngameManager.GetInstance ().mI_Score = score;
//		IngameManager.GetInstance ().mB_isPlaying = true;
//		UI_Manager.GetInstance ().UpdateScore (score);
//		IngameManager.GetInstance ().worldGen.NextRoom (score);
	}

	public void OpenCharacterPick()
	{
        //SwapAudio.GetInstance().Play("Button");
        GameOverCanvas.SetActive(false);
        Close();
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.PICK_CHARACTER);
	}

#region CONTROL_DELEGATES
	public void ScoreBounce()
	{
		mT_CharArea.onFinished.Clear();
		mT_CharArea.to   = mT_CharArea.from;
		mT_CharArea.from = Vector3.one;

		mT_CharArea.AddOnFinished(new EventDelegate(this,"EndScoreTweens"));
		mT_CharArea.PlayReverse();
	}

	int counter = 0;
	int increment = 1;
	void UpdateCoinsFillLabel()
	{
		counter += increment;
		if(counter >= currMoney)
		{
			counter = currMoney;
			CancelInvoke("UpdateCoinsFillLabel");
		}
		mLabel_CoinsLabel.text = counter.ToString();
	}

#endregion
}
