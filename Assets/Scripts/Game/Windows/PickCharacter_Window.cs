using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class PickCharacter_Window : WindowBase
{
	public TweenColor TWEEN_BG;

	public ScrollCharactersINPUT myInput;
	//public UIPanel ScrollPanel;

	public UISprite PickButton;

	public Sprite[] mA_AllCharactersSprites;	

	public TweenPosition ScrollTween;

	public List<PickPlayerInfo> mL_Chars = new List<PickPlayerInfo>();

	public int mI_CharIndex;

	public Transform ScrollParentObj;
	public GameObject ScrollBaseObj;

	public UIButton PickButtonButton;

	public UILabel PickedCharacterLabel;

    //public IngameManager _ingameManager;
    public int charaReleaseCoin;

    public UILabel mLabel_CoinsLabel;

    [SerializeField]
    private GameObject GameOverCanvas;

    [SerializeField]
    private GameObject SelectCanvas;

    [SerializeField]
    private Text cur_moneyText;



    public override void Initialize ()
	{
		base.Initialize ();
		mL_Chars.Clear();
		myInput.myWind = this;

        /*
		Vector4 newPanel = new Vector4(ScrollPanel.baseClipRegion.x,
		                               ScrollPanel.baseClipRegion.y,
		                               (float)mA_AllCharactersAnimators.Length * 287.5f,//* 250f
		                               ScrollPanel.baseClipRegion.w);
		ScrollPanel.baseClipRegion = newPanel;
		*/

        int it=0;
		foreach(Sprite data in mA_AllCharactersSprites)
		{
			Vector3 position = new Vector3( (140f * it),//(100f * it )
			                               35f,
			                               0f);

			if(it==0)
				myInput.MinimumCharPosition = position;
			else if( (it+1) == mA_AllCharactersSprites.Length)
				myInput.MaximumCharPostion = position;
			GameObject newObj = (GameObject)GameObject.Instantiate(ScrollBaseObj);
			newObj.transform.parent = ScrollParentObj;
			newObj.transform.localPosition = position;
			newObj.transform.localScale = Vector3.one * 35f;

			PickPlayerInfo info = newObj.GetComponent<PickPlayerInfo>();
			info.CenteredPostion = -position.x;
            //info.myAnimator.runtimeAnimatorController = data;
            info.myAnimator.gameObject.GetComponent<SpriteRenderer>().sprite = data;
            info.mI_PlayerIndex = it;

			newObj.SetActive(true);

			SpriteRenderer renderer = info.myAnimator.gameObject.GetComponent<SpriteRenderer>();
			if(GameManager.GetInstance().GetUnlockedChar(data.name) || it == 0)
			{
				renderer.color = Color.white;
				info.mB_IsEnabled = true;
				info.myAnimator.enabled = true;
			}
			else
			{
				renderer.color = Color.black;
                info.mB_IsEnabled = false;
			}

			mL_Chars.Add(info);

			it++;
		}
	}

    void OnEnable()
    {
        //mLabel_CoinsLabel.text = IngameManager.GetInstance().mI_Coins.ToString();
        cur_moneyText.text = IngameManager.GetInstance().mI_Coins.ToString();

        SelectCanvas.SetActive(true);
    }

    void OnDisable()
    {
        SelectCanvas.SetActive(false);
    }

	public override void OnEnter ()
	{
		base.OnEnter ();
	
		if(!TWEEN_BG.playedReverse)
			TWEEN_BG.ResetToBeginning();
		TWEEN_BG.PlayForward();
		Invoke("TurnAllCharacters",0.02f);
	}

	void TurnAllCharacters()
	{
		foreach(PickPlayerInfo data in mL_Chars)
		{
            //bool newState = GameManager.GetInstance().GetUnlockedChar(data.myAnimator.runtimeAnimatorController.name);
            bool newState = GameManager.GetInstance().GetUnlockedChar(data.myAnimator.gameObject.GetComponent<SpriteRenderer>().sprite.name);
            data.mB_IsEnabled = newState;
			data.myAnimator.enabled = data.mB_IsEnabled;
			if(data.mB_IsEnabled)
			{
				data.GetComponentInChildren<SpriteRenderer>().color = Color.white;
			}
			else
			{
				// CHANGE COLOR
				data.GetComponentInChildren<SpriteRenderer>().color = Color.black;
			}
		}
	}

	public void UpdateScroll(Vector3 delta)
	{
		if(delta == (Vector3.one * 200f))
			return;
		delta = -delta;
		Vector3 newPos = ScrollParentObj.transform.localPosition;
		newPos.x += delta.x;

		ScrollParentObj.transform.localPosition = newPos;
	}

	public void DropScroll()
	{
		PickPlayerInfo data = mL_Chars[mI_CharIndex];
		Vector3 target = new Vector3(data.CenteredPostion,ScrollTween.transform.localPosition.y,0f);
		if(-ScrollParentObj.transform.localPosition.x > myInput.MaximumCharPostion.x)
		{
			//Tween To las Charactwer

			//return;
		}
		else if(-ScrollParentObj.transform.localPosition.x < myInput.MinimumCharPosition.x)
		{
			//Tween to frist character
			//return;
		}

		//Tween to picked char
		ScrollTween.from = ScrollTween.transform.localPosition;
		ScrollTween.to   = target;

		if(!ScrollTween.playedReverse)
			ScrollTween.ResetToBeginning();
		ScrollTween.PlayForward();
	}

	public override void OnExit ()
	{
		base.OnExit ();
	}

	public override void Close ()
	{
		//SwapAudio.GetInstance().Play("Button");
		base.Close ();
		//WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);

		WindowManager.NoTweens = true;
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.GAME_OVER);

        GameOverCanvas.SetActive(true);

	}

    private bool canPick = true;
	public bool CanPick
    {
        get { return canPick; }
        set { canPick = value; }
    }

    //void SetPickCharacterView(bool value, PickCharacter_Window)
    //{
    //    if(value)
    //    {
    //        mI_CharIndex = info.mI_PlayerIndex;
    //        PickButton.spriteName = "play_button";
    //        PickButtonButton.normalSprite = "play_button";
    //        PickButton.width = 100;
    //        PickButton.height = 110;
    //    }
    //}

	public string currentPickedCharacter = string.Empty;
	public void PickPlayer()
	{
		//SwapAudio.GetInstance().Play("Button");
		if(CanPick)
		{

			EntityManager.GetInstance().DespawnAllPools();
			IngameManager.GetInstance().ResetWorld();

			IngameManager.GetInstance().mC_CurrentSprite = mA_AllCharactersSprites[mI_CharIndex];
			GameManager.GetInstance().GoogleAnalyticsLog("Equiped",IngameManager.GetInstance().mC_CurrentSprite.name);
			WindowManager.GetInstance().HideWindow(myWindowType);
			IngameManager.GetInstance().Initialize();
		}
		else
		{
            //解放するお金が足りない時は解放しない
            if (charaReleaseCoin > IngameManager.GetInstance().mI_Coins)
            {
                return;
            }

            //smLabel_CoinsLabel.text = (int.Parse(mLabel_CoinsLabel.text) - charaReleaseCoin).ToString();
            cur_moneyText.text = (int.Parse(cur_moneyText.text) - charaReleaseCoin).ToString();

            IngameManager.GetInstance().SpendMoney(charaReleaseCoin);
            GameManager.GetInstance().UnlockCharacter(mA_AllCharactersSprites[mI_CharIndex].name,mA_AllCharactersSprites[mI_CharIndex].name);

            /*
			currentPickedCharacter = mA_AllCharactersSprites[mI_CharIndex].name;
			Debug.Log("Char to buy = "+currentPickedCharacter);
			GameManager.charToUnlock = currentPickedCharacter;
			GameManager.GetInstance().CallStore(CONSTANTS.ONE_CHAR);
            */
        }
	}



	public void UnlockAll()
	{
		//SwapAudio.GetInstance().Play("Button");
		GameManager.GetInstance().CallStore(CONSTANTS.ALL_CHARS);
	}

    public void UnlockCharacter(string characterName)
    {
        PickPlayerInfo data = mL_Chars.Where(x => x.myAnimator.gameObject.GetComponent<SpriteRenderer>().sprite.name == characterName).FirstOrDefault();
        if (data != null && data.gameObject.activeInHierarchy)
        {
            bool temp = GameManager.GetInstance().GetUnlockedChar(characterName);
            data.GetComponentInChildren<SpriteRenderer>().color = Color.white;
            data.mB_IsEnabled = true;
            data.myAnimator.enabled = true;

            PickButton.spriteName = "play_button";
            PickButtonButton.normalSprite = "play_button";
            PickButton.width = 100;
            PickButton.height = 110;
            CanPick = true;
        }

    }

}

