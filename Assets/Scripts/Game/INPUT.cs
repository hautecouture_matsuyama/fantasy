using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class INPUT : MonoBehaviour
{

	public enum UserAction
	{
		UA_INACTIVE,
		UA_TOUCHED,
		UA_DRAGGED,
		UA_DROPED
	};

	Vector2 InputPosition;
	public UserAction UserBehavior = UserAction.UA_INACTIVE;

	public Vector2 Offset;
	
	public Vector3 Touch_BeganPosition;
	public Vector3 Touch_DragPosition;
	public Vector3 Touch_DropPosition;


	void Update()
	{
#if UNITY_EDITOR_OSX || UNITY_EDITOR_64 || UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
			Swipe(MOVEMENT.UP);
		else if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
			Swipe(MOVEMENT.LEFT);
		else if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
			Swipe(MOVEMENT.RIGHT);
		else if(Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
			Swipe(MOVEMENT.DOWN);
#endif
		if( Application.platform == RuntimePlatform.WindowsEditor || 
		   Application.platform == RuntimePlatform.OSXEditor || 
		   Application.platform == RuntimePlatform.WindowsPlayer )
		{
			if( Input.GetButtonDown("Fire1") )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_TOUCHED;
			}
			else if( Input.GetMouseButton(0)   )
			{
				InputPosition = Input.mousePosition;
				UserBehavior = UserAction.UA_DRAGGED;
			}
			else if( Input.GetMouseButtonUp(0) )
				UserBehavior = UserAction.UA_DROPED;

			if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
				Swipe(MOVEMENT.LEFT);
			else if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
				Swipe(MOVEMENT.RIGHT);
			else if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
				Swipe(MOVEMENT.UP);
			else if(Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
				Swipe(MOVEMENT.DOWN);
		}
		else // iOS Touch Event
		{
			#if UNITY_IOS || UNITY_ANDROID
			foreach( Touch touch in Input.touches )
			{
				if ( touch.phase == TouchPhase.Began )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_TOUCHED;
				}
				else if( touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary )
				{
					InputPosition = touch.position;
					UserBehavior = UserAction.UA_DRAGGED;
				}
				else if( touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled ) 
				{
					UserBehavior = UserAction.UA_DROPED;
				}
			}
			#endif
		}

		if( UserBehavior != UserAction.UA_INACTIVE )
		{
			if(Camera.main == null)
			{
				Debug.Log("Theres no main camera");
				return;
			}
			
			//Vector3 vec = Camera.main.ScreenToWorldPoint(InputPosition);
			//rayTraced = Camera.main.ScreenPointToRay( InputPosition );

			switch(UserBehavior)
			{
			case UserAction.UA_TOUCHED:
				//raytraced
				Touch_BeganPosition = InputPosition;
				break;
			case UserAction.UA_DRAGGED:
				Touch_DragPosition = InputPosition;
				Swipe();
				break;
			case UserAction.UA_DROPED:
				Touch_DropPosition = Touch_DragPosition;
				moved = false;
				Touch_BeganPosition = Touch_DragPosition = Touch_DropPosition = Vector3.zero;
				break;
			}
			UserBehavior = UserAction.UA_INACTIVE;
		}
	}

	bool moved = false;
	void Swipe()
	{
		if(Touch_BeganPosition == Vector3.zero)
			return;
		float x = Math2D.GetRange(Touch_BeganPosition.x,Touch_DragPosition.x);
		float y = Math2D.GetRange(Touch_BeganPosition.y,Touch_DragPosition.y);
		 
		if( (x >= Offset.x || y >= Offset.y) && !moved)
		{
			if(y > x)
			{
				if( Touch_BeganPosition.y > Touch_DragPosition.y)
				{
					//Debug.Log("Down");
					IngameManager.GetInstance().UserAction(MOVEMENT.DOWN);
					moved = true;
				}
				else
				{
					//Debug.Log("Up");
					IngameManager.GetInstance().UserAction(MOVEMENT.UP);
					moved = true;
				}
			}
			else
			{
				if( Touch_BeganPosition.x > Touch_DragPosition.x)
				{
					//Debug.Log("Left");
					IngameManager.GetInstance().UserAction(MOVEMENT.LEFT);
					moved = true;
				}
				else
				{
					//Debug.Log("RIGHT");
					IngameManager.GetInstance().UserAction(MOVEMENT.RIGHT);
					moved = true;
				}
			}
			Touch_BeganPosition = Touch_DragPosition = Touch_DropPosition = Vector3.zero;
		}
		//Touch_BeganPosition = Touch_DragPosition = Touch_DropPosition = Vector3.zero;
	}

	void Swipe(MOVEMENT dir)
	{
		IngameManager.GetInstance().UserAction(dir);
	}

}

