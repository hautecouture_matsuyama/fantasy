﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class IngameManager : Singleton<IngameManager>
{

	protected IngameManager () {}

	public PlayerControl playerController;

	public WorldGenerator worldGen;

	public int mI_HighScore=0;
	public int mI_Score;

	public int mI_PrevMoney=0;
	public int mI_Coins;

	public bool mB_isPlaying=false;	

	public Sprite mC_DefaultSprite;

	public Sprite mC_CurrentSprite;

	[HideInInspector]
	public List<SwapEntity> mL_Entities = new List<SwapEntity>();

	public SpriteRenderer FloorRenderer;
	public SpriteRenderer WallsRenderer;

	private float Walls_Width = 1f;
	private float Walls_Height = 1f;

	private float Floor_Width = 1f;
	private float Floor_Height = 1f;

	public int mI_DeviceWidth  = 1;
	public int mI_DeviceHeight = 1;

	[HideInInspector]
	public float xScale;
	[HideInInspector]
	public float yScale;

	public Vector2 TestResolution;

    public GameManager _gameManager;

    
    //TEMP
    /*
	void Awake()
	{
		if(!GameManager.GetInstance().mB_LoadedFromBeginning)
		{
			Debug.Log("Ingame First");
			CalculateResolution();

			WindowManager.GetInstance().Initialize();
			WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);
			EntityManager.GetInstance().Initialize();
		
			UI_Manager.GetInstance().SetState(false);
		}
	}
	*/

    public PROMOTION_TYPE ReturnPromo()
	{
#if UNITY_EDITOR
		return PROMOTION_TYPE.GIFT;
#endif
        /*
		if(GameManager.GetInstance().VideoIterator() && AdManagementSystem.GetInstance().VideoAvailable())
		{
			return PROMOTION_TYPE.WATCH;
		}
        */
		return PROMOTION_TYPE.NONE;

		//return PROMOTION_TYPE.NONE;
		if(DateTime.Compare(DateTime.UtcNow,GameManager.GetInstance().NextGiftDate.AddMinutes( CONSTANTS.MINUTES_SURPRISE )) > 0)
		{
			Debug.Log("GIFT PROMO");
			return PROMOTION_TYPE.GIFT;
		}
		else
		{
			if(GameManager.GetInstance().CanSeeVideoReward())
				return PROMOTION_TYPE.WATCH;

			return PROMOTION_TYPE.NONE;
			// Check Promotion Video with priority over rate
			if(!GameManager.GetInstance().mB_Rated)
			{
				return PROMOTION_TYPE.RATE;
			}
			else
			{
				// * Should we have a timer or counter to have a reward every n times/seconds

				// Check video
				return PROMOTION_TYPE.WATCH;
			}
		}
		return PROMOTION_TYPE.NONE;
	}

	public void CalculateResolution()
	{

		if(TestResolution.x != 0f && TestResolution.y != 0f)
		{
			mI_DeviceWidth = (int)TestResolution.x;
			mI_DeviceHeight = (int)TestResolution.y;
		}
		else
		{
			mI_DeviceHeight = Screen.height;
			mI_DeviceWidth  = Screen.width;
			Debug.Log("No IOS device used, checking current resolution");
		}

		if(Screen.dpi > 320f)
		{
			// Use HD textures
		}
		else
		{
			// Use SD textures
		}


//		Debug.Log("Device Resolution Width  = "+ mI_DeviceWidth);
//		Debug.Log("Device Resolution Height = "+ mI_DeviceHeight);

		float worldScreenHeight = Camera.main.orthographicSize*2f;
		float worldScreenWidth  = worldScreenHeight/mI_DeviceHeight*mI_DeviceWidth;

		Walls_Width  = WallsRenderer.sprite.bounds.size.x;//WallsRenderer.sprite.texture.width
		Walls_Height = WallsRenderer.sprite.bounds.size.y;//WallsRenderer.sprite.texture.height;

		Floor_Width  = FloorRenderer.sprite.bounds.size.x;
		Floor_Height = FloorRenderer.sprite.bounds.size.y;

		LavaBehaviour lava = worldGen.mC_Lava;
		float lavaWidth = lava.myRenderer.sprite.bounds.size.x;
		//Vector3 lavaRes = Vector3.one;
		//lavaRes.x = worldScreenWidth / lavaWidth;
		//lavaRes.y = 0.5f;
		//lava.transform.localScale = lavaRes;

		Vector3 deviceRes = Vector3.one;
		deviceRes.x = worldScreenWidth / Walls_Width;
		deviceRes.y = worldScreenHeight / Walls_Height;

		xScale = (float)((float)mI_DeviceWidth / (float)Walls_Width);
		yScale = (float)((float)mI_DeviceHeight / (float)Walls_Height);

		WallsRenderer.transform.localScale = deviceRes;

		deviceRes.x = worldScreenWidth / Floor_Width;
		deviceRes.y = worldScreenHeight / Floor_Height;

		FloorRenderer.transform.localScale = deviceRes;
		/*
		WallsRenderer.transform.localScale = new Vector3(xScale,
		                                                 yScale,
		                                                 1f);
		FloorRenderer.transform.localScale = new Vector3(xScale,
		                                                 yScale,
		                                                 1f);
		*/
		worldGen.mC_InitialRoom.WallsSprite.transform.localScale = WallsRenderer.transform.localScale;
		worldGen.mC_InitialRoom.FloorSprite.transform.localScale = FloorRenderer.transform.localScale;
		//worldGen.mC_InitialRoom.BackgroundSprite.transform.localScale = FloorRenderer.transform.localScale;

		foreach(RoomInfo data in worldGen.mA_Rooms)
		{
			data.WallsSprite.transform.localScale = WallsRenderer.transform.localScale;
			data.FloorSprite.transform.localScale = FloorRenderer.transform.localScale;
			//data.BackgroundSprite.transform.localScale = FloorRenderer.transform.localScale;

			data.TopLeft_DecorationSpr.transform.parent = data.transform;
			data.TopRight_DecorationSpr.transform.parent = data.transform;
			data.BotLeft_DecorationSpr.transform.parent = data.transform;
			data.BotRight_DecorationSpr.transform.parent = data.transform;
		}
	}

	// Use this for initialization
	public void Initialize(bool justData = false)
	{
        if(justData)
        {
            mI_Coins = GameManager.GetInstance().mI_Money;
            UI_Manager.GetInstance().UpdateCoins(mI_Coins);
            mI_PrevMoney = mI_Coins;
            return;
        }


        mI_Coins = GameManager.GetInstance().mI_Money;
        
        mI_Score = 0;
		mB_isPlaying = true;
        SwapAudio.GetInstance().Pause("Menu");
		SwapAudio.GetInstance().Play("Gameplay",true);

		UI_Manager.GetInstance().Initialize(0);
		UI_Manager.GetInstance().SetState(true);

		mI_HighScore = GameManager.GetInstance().mI_HighScore;

		playerController.Initialize(mC_CurrentSprite);


		worldGen.Initialize();


		ClearPowerUps();

		if(!GameManager.ShowTutorial)
		{
			UI_Manager.GetInstance().ShowGO();
		}
	}

    void InitData()
    {

    }

	/// <summary>
	/// Relocates everything so when the game is restarted, it doesnt overlaps
	/// </summary>
	public void ResetWorld()
	{
		worldGen.mC_InitialRoom.transform.position = Vector3.zero;
		worldGen.mC_InitialRoom.WallsSprite.color = Color.white;
		//Reset first door sprite to null
		worldGen.mC_InitialRoom.NorthSprite.sprite = null;

		worldGen.mA_Rooms[0].transform.position = Vector3.left*100f;
		worldGen.mA_Rooms[1].transform.position = Vector3.right*100f;
		worldGen.mA_Rooms[2].transform.position = Vector3.down*150f;

		playerController.transform.position = playerController.mV3_PositionCenter;
		playerController.myAnimator.Play("Idle");
	}


	[HideInInspector]
	public DIRECTION previousDirection;
	public void GoToNextRoom(DIRECTION direction)
	{
		worldGen.mC_Lava.StopMovement();
		previousDirection = direction;
		TurnWalls(false);
		mI_Score++;
		UI_Manager.GetInstance().UpdateScore(mI_Score);
		worldGen.NextRoom(mI_Score);
	}

	public void FinishEnteringRoom()
	{
		if(worldGen.currentRoom == null)
			return;
		if(worldGen.currentRoom.mB_IsInitialRoom)
			return;

		Vector3 speed = Vector3.one;
		// Set Player location to door location
		// * Use previous direction to know in which door player should "spawn"
		if(previousDirection == DIRECTION.NORTH)
		{
			playerController.transform.position = worldGen.currentRoom.SouthSprite.transform.position;
			speed = Vector3.up * playerController.MovementSpeed;
		}
		else if(previousDirection == DIRECTION.EAST)
		{
			playerController.transform.position = worldGen.currentRoom.WestSprite.transform.position + playerController.mV3_InitialPosition;
			speed = Vector3.right * playerController.MovementSpeed;
		}
		else if(previousDirection == DIRECTION.SOUTH)
		{
			playerController.transform.position = worldGen.currentRoom.NorthSprite.transform.position;
			speed = Vector3.down * playerController.MovementSpeed;
		}
		else if(previousDirection == DIRECTION.WEST)
		{
			playerController.transform.position = worldGen.currentRoom.EastSprite.transform.position + playerController.mV3_InitialPosition;;
			speed = Vector3.left * playerController.MovementSpeed;
		}

		worldGen.TurnHidders(true);

		playerController.mB_IgnoreColliders = true;
		playerController.mB_ObservePosition = true;
		playerController.SetScale(Vector3.one * 0.4f);

		playerController.myAnimator.enabled = true;
		if(speed.x != 0)
		{
			//Debug.LogWarning("Resuming Animator Horizontal");
			playerController.myAnimator.Play("Swipe_Horizontal");
		}
		else 
		{
			//Debug.LogWarning("Resuming Animator Vertical");
			playerController.myAnimator.Play("Swipe_Vertical");
		}

		// Set movement speed to the center to the player
		speed *= 1.5f;
		playerController.speed = speed;
		playerController.myRigidbody.velocity = playerController.speed;
	}

	public void FinishPlayerEnteringRoom()
	{
		// Start room behaviour
		SwapAudio.GetInstance().Play("Closed");
		worldGen.StartRoomBehaviour();

		if(TutorialManager.doingTutorial)
		{
			WindowManager.GetInstance().TutorialWindow.NextTutorialStep();
		}
	}

	public void TurnWalls(bool state)
	{
		worldGen.TurnWallsColliders(state);
	}

	public void UserAction(MOVEMENT phase)
	{
		if(!mB_isPlaying)
			return;
		SwapAudio.GetInstance().Play("Swipe");
		switch(phase)
		{
		case MOVEMENT.DOWN:
			playerController.Move(Vector2.down);
			playerController.myAnimator.Play("Swipe_Vertical");
			break;
		case MOVEMENT.RIGHT:
			playerController.Move(Vector2.right);
			playerController.myAnimator.Play("Swipe_Horizontal");
			break;
		case MOVEMENT.UP:
			playerController.Move(Vector2.up);
			playerController.myAnimator.Play("Swipe_Vertical");
			break;
		case MOVEMENT.LEFT:
			playerController.Move(Vector2.left);
			playerController.myAnimator.Play("Swipe_Horizontal");
			break;
		}
	}
	
    //ゲームオーバー処理
	public void EndGame()
	{
		Debug.Log("Game has ended");
		mB_isPlaying = false;

		List<string> audios = SwapAudio.GetInstance().loopingAudios;
		if(audios.Contains("Icesaw") || audios.Contains("Water") )
		{
			SwapAudio.GetInstance().Stop("Icesaw");
			SwapAudio.GetInstance().Stop("Water");
		}

		worldGen.mC_Lava.FreezeLava();//StopMovement
		Invoke("ShowGameOverWindow",1.5f);


	}

	public void ContinueGame() {
		SwapAudio.GetInstance().Pause("Menu");
		SwapAudio.GetInstance().Play("Gameplay",true);

		UI_Manager.GetInstance().Initialize(0);
		UI_Manager.GetInstance().SetState(true);

		mI_HighScore = GameManager.GetInstance ().mI_HighScore;

		mB_isPlaying = true;
		float curve = 1f;
		if(IngameManager.GetInstance().mI_Score < 20)
		{
			curve = 0.85f;
		}
		float speed = 1.05f;
		// IF is room large make it slower?
		if(worldGen.mB_IsLargeRoom)
		{
			if( IngameManager.GetInstance().mB_DoingPowerUp ) 
			{
				if( IngameManager.GetInstance().mB_DoingFreezePowerUp)
					speed = 0f;
			}
			else
				speed = 0.95f;
		}

		speed *= curve;
		worldGen.mC_Lava.Initialize( IngameManager.GetInstance().previousDirection,0f,speed, worldGen.currentRoom.gameObject);
		UI_Manager.GetInstance().UpdateScore(mI_Score);
	}

	void ShowGameOverWindow()
	{
		UI_Manager.GetInstance().SetState(false);
		if(GameManager.ShowTutorial)
		{
			WindowManager.GetInstance().HideWindow(WINDOWSTYPES.TUTORIAL);
		}
		WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.GAME_OVER);

        StartCoroutine(ShowInterstitial());
        BannerScripts.Instance.ShowBanner();
        
        
	}

    IEnumerator ShowInterstitial()
    {

        yield return new WaitForSeconds(0.8f);

    }

	public void AddMoney(int amount)
	{
		if(amount<=0)
			return;
		mI_Coins += amount;
		UI_Manager.GetInstance().UpdateCoins(mI_Coins);

		// SAVE MONEY
		GameManager.GetInstance().UpdateMoney(mI_Coins);
	}

	public void SpendMoney(int amount)
	{
		if(mI_Coins - amount < 0)
		{
			Debug.LogError("Incorrect money amount (Amount="+amount+";Money="+mI_Coins+")");
			return;
		}
		mI_Coins -= amount;
		GameManager.GetInstance().UpdateMoney(mI_Coins);
	}

	/// <summary>
	/// Pauses the game so we can call the store
	/// </summary>
	public void StorePause()
	{
		Time.timeScale = 0f;
		GameManager.GetInstance().CallStore( CONSTANTS.POWER_PACK );
	}

	/// <summary>
	/// Resumes the game after going to the store
	/// </summary>
	public void StoreResume()
	{
		Time.timeScale = 1f;
	}

	#region POWER_UPS

	public void ClearPowerUps()
	{
		mB_DoingPowerUp = false;
		mB_DoingFreezePowerUp = false;
		mB_DoingSandClock = false;
		//???
		//playerController.mA_ShieldAnimator.gameObject.SetActive(false);
		mB_Inmunity = false;
	}

	public bool mB_DoingPowerUp;

	public bool mB_DoingFreezePowerUp = false;
	public void FreezePowerUp()
	{
		if(mB_DoingPowerUp)
			return;
		SwapAudio.GetInstance().Play("Freeze");
		mB_DoingFreezePowerUp = true;
		mB_DoingPowerUp = true;
		worldGen.mC_Lava.FreezeLava();
		GameManager.GetInstance().SpendFreeze();
		//Invoke("ResumeLavaMovement",2f);
	}

	void ResumeLavaMovement()
	{
		worldGen.mC_Lava.StartMoving();
	}

	public void AddEntity(SwapEntity entity)
	{
		if(mL_Entities.Contains(entity))
		{
			Debug.LogWarning("Entity ("+entity+") already added");
			return;
		}
		mL_Entities.Add(entity);
	}

	public void RemoveEntity(SwapEntity entity)
	{
		if(mL_Entities.Contains(entity))
		{
			mL_Entities.Remove(entity);
			return;
		}
		Debug.LogWarning("Entity ("+entity+") wasnt added");
	}

	public void RemoveEntities(List<SwapEntity> entities)
	{
		foreach(SwapEntity data in entities)
		{
			if(mL_Entities.Contains(data))
				mL_Entities.Remove(data);
			else 
				Debug.LogWarning("Entity ("+data+") wasnt added");
		}
	}

	public bool mB_DoingSandClock = false;
	public void SandClockPowerUp()
	{
		if(!mB_DoingPowerUp)
		{
			Debug.Log("///// SANDCLOCK POWER UP ACTIVATED //////");
			SwapAudio.GetInstance().Play("Slow");
			GameManager.GetInstance().SpendSandClock();
			mB_DoingPowerUp = true;
			mB_DoingSandClock = true;
			worldGen.mC_Lava.SandClockSpeed();
			if(worldGen.mC_ActiveHandler != null)
				worldGen.mC_ActiveHandler.SandClock(true);
			//Invoke("EndSandClock",20f);
		}
	}

	void EndSandClock()
	{
		Debug.Log("\\\\\\\\\\ SANDCLOCK POWER UP DEACTIVATED \\\\\\\\\\");
		mB_DoingPowerUp = false;
		mB_DoingSandClock = false;
		worldGen.mC_Lava.NormalSpeed();
		if(worldGen.mC_ActiveHandler != null)
			worldGen.mC_ActiveHandler.SandClock(false);
	}

	public bool mB_Inmunity = false;
	public void InmunityShield()
	{
		if(!mB_DoingPowerUp)
		{
			mB_DoingPowerUp = true;
			SwapAudio.GetInstance().Play("Shield");
			playerController.mB_IsSaved = true;
			GameManager.GetInstance().SpendShield();
			mB_Inmunity = true;
			playerController.mA_ShieldAnimator.gameObject.SetActive(true);
			playerController.mA_ShieldAnimator.Play("Activate");
			//Invoke("EndInmunity",5f);
		}
	}

	void EndInmunity()
	{
		mB_DoingPowerUp = false;
		playerController.mB_IsSaved = true;
		mB_Inmunity = false;
		playerController.mA_ShieldAnimator.Play("FadeOut");
		Invoke("DespawnShield",0.45f);
	}

	void DespawnShield()
	{
		playerController.mA_ShieldAnimator.gameObject.SetActive(false);
	}
	#endregion
}
