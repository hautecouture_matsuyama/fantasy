﻿using UnityEngine;
using System.Collections;

public class InputController : GameEntity 
{
	public Vector3 TouchPosition;
	public Vector3 DropPosition;

	public float DeltaX = 10f;
	public float DeltaY = 10f;

	public override void OnTouched (Vector3 touchPos)
	{
		base.OnTouched (touchPos);
		TouchPosition = touchPos;
	}

	public override void OnDropped (Vector3 dropPos)
	{
		base.OnDropped (dropPos);
		DropPosition = dropPos;
		//CheckSwipe();
		//CheckCircleCoordinatesSwipe();
		CheckSwipeOffsets();
	}

	public override void FixedUpdate ()
	{
		base.FixedUpdate ();

	}

#if UNITY_EDITOR
	void Update()
	{
		/*
		if(Input.GetKeyDown(KeyCode.UpArrow))
			PlayerManager.GetInstance().AttackUp();
		else if(Input.GetKeyDown(KeyCode.DownArrow))
			PlayerManager.GetInstance().AttackDown();
		else if(Input.GetKeyDown(KeyCode.LeftArrow))
			PlayerManager.GetInstance().AttackLeft();
		else if(Input.GetKeyDown(KeyCode.RightArrow))
			PlayerManager.GetInstance().AttackRight();*/
	}
#endif

	/// <summary>
	/// Checks the swipe.
	/// </summary>
	public void CheckSwipe()
	{
		float dx = Math2D.GetRange(TouchPosition.x,DropPosition.x);
		float dy = Math2D.GetRange(TouchPosition.y,DropPosition.y);


		//If dx is Lower than DeltaX means probably the input was highter in dy
		if ( dx < DeltaX )
		{
			if(dy > DeltaY )//The swipe was in Y
			{
				if( TouchPosition.y > DropPosition.y)
				{
					//Swip Down
					Debug.Log("DOWN");
					//PlayerManager.GetInstance().AttackDown();
					return;
				}
				else
				{
					//Swip Up
					Debug.Log("UP");
					//PlayerManager.GetInstance().AttackUp();
					return;
				}
			}
		}

		if( dy < DeltaY)//
		{
			if(dx > DeltaX )//The swipe was in Y
			{
				if( TouchPosition.x > DropPosition.x)
				{
					//Swip Left
					Debug.Log("LEFT");
					//PlayerManager.GetInstance().AttackLeft();
					return;
				}
				else
				{
					//Swip Right
					Debug.Log("RIGHT");
					//PlayerManager.GetInstance().AttackRight();
					return;
				}
			}
		}

		Debug.Log("Unknown SWIPE");
	}

	//Incomplete
	public void CheckCircleCoordinatesSwipe()
	{
		float dx = Math2D.GetRange(TouchPosition.x,DropPosition.x);
		float dy = Math2D.GetRange(TouchPosition.y,DropPosition.y);

		Vector3 tempVector = new Vector3(TouchPosition.x + dx,
		                                 TouchPosition.y + dy,
		                                 TouchPosition.z );



		TouchPosition = Vector3.zero;
		DropPosition  = Vector3.zero;
	}


	public void CheckSwipeOffsets()
	{
		float dx = Math2D.GetRange(TouchPosition.x,DropPosition.x);
		float dy = Math2D.GetRange(TouchPosition.y,DropPosition.y);

		//Swipe was in Y
		if(dy > dx)
		{
			//Check if the deltaY was higher than the required to be considered touch
			if(dy > DeltaY)
			{
				if( TouchPosition.y > DropPosition.y)
				{
					Debug.Log("Down");
					//PlayerManager.GetInstance().AttackDown();
				}
				else
				{
					Debug.Log("Up");
					//PlayerManager.GetInstance().AttackUp();
				}
			}
		}
		//Swipe was in x
		else
		{
			//Check if the deltaX is highter to the required to be considered touch
			if(dx > DeltaX)
			{
				if( TouchPosition.x > DropPosition.x)
				{
					Debug.Log("Left");
					//PlayerManager.GetInstance().AttackLeft();
				}
				else
				{
					Debug.Log("RIGHT");
					//PlayerManager.GetInstance().AttackRight();
				}
			}
		}

		TouchPosition = Vector3.zero;
		DropPosition  = Vector3.zero;
	}


}
