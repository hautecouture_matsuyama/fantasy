﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwapAudio : Singleton<SwapAudio> 
{

	protected SwapAudio () { }

	public AudioSource[] mL_AllSources;

	public Dictionary<string,AudioSource> mD_Sources;



	public void Initialize()
	{
		mD_Sources = new Dictionary<string, AudioSource>();
		foreach(AudioSource data in mL_AllSources)
		{
			mD_Sources.Add(data.gameObject.name,data);
			//Debug.Log(data.gameObject.name);
		}
	}

	public void SetMusicVolume(float volume = 100f)
	{
		volume = volume * 0.01f;
		mD_Sources["Gameplay"].volume = volume;
		mD_Sources["Menu"].volume = volume;
	}

    public void SetFXVolume(float volume = 100f)
    {
        volume = volume * 0.01f;
        foreach (KeyValuePair<string, AudioSource> kvp in mD_Sources)
        {
            if (kvp.Key != "Gameplay" && kvp.Key != "Menu")
            {
                kvp.Value.volume = volume;
            }
        }
    }

    /*
    public void SetFXVolume(float volume = 100f)
	{
		volume = volume * 0.01f;
		foreach(KeyValuePair<string,AudioSource> kvp in mD_Sources)
		{
			if(kvp.Key == "Gameplay" || kvp.Key == "Menu")
                kvp.Value.volume = 0.4f;
            continue;

			if(kvp.Key == "Open" || kvp.Key == "Slow" || kvp.Key == "Freeze" || kvp.Key == "Shield")
				kvp.Value.volume = 0.72f;
			else
				kvp.Value.volume = volume;
		}
	}
    */

	public void Pause(string key)
	{
		mD_Sources[key].Pause();
	}

	public void Resume(string key)
	{
		mD_Sources[key].UnPause();
	}

	public void Play(string key,bool loop = false)
	{
		mD_Sources[key].loop = loop;
		if(loop && !loopingAudios.Contains(key))
			loopingAudios.Add(key);
        //        Debug.Log("Playing " + key + " " + mD_Sources[key].volume);
        //Debug.Log("Play" + key);
        mD_Sources[key].Play();
	}

	public List<string> loopingAudios = new List<string>();
	public void Stop(string key)
	{
		if(loopingAudios.Contains(key))
		{
			loopingAudios.Remove(key);
		}
        //Debug.Log("Stop" + key);
		mD_Sources[key].Stop();
	}	

}
