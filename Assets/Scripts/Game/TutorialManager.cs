﻿using UnityEngine;
using System.Collections;

public class TutorialManager : WindowBase
{
	protected TutorialManager () {}

	public static int TutorialStep = 0;
	public static bool doingTutorial = false;

	public TweenPosition[] TutorialSwipes;

	public void Initialize()
	{

	}

	public void ShowTutorial()
	{
		this.gameObject.SetActive(true);
		NextTutorialStep();
	}

	public override void OnEnter ()
	{
		base.OnEnter ();
		doingTutorial = true;
		TutorialStep = 0;
		NextTutorialStep();
	}

	public void NextTutorialStep()
	{
		switch(TutorialStep)
		{
		case 0:
			TutorialSwipes[0].gameObject.SetActive(true);
			TutorialSwipes[0].ResetToBeginning();
			TutorialSwipes[0].PlayForward();
			break;
		case 1:
			TutorialSwipes[0].gameObject.SetActive(false);
			TutorialSwipes[1].gameObject.SetActive(true);
			TutorialSwipes[1].ResetToBeginning();
			TutorialSwipes[1].PlayForward();
			break;
		case 2:
			TutorialSwipes[1].gameObject.SetActive(false);
			TutorialSwipes[2].gameObject.SetActive(true);
			TutorialSwipes[2].ResetToBeginning();
			TutorialSwipes[2].PlayForward();
			break;
		case 3:
			TutorialSwipes[2].gameObject.SetActive(false);
			TutorialSwipes[3].gameObject.SetActive(true);
			TutorialSwipes[3].ResetToBeginning();
			TutorialSwipes[3].PlayForward();
			break;
		case 4:
			EndTutorial();
			break;
		}
		Debug.Log("Tutorial Step ="+TutorialStep);
		//TutorialStep++;
	}

	public void EndTutorial()
	{
		doingTutorial = false;
		TutorialSwipes[0].gameObject.SetActive(false);
		TutorialSwipes[1].gameObject.SetActive(false);
		TutorialSwipes[2].gameObject.SetActive(false);
		TutorialSwipes[3].gameObject.SetActive(false);
		Close();
		GameManager.ShowTutorial = false;
		GameManager.GetInstance().SavePlayerInfo();
		UI_Manager.GetInstance().ShowGO();
	}

	public override void OnExit ()
	{
		TutorialSwipes[0].gameObject.SetActive(false);
		TutorialSwipes[1].gameObject.SetActive(false);
		TutorialSwipes[2].gameObject.SetActive(false);
		TutorialSwipes[3].gameObject.SetActive(false);
		base.OnExit ();
	}
}
