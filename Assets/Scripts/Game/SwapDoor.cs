using UnityEngine;
using System.Collections;

public class SwapDoor : SwapEntity
{

	/// <summary>
	/// This flag says if the player will bounce or interact with the door
	/// </summary>
	public bool mB_Transitable = false;

	int currentState=-1;

	public DIRECTION doorDirection;

	public Animator SuperiorAnim;

	public bool mB_IsInvisible;

	public override void Initialize (string key, DIRECTION dir)
	{
		base.Initialize (key);

		if(string.IsNullOrEmpty( mS_EntityKey ) )
			mS_EntityKey = key;

		doorDirection = dir;

		if(!mB_IsInvisible)
		{
			mB_Transitable = false;
			mC_myCollider.enabled = false;
		}
		else
		{
			mB_Transitable = true;
			mC_myCollider.enabled = true;
		}



	}

	/// <summary>
	/// Sets the state of the door
	/// 0 -> Idle
	/// 1 -> Opening
	/// 2 -> Open
	/// </summary>
	/// <param name="state">State.</param>
	public override void SetState(int state)
	{
		if(mB_IsInvisible)
		{
			mB_Transitable = true;
			mC_myCollider.enabled = true;
			return;
		}

		currentState = state;
		if(state == 0)
		{
			mC_Animator.Play(mS_IdleKey);
			SuperiorAnim.Play(mS_IdleKey);
			mC_Animator.SetBool("Opened",false);
		}
		else if(state == 1)
		{
			mC_Animator.Play("Shaking");
			SuperiorAnim.Play("Shaking");
			//mC_Animator.Play(mS_OpeningKey);
		}
		else if(state == 2)
		{
			mC_Animator.Play(mS_OpeningKey);
			SuperiorAnim.Play(mS_OpeningKey);
			mB_Transitable = true;
			mC_myCollider.enabled = true;
		}
		else if(state == 3)
		{
			// New opened
			mC_Animator.Play(mS_OpenKey);
			SuperiorAnim.Play(mS_OpenKey);
			mB_Transitable = true;
			mC_myCollider.enabled = true;
		}
		else if (state >= 100)
		{
			// Obstacle Door on Enter Character
			if(mB_Obstacle)
			{
				mC_Animator.Play("EnterRoom");
				//SuperiorAnim.Play("");
				mB_Transitable = true;//false
				mC_myCollider.enabled = true;
			}
			else
			{
				mC_Animator.Play(mS_OpenKey);
				SuperiorAnim.Play(mS_OpenKey);
				mB_Transitable = true;
				mC_myCollider.enabled = true;
			}
		}
		else
		{
			Debug.LogError("Invalid Argument sent ("+state+")");
		}
	}

	public override void SetTimer(float t)
	{
		base.SetTimer(t);

		SetState(0);
		//Invoke("StartOpeningDoor",mF_ActionTimer);
	}

	void StartOpeningDoor()
	{
		SetState(1);
	}

	public override void Despawn ()
	{
		base.Despawn ();
	}

	public override void StartBehaviour()
	{
		base.StartBehaviour();

		if(currentState==3 || currentState >= 100)
			return;

		Invoke("StartOpeningDoor",mF_ActionTimer);
	}

	bool shakePlayed = false;
	bool audioPlayed = false;
	public override void FixedUpdate ()
	{
		if(currentState < 3)
		{
			if(mC_Animator != null)
			{
				if(mC_Animator.GetCurrentAnimatorStateInfo(0).IsName("Shaking"))
				{
					if(!shakePlayed)
					{
						shakePlayed = true;
						SwapAudio.GetInstance().Play("Shake");
					}
				}
				else
					shakePlayed = false;
				if(mC_Animator.GetCurrentAnimatorStateInfo(0).IsName("Opening"))
				{
					if(!mB_Obstacle && !audioPlayed)
					{
						Debug.LogWarning("Open Audio Played");
						SwapAudio.GetInstance().Play("Open");
						audioPlayed = true;
					}
					/*else
						SoundManager.GetInstance().PlaySound("Closed");*/
					mB_Transitable = true;
					mC_myCollider.enabled = true;
				}
				else
					audioPlayed = false;

				if(mC_Animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
				{
				//if(mC_Animator.GetBool("Opened"))
					SetState(3);
				}
			}
		}

		/*
		if(currentState==2 && !mB_Obstacle )
		{

		}
		*/
	}
}

