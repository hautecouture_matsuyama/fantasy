using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
	public Animator myAnimator;

	RectVector myDestiny;

	bool taken = false;
	public void Take()
	{
		//TODO : ADD AUDIO
		IngameManager.GetInstance().AddMoney(5);
		SwapAudio.GetInstance().Play("Coin");
		myAnimator.Play("Taken");
		Invoke("Despawn",1.033f);
	}

	public void Initialize()
	{
		taken = false;
	}

	void Despawn()
	{
		this.transform.position = Vector3.up * 50f;
	}

	void FixedUpdate()
	{
		if(myDestiny == null)
			myDestiny = new RectVector(-0.25f,0.25f,0.25f,-0.25f);

		if(myDestiny.IsInside(this.transform.position))
		{
			if(!taken)
			{
				Take();
				taken = true;
			}
		}
	}
}

