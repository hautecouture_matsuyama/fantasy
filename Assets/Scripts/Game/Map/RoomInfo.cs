using UnityEngine;
using System.Collections.Generic;

public class RoomInfo : MonoBehaviour
{
	public DIRECTION doorDirection;

	StateTween myCurrentState = StateTween.ENTER;

	public bool mB_IsInitialRoom;

	public bool mB_isLarge = false;

	public TweenPosition myTween;

	public SpriteRenderer NorthSprite;
	public SpriteRenderer EastSprite;
	public SpriteRenderer SouthSprite;
	public SpriteRenderer WestSprite;

	public Transform NorthPosition;
	public Transform EastPosition;
	public Transform SouthPosition;
	public Transform WestPosition;
	
	public SpriteRenderer TopLeft_DecorationSpr;
	public SpriteRenderer TopRight_DecorationSpr;
	public SpriteRenderer BotLeft_DecorationSpr;
	public SpriteRenderer BotRight_DecorationSpr;

	List<SpriteRenderer> allRenderers = new List<SpriteRenderer>();

	public DoorSequence mC_NorthDoor;
	public DoorSequence mC_EastDoor;
	public DoorSequence mC_SouthDoor;
	public DoorSequence mC_WestDoor;

	public SpriteRenderer WallsSprite;
	public SpriteRenderer FloorSprite;
	//public SpriteRenderer BackgroundSprite;

	public List<SwapEntity> mL_AllEntities = new List<SwapEntity>();

	public Transform[] mA_TrapPositions;	

	public Blur myBlur;

	public int mI_CurrentRoomId = -1;

	public void Initialize(Sprite north,Sprite east,Sprite south,Sprite west, float oN,float oE,float oS,float oW)
	{
		NorthSprite.sprite = north;
		EastSprite.sprite  = east;
		SouthSprite.sprite = south;
		WestSprite.sprite  = west;

		if(allRenderers.Count == 0)
		{
			allRenderers.Add(NorthSprite);
			allRenderers.Add(EastSprite);
			allRenderers.Add(SouthSprite);
			allRenderers.Add(WestSprite);
		}
	}

	public void Initialize(DoorSequence north,DoorSequence east,DoorSequence south,DoorSequence west, Vector3 originPoint)
	{
		NorthSprite.sprite = north.mC_doorSprite;
		EastSprite.sprite  = east.mC_doorSprite;
		SouthSprite.sprite = south.mC_doorSprite;
		WestSprite.sprite  = west.mC_doorSprite;

		mC_NorthDoor = north;
		mC_EastDoor  = east;
		mC_SouthDoor = south;
		mC_WestDoor  = west;

		if(allRenderers.Count == 0)
		{
			allRenderers.Add(NorthSprite);
			allRenderers.Add(EastSprite);
			allRenderers.Add(SouthSprite);
			allRenderers.Add(WestSprite);

			NorthSprite.transform.position = NorthPosition.position;
			EastSprite.transform.position  = EastPosition.position;
			SouthSprite.transform.position = SouthPosition.position;
			WestSprite.transform.position  = WestPosition.position;
		}

		myTween.from = originPoint;
		myTween.to   = Vector3.zero;

		//
		this.transform.position = originPoint;

		if(north.mB_Active && !north.mB_IsTrap)
			doorDirection = DIRECTION.NORTH;
		else if(south.mB_Active && !south.mB_IsTrap)
			doorDirection = DIRECTION.SOUTH;
		else if(east.mB_Active && !east.mB_IsTrap)
			doorDirection = DIRECTION.EAST;
		else if(west.mB_Active && !west.mB_IsTrap)
			doorDirection = DIRECTION.WEST;
		else
			Debug.LogError("Unknown direction");

		//mL_AllEntities.Clear();
		mD_Doors.Clear();

		ClearDecoration();
	}	

	public void ClearDecoration()
	{
		TopLeft_DecorationSpr.sprite = null;
		TopRight_DecorationSpr.sprite = null;
		BotRight_DecorationSpr.sprite = null;
		BotLeft_DecorationSpr.sprite = null;
	}

	public void SetDecoration(Sprite topR,Sprite topL,Sprite botR,Sprite botL)
	{
		TopLeft_DecorationSpr.sprite = topL;
		TopRight_DecorationSpr.sprite = topR;
		BotLeft_DecorationSpr.sprite = botL;
		BotRight_DecorationSpr.sprite = botR;
	}

	public void SetRoomSprites(Sprite walls, Sprite floor)
	{
		WallsSprite.sprite = walls;
		FloorSprite.sprite = floor;
	}

	public void SetRoomOrientation(Vector3 orientation)
	{
		FloorSprite.gameObject.transform.eulerAngles = orientation;
		WallsSprite.gameObject.transform.eulerAngles = orientation;
	}

	public void SetRoomOrientation(DIRECTION prevRoomDir)
	{
		Vector3 orientation = Vector3.zero;

		if(prevRoomDir == DIRECTION.SOUTH)
			orientation = Vector3.forward*180f;
		else if(prevRoomDir == DIRECTION.EAST)
			orientation = Vector3.forward*-90f;
		else if(prevRoomDir == DIRECTION.WEST)
			orientation = Vector3.forward*90f;

		/*
		FloorSprite.gameObject.transform.eulerAngles = orientation;
		WallsSprite.gameObject.transform.eulerAngles = orientation;
		 */
	}

	public Vector3 GetTrapPosition(DIRECTION dir)
	{
		return mA_TrapPositions[(int)dir].position;
	}

	public Vector3 GetLocalTrapPosition(DIRECTION dir)
	{
		return mA_TrapPositions[(int)dir].localPosition;
	}

	public void Enter()
	{
		myCurrentState = StateTween.ENTER;

		DIRECTION prevDir = IngameManager.GetInstance().previousDirection;
		if(prevDir == DIRECTION.NORTH || 
		   prevDir == DIRECTION.SOUTH)
			myTween.duration = CONSTANTS.VerticalTransitionSpeed;
		else
			myTween.duration = CONSTANTS.HorizontalTransitionSped;

		if(!myTween.playedReverse)
			myTween.ResetToBeginning();
		myTween.PlayForward();


		WallsSprite.color = CONSTANTS.EnterWhite;
		//										   0.3
		LeanTween.alpha( WallsSprite.gameObject,1f,0.5f).setEase(LeanTweenType.easeOutQuint);
		//myBlur.ActivateBlur();
	}

	Dictionary<string,SwapDoor> mD_Doors = new Dictionary<string, SwapDoor>();

	public void SetState(StateTween s)
	{
		myCurrentState = s;
	}

	public float FirstDoorOpenDelay = 1f;

	public void FinishTween()
	{
		if(myCurrentState == StateTween.ENTER)
		{ 
			if(mB_IsInitialRoom)
			{
				GameObject obj = EntityManager.GetInstance().Pools[mC_NorthDoor.mS_DoorKey].Spawn();
				SwapDoor  door = obj.GetComponent<SwapDoor>();
				
				door.Initialize(mC_NorthDoor.mS_DoorKey,DIRECTION.NORTH);
				obj.transform.position = NorthSprite.transform.position;
				obj.transform.eulerAngles = Vector3.zero;
				door.SetState(3);
				mL_AllEntities.Add(door);
				mD_Doors.Add("North",door);
				return;
			}

			FirstDoorOpenDelay = Random.Range(1f,1.9f);//0.8f,1.7f
			float delay = FirstDoorOpenDelay;
			// Spawn Doors and turn off current sprites
			// * Spawn Doors
			if(mC_NorthDoor.mB_Active)
			{
				GameObject obj = EntityManager.GetInstance().Pools[mC_NorthDoor.mS_DoorKey].Spawn();
				SwapDoor  door = obj.GetComponent<SwapDoor>();

				door.Initialize(mC_NorthDoor.mS_DoorKey,DIRECTION.NORTH);
				obj.transform.position = NorthSprite.transform.position;
				obj.transform.eulerAngles = Vector3.zero;
				//Set timer or animation
				if(mC_NorthDoor.mF_OpenTimer < 0f)
				{
					door.SetState(150);//3
				}
				else
				{
					door.SetTimer(0.1f);//delay
					delay += 0.2f;
				}
				mL_AllEntities.Add(door);
				mD_Doors.Add("North",door);
			}
			if(mC_SouthDoor.mB_Active)
			{
				GameObject obj = EntityManager.GetInstance().Pools[mC_SouthDoor.mS_DoorKey].Spawn();
				SwapDoor  door = obj.GetComponent<SwapDoor>();
				
				door.Initialize(mC_SouthDoor.mS_DoorKey, DIRECTION.SOUTH);
				obj.transform.position = SouthSprite.transform.position;
				obj.transform.eulerAngles = Vector3.forward*180f;
				//Set timer or animation
				if(mC_SouthDoor.mF_OpenTimer < 0f)
				{
					door.SetState(150);//3
				}
				else
				{
					door.SetTimer(0.1f);//delay
					delay += 0.2f;
				}
				
				mL_AllEntities.Add(door);
				mD_Doors.Add("South",door);
			}
			if(mC_WestDoor.mB_Active)
			{
				GameObject obj = EntityManager.GetInstance().Pools[mC_WestDoor.mS_DoorKey].Spawn();
				SwapDoor  door = obj.GetComponent<SwapDoor>();
				
				door.Initialize(mC_WestDoor.mS_DoorKey,DIRECTION.WEST);
				obj.transform.position = WestSprite.transform.position;
				obj.transform.eulerAngles = Vector3.forward*90f;
				//Set timer or animation
				if(mC_WestDoor.mF_OpenTimer < 0f)
				{
					door.SetState(150);//3
				}
				else
				{
					door.SetTimer(0.1f);//delay
					delay += 0.2f;
				}
				
				mL_AllEntities.Add(door);
				mD_Doors.Add("West",door);
			}
			if(mC_EastDoor.mB_Active)
			{
				GameObject obj = EntityManager.GetInstance().Pools[mC_EastDoor.mS_DoorKey].Spawn();
				SwapDoor  door = obj.GetComponent<SwapDoor>();
				
				door.Initialize(mC_EastDoor.mS_DoorKey,DIRECTION.EAST);
				obj.transform.position = EastSprite.transform.position;
				obj.transform.eulerAngles = -Vector3.forward*90f;
				//Set timer or animation
				if(mC_EastDoor.mF_OpenTimer < 0f)
				{
					door.SetState(150);//3
				}
				else
				{
					door.SetTimer(0.1f);//delay
					delay += 0.2f;
				}
				
				mL_AllEntities.Add(door);
				mD_Doors.Add("East",door);
			}

			// * Turn Off current sprites
			foreach(SpriteRenderer data in allRenderers)
				data.sprite = null;

			/*
			foreach(SwapEntity data in mL_AllEntities)
			{
				(SwapDoor)data
			}
			*/
			IngameManager.GetInstance().TurnWalls(true);
			if(!mB_IsInitialRoom)
				IngameManager.GetInstance().FinishEnteringRoom();
		}
		else
		{
			// * Add to pool

		}

		if(myBlur != null)
			myBlur.DeactivateBlur();
	}

	public void Exit(Vector3 position)
	{
		myCurrentState = StateTween.EXIT;

		if(mC_NorthDoor.mB_Active)
		{
			if(mD_Doors["North"].gameObject.GetComponent<SpriteRenderer>().enabled)
				NorthSprite.sprite = mD_Doors["North"].gameObject.GetComponent<SpriteRenderer>().sprite;
		}
		if(mC_SouthDoor.mB_Active)
		{
			if(mD_Doors["South"].gameObject.GetComponent<SpriteRenderer>().enabled)
				SouthSprite.sprite = mD_Doors["South"].gameObject.GetComponent<SpriteRenderer>().sprite;
		}
		if(mC_EastDoor.mB_Active)
		{
			if(mD_Doors["East"].gameObject.GetComponent<SpriteRenderer>().enabled)
				EastSprite.sprite = mD_Doors["East"].gameObject.GetComponent<SpriteRenderer>().sprite;
		}
		if(mC_WestDoor.mB_Active)
		{
			if(mD_Doors["West"].gameObject.GetComponent<SpriteRenderer>().enabled)
				WestSprite.sprite = mD_Doors["West"].gameObject.GetComponent<SpriteRenderer>().sprite;
		}

		foreach(SwapEntity data in mL_AllEntities)
		{
			data.Despawn();
		}
		mL_AllEntities.Clear();
		mD_Doors.Clear();

		DIRECTION prevDir = IngameManager.GetInstance().previousDirection;
		if(prevDir == DIRECTION.NORTH || 
		   prevDir == DIRECTION.SOUTH)
			myTween.duration = CONSTANTS.VerticalTransitionSpeed;
		else
			myTween.duration = CONSTANTS.HorizontalTransitionSped;

		if(mB_IsInitialRoom)
		{
			myTween.from = Vector3.zero;
			myTween.to   = Vector3.down * WorldGenerator.y_Threshold;
			myCurrentState = StateTween.EXIT;
			if(!myTween.playedReverse)
				myTween.ResetToBeginning();
			myTween.PlayForward();

			LeanTween.alpha( WallsSprite.gameObject,CONSTANTS.BlurredWhite.a,0.2f).setEase(LeanTweenType.easeOutQuint);

			mB_IsInitialRoom = false;

			if(myBlur != null)
				myBlur.ActivateBlur();
			return;
		}

		//GET A DESTINY
		myTween.from = Vector3.zero;
		myTween.to   = position;
		if(!myTween.playedReverse)
			myTween.ResetToBeginning();
		myTween.PlayForward();

		LeanTween.alpha( WallsSprite.gameObject,CONSTANTS.BlurredWhite.a,0.2f).setEase(LeanTweenType.easeOutQuint);

		if(myBlur != null)
			myBlur.ActivateBlur();
	}

	public void StartBehaviour()
	{
		foreach(SwapEntity data in mL_AllEntities)
		{
			data.StartBehaviour();
		}
	}
}

[System.Serializable]
public class DoorSequence
{
	/// <summary>
	/// The m c_door sprite.
	/// </summary>
	public Sprite mC_doorSprite;

	/// <summary>
	/// The door open timer. (-1 means open instantly)
	/// </summary>
	public float  mF_OpenTimer;

	/// <summary>
	/// This door is going to be active?
	/// </summary>
	public bool   mB_Active;

	/// <summary>
	/// The door key to spawn a door
	/// </summary>
	public string mS_DoorKey;

	/// <summary>
	/// The trap flag
	/// </summary>
	public bool mB_IsTrap;

	public DoorSequence()
	{
		mC_doorSprite = null;
		mF_OpenTimer = 0f;
		mB_Active=false;
		mS_DoorKey=string.Empty;
		mB_IsTrap = false;
	}

}

public class DoorCreationStruct
{
	public DoorSequence mySequence;
	public DIRECTION myDirection;

	public DoorCreationStruct()
	{
		mySequence = new DoorSequence();
		myDirection = DIRECTION.NORTH;
	}
}

