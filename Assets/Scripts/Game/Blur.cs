using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Blur : MonoBehaviour
{
	public SpriteRenderer BlurOriginSprite;

	public List<SpriteRenderer> blurPass = new List<SpriteRenderer>();

	public int BlurIterations=2;

	bool blur=true;

	public bool MotionBlur = false;
	void Awake()
	{
		if(BlurOriginSprite==null)
		{
			BlurOriginSprite = this.gameObject.GetComponent<SpriteRenderer>();
			if(BlurOriginSprite==null)
			{
				Debug.Log("There isnt any Sprite Renderer attached to ("+this.name+") gameobject");
				blur=false;
				return;
			}
		}
		float div = 255f/(float)(BlurIterations+1);
		for(int i=0;i<BlurIterations;i++)
		{
			GameObject newRender = new GameObject("Blur_"+i);
			newRender.transform.parent = BlurOriginSprite.transform;
			newRender.transform.localPosition = Vector3.zero;
			newRender.transform.localScale = Vector3.one;
			SpriteRenderer render = newRender.AddComponent<SpriteRenderer>();
			render.sortingOrder = BlurOriginSprite.sortingOrder-(1+i);
			render.sortingLayerID = BlurOriginSprite.sortingLayerID;
			Color color = BlurOriginSprite.color;
			// (div * (float)( i+1 ) );
			float mul =  div* ( (float)(BlurIterations-i ) );

			color.a = (float)(mul/ 255f);
			render.color = color;
			//Set a default sprite
			render.sprite = BlurOriginSprite.sprite;
			//TEMP
			render.enabled = false;
			blurPass.Add(render);
		}
	}

	public void ActivateBlur()
	{
		MotionBlur=true;
		foreach(SpriteRenderer data in blurPass)
			data.enabled = true;
		updated = false;
	}

	public void DeactivateBlur()
	{
		MotionBlur=false;
		foreach(SpriteRenderer data in blurPass)
			data.enabled = false;
	}

	bool updated = false;
	Vector3 previousPos = Vector3.zero;
	Vector3 originPos = Vector3.zero;
	Vector3 deltaPos  = Vector3.zero;

	void Update()
	{
		if(MotionBlur && blur)
		{
			if(!updated)
			{}
			else
				previousPos = originPos;
			originPos = BlurOriginSprite.transform.position;

			if(updated)
			{
				if(originPos-previousPos == Vector3.zero)
				{
					Debug.Log("SAME COORDINATES");
				}
				else
				{
					deltaPos = originPos-previousPos;
				}
			}

			for(int i=0;i<blurPass.Count;i++)
			{
				if(i==0)
					blurPass[0].transform.position = previousPos;
				else
				{
					blurPass[1].transform.position = previousPos-(deltaPos*(i+1));
				}
			}
			updated = true;
		}
	}
}

