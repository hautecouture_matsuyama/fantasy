﻿using UnityEngine;
using System.Collections;

public class PickPlayerInfo : MonoBehaviour 
{
	public int mI_PlayerIndex;
	
	public Animator myAnimator;

	public bool mB_IsEnabled = false;

	public float CenteredPostion=0f;

	public void TurnAnimator(bool state)
	{
		myAnimator.enabled = state;
	}
}