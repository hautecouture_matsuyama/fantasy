﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Soomla.Store;

namespace SantaAssets
{
    /*
	public class SantaStoreAssets : IStoreAssets
	{
		public int GetVersion()
		{return 1;}

		public VirtualCurrency[] GetCurrencies()
		{return new VirtualCurrency[]{};}
		
		public VirtualGood[] GetGoods() {
			return new VirtualGood[] {UNLOCK_CHAR,UNLOCK_ALLCHARS,WOMBO_COMBO,BUY_POWERS,DISABLE_ADS};
		}
		
		public VirtualCurrencyPack[] GetCurrencyPacks() {
			return new VirtualCurrencyPack[] {};
		}
		
		public VirtualCategory[] GetCategories() {
			return new VirtualCategory[]{};
		}

#if UNITY_IOS
		//public static VirtualGood UNLOCK_CHAR = new VirtualGood()
#endif
		public const string ONE_CHAR = "snowtimeONECHAR";
		public const string POWER_PACK = "snowtimePOWPACK";
		public const string NO_ADS = "snowtimeNOADS";
		public const string ALL_CHARS = "snowtimeALLCHARS";
		public const string ALL_CHARS_NO_ADS = "snowtimeALLCHARSNOADS";

		public static VirtualGood UNLOCK_CHAR = new SingleUseVG(
			"Unlock the current character",
			"Unlock the selected character",
			CONSTANTS.ONE_CHAR,
			new PurchaseWithMarket(  CONSTANTS.ONE_CHAR ,0.99 ) );

		public static VirtualGood UNLOCK_ALLCHARS = new LifetimeVG(
			"Unlock all characters",
			"Unlock all characters",
			CONSTANTS.ALL_CHARS,
			new PurchaseWithMarket(CONSTANTS.ALL_CHARS,3.99 ) );

		public static VirtualGood WOMBO_COMBO = new LifetimeVG(
			"Get all characters and disable of ads",
			"Get all characters and disable of ads",
			CONSTANTS.ALL_CHARS_NO_ADS,
			new PurchaseWithMarket(CONSTANTS.ALL_CHARS_NO_ADS,4.99) );

		public static VirtualGood BUY_POWERS = new SingleUseVG(
			"Get a Power Pack",
			"Get a Power Pack",
			CONSTANTS.POWER_PACK,
			new PurchaseWithMarket(CONSTANTS.POWER_PACK,0.99) );

		public static VirtualGood DISABLE_ADS = new LifetimeVG(
			"No Ads",
			"No more Ads in the Game",
			CONSTANTS.NO_ADS,
			new PurchaseWithMarket(CONSTANTS.NO_ADS,0.99) );
	}
    */
}