﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwapEntity : GameEntity
{
	public string mS_IdleKey;
	public string mS_OpeningKey;
	public string mS_OpenKey;

	public Animator mC_Animator;
	public Collider2D mC_myCollider;

	public bool mB_Obstacle;
	public KILL mE_KillReason;

	public float mF_ActionTimer;

	public virtual void Initialize(string key)
	{
		if(mC_Animator == null)
		{
			mC_Animator = this.gameObject.GetComponent<Animator>();
		}
		if(mC_myCollider == null)
		{
			mC_myCollider = this.gameObject.GetComponent<Collider2D>();
		}
	}

	public virtual void Initialize(string key,DIRECTION dir)
	{
		if(mC_Animator == null)
		{
			mC_Animator = this.gameObject.GetComponent<Animator>();
		}
		if(mC_myCollider == null)
		{
			mC_myCollider = this.gameObject.GetComponent<Collider2D>();
		}
	}

	public virtual void Despawn()
	{
		CancelInvoke();
		//Debug.LogWarning("Despawning ("+this.gameObject.name+")");
		EntityManager.GetInstance().Pools[mS_EntityKey].Despawn(this.gameObject);
	}

	public virtual void StartBehaviour()
	{

	}

	public virtual void SetState(int state)
	{

	}

	public virtual void SetTimer(float t)
	{
		mF_ActionTimer = t;
	}

	public virtual void SetVelocity(Vector3 vel)
	{

	}
}
