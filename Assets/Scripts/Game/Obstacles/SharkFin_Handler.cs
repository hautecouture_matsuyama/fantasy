using UnityEngine;
using System.Collections;

public class SharkFin_Handler : ObstacleHandler
{
	public Sprite mC_Sprite;

	public SpriteRenderer mC_SharkFin1;
	public SpriteRenderer mC_SharkFin2;

	public SharkFinsPoints[] mA_StartingPoints;

	public SwapSharkFin mC_Fin1 = null;
	public SwapSharkFin mC_Fin2 = null;

	public RectVector Left_Destiny;
	public RectVector Right_Destiny;

	public Vector2 mV3_MovementAxis;

	public override void Initialize (AXIS	axis)
	{
		base.Initialize (axis);

		Debug.Log("* Initializing SharkFin Trap");

		// Pick a initialPosition
		int randomPos = Random.Range(0,mA_StartingPoints.Length);

		SharkFinsPoints points = mA_StartingPoints[ randomPos ];

		mC_SharkFin1.transform.position = points.Fin_One.position;
		mC_SharkFin2.transform.position = points.Fin_Two.position;

		myState = StateTween.ENTER;

		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();
		myTweenPosition.PlayForward();
	}

	public override void Activate ()
	{
		base.Activate ();
		Debug.Log("** Activating SharkFin Trap");

		GameObject obj1 = EntityManager.GetInstance().Pools[ObstacleKey].Spawn();
		obj1.transform.position = mC_SharkFin1.transform.position;
		/*
		obj1.transform.localScale = new Vector3( -Mathf.Abs(obj1.transform.localScale.x),
		                                         obj1.transform.localScale.y,
		                                        obj1.transform.localScale.z);
		                                        */
		mC_Fin1 = obj1.GetComponent<SwapSharkFin>();
		mC_Fin1.myRenderer.sprite = mC_SharkFin1.sprite;
		//mC_SharkFin1.sprite = null;
		mC_SharkFin1.enabled = false;


		GameObject obj2 = EntityManager.GetInstance().Pools[ObstacleKey].Spawn();
		obj2.transform.position = mC_SharkFin2.transform.position;
		/*
		obj2.transform.localScale = new Vector3( Mathf.Abs(obj1.transform.localScale.x),
		                                        obj1.transform.localScale.y,
		                                        obj1.transform.localScale.z);
		*/
		mC_Fin2 = obj2.GetComponent<SwapSharkFin>();
		mC_Fin2.myRenderer.sprite = mC_SharkFin2.sprite;
		//mC_SharkFin2.sprite = null;
		mC_SharkFin2.enabled = false;
	
		float speedMul = 1f;

		if(mE_ObstacleAxis == AXIS.HORIZONTAL)
		{
			Debug.Log("TRAP was horizontal");
			//this.transform.eulerAngles = Vector3.zero;
			mV3_MovementAxis = Vector2.right;
			mC_Fin1.transform.eulerAngles = Vector3.zero;
			mC_Fin2.transform.eulerAngles = Vector3.zero;
		}
		else
		{
			Debug.Log("TRAP was vertical");
			//this.transform.eulerAngles = Vector3.forward * 90f;
			mV3_MovementAxis = Vector2.up;
			mC_Fin1.transform.eulerAngles = this.transform.eulerAngles;
			mC_Fin2.transform.eulerAngles = this.transform.eulerAngles;
			if(IngameManager.GetInstance().worldGen.currentRoom.doorDirection == DIRECTION.EAST)
				speedMul = -1f;
			/*
			mC_Fin1.transform.eulerAngles = Vector3.forward * 90f;
			mC_Fin2.transform.eulerAngles = Vector3.forward * 90f;
			*/
		}

		mC_Fin1.Initialize(ObstacleKey,true,this,mV3_MovementAxis);
		mC_Fin2.Initialize(ObstacleKey,false,this,mV3_MovementAxis);
		mC_Fin1.SetTimer(0.34f);
		mC_Fin2.SetTimer(0.34f);

		Vector3 refPos = mA_StartingPoints[0].Fin_One.position;
		Left_Destiny = new RectVector(refPos.x - 0.5f, refPos.x + 0.5f,
		                              refPos.y + 0.5f, refPos.y - 0.5f);

		refPos = mA_StartingPoints[0].Fin_Two.position;
		Right_Destiny = new RectVector( refPos.x - 0.5f, refPos.x + 0.5f,
		                                refPos.y + 0.5f, refPos.y - 0.5f);

		mC_Fin1.SetDestinyArea( Right_Destiny );
		mC_Fin2.SetDestinyArea( Left_Destiny  );

		SwapAudio.GetInstance().Play("Icesaw",true);

		mC_Fin1.StartBehaviour();
		mC_Fin2.StartBehaviour();

		if(!IngameManager.GetInstance().mB_DoingSandClock)
		{
			mC_Fin1.SetVelocity(1f*speedMul);
			mC_Fin2.SetVelocity(1f*speedMul);
		}
		else
		{
			mC_Fin1.SetVelocity(0.5f*speedMul);
			mC_Fin2.SetVelocity(0.5f*speedMul);
			mC_Fin1.SetTimer(0.7f);
			mC_Fin2.SetTimer(0.7f);
		}
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine( Left_Destiny.TopLeft,		Left_Destiny.TopRight );
		Gizmos.DrawLine( Left_Destiny.TopRight,		Left_Destiny.BottomRight );
		Gizmos.DrawLine( Left_Destiny.BottomRight,	Left_Destiny.BottomLeft );
		Gizmos.DrawLine( Left_Destiny.BottomLeft,	Left_Destiny.TopLeft );

		Gizmos.color = Color.blue;
		Gizmos.DrawLine( Right_Destiny.TopLeft,		Right_Destiny.TopRight );
		Gizmos.DrawLine( Right_Destiny.TopRight,	Right_Destiny.BottomRight );
		Gizmos.DrawLine( Right_Destiny.BottomRight,	Right_Destiny.BottomLeft );
		Gizmos.DrawLine( Right_Destiny.BottomLeft,	Right_Destiny.TopLeft );
	}
#endif

	public RectVector GetNewDestiny(bool left_Reached)
	{
		if(mE_ObstacleAxis == AXIS.VERTICAL)
		{
			if(IngameManager.GetInstance().worldGen.currentRoom.doorDirection == DIRECTION.EAST)
			{
				if(left_Reached)
					return Left_Destiny;
				return Right_Destiny;
			}
			else
			{

			}
		}
		if(left_Reached)
			return Right_Destiny;
		return Left_Destiny;
	}

	public override void Despawn ()
	{
		base.Despawn ();

		SwapAudio.GetInstance().Stop("Icesaw");

		myState = StateTween.EXIT;

		Vector3 newDestiny = -myTweenPosition.from;
		myTweenPosition.from = this.transform.position;
		myTweenPosition.to   = newDestiny;
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();

		mC_SharkFin1.sprite = mC_Fin1.myRenderer.sprite;
		mC_SharkFin2.sprite = mC_Fin2.myRenderer.sprite;

		mC_Fin1.Despawn();
		mC_Fin2.Despawn();

		myTweenPosition.PlayForward();
	}

	public override void FinishTween()
	{
		if(myState == StateTween.EXIT)
		{
			EntityManager.GetInstance().Pools[mS_EntityKey].Despawn(this.gameObject);
		}
	}

	public override void SandClock (bool activate)
	{
		if(activate)
		{
			mC_Fin1.SetVelocity(0.5f);
			mC_Fin2.SetVelocity(0.5f);
			mC_Fin1.SetTimer(0.7f);
			mC_Fin2.SetTimer(0.7f);
		}
		else
		{
			mC_Fin1.SetVelocity(2f);
			mC_Fin2.SetVelocity(2f);
			mC_Fin1.SetTimer(0.34f);
			mC_Fin2.SetTimer(0.34f);
		}
	}
}

[System.Serializable]
public struct SharkFinsPoints
{
	public Transform Fin_One;
	public Transform Fin_Two;
}
