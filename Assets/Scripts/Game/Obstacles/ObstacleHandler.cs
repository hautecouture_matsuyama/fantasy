using UnityEngine;
using System.Collections;

public class ObstacleHandler : GameEntity
{
	public string ObstacleKey;
	public TweenPosition myTweenPosition;

	public StateTween myState = StateTween.ENTER;

	public AXIS mE_ObstacleAxis;

	public virtual void Initialize(AXIS	axis)
	{
		mE_ObstacleAxis = axis;
	}

	public virtual void Activate()
	{

	}

	public virtual void Despawn()
	{

	}

	public virtual void FinishTween()
	{
		if(myState == StateTween.EXIT)
		{
			EntityManager.GetInstance().Pools[mS_EntityKey].Despawn(this.gameObject);
		}
	}

	public virtual void SandClock(bool activate)
	{

	}
}

