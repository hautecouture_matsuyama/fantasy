﻿using UnityEngine;
using System.Collections;

public class Bridge_Handler : ObstacleHandler 
{
	public string BridgeKey;

	public Transform[] mA_SpawnPoints;

	public SpriteRenderer myFalseBridge;

	public Bridge myBridge = null;

	public Vector2 movementAxis;

	public RectVector Left_Area;
	public RectVector Right_Area;

	public override void Initialize (AXIS axis)
	{
		base.Initialize (axis);

		Debug.Log("* Initializing Bridge Trap");

		Vector3 startPosition = mA_SpawnPoints[ Random.Range(0,mA_SpawnPoints.Length) ].position;
		myFalseBridge.enabled = true;
		myFalseBridge.transform.position = startPosition;

		myState = StateTween.ENTER;
		
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();
		myTweenPosition.PlayForward();

		myBridge = null;

		if(Left_Area == null)
			Left_Area = new RectVector(0f,0f,0,0f);
		if(Right_Area == null)
			Right_Area = new RectVector(0f,0f,0,0f);
	}
	
	public override void Activate ()
	{
		base.Activate ();

		Debug.Log("** Activating Bridge Trap");

		GameObject obj = EntityManager.GetInstance().Pools[ BridgeKey ].Spawn();

		obj.transform.position = myFalseBridge.transform.position;

		myBridge = obj.GetComponent<Bridge>();

		myFalseBridge.enabled = false;

		if(mE_ObstacleAxis == AXIS.HORIZONTAL)
		{
			movementAxis = Vector2.right;
			obj.transform.eulerAngles = Vector3.zero;
		}
		else
		{
			movementAxis = Vector2.up;
			/*
			if(IngameManager.GetInstance().previousDirection  == DIRECTION.WEST)
				this.transform.eulerAngles = Vector3.forward*90f;
			else
				this.transform.eulerAngles = Vector3.back*90f;
			*/
			obj.transform.eulerAngles = Vector3.forward * 90f;
		}

		Left_Area.SetNewRect(mA_SpawnPoints[0].position.x-0.25f,mA_SpawnPoints[0].position.x+0.25f,
		                     mA_SpawnPoints[0].position.y+0.25f,mA_SpawnPoints[0].position.y-0.25f);
		Right_Area.SetNewRect(mA_SpawnPoints[4].position.x-0.25f,mA_SpawnPoints[4].position.x+0.25f,
		                      mA_SpawnPoints[4].position.y+0.25f,mA_SpawnPoints[4].position.y-0.25f);

		myBridge.Initialize( movementAxis,this );
		myBridge.StartBehaviour();
	}

	public RectVector GetDestiny(bool negative)
	{
		if(negative)
			return Left_Area;
		return Right_Area;
	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine( Left_Area.TopLeft,		Left_Area.TopRight );
		Gizmos.DrawLine( Left_Area.TopRight,	Left_Area.BottomRight );
		Gizmos.DrawLine( Left_Area.BottomRight,	Left_Area.BottomLeft );
		Gizmos.DrawLine( Left_Area.BottomLeft,	Left_Area.TopLeft );
		
		Gizmos.color = Color.blue;
		Gizmos.DrawLine( Right_Area.TopLeft,		Right_Area.TopRight );
		Gizmos.DrawLine( Right_Area.TopRight,		Right_Area.BottomRight );
		Gizmos.DrawLine( Right_Area.BottomRight,	Right_Area.BottomLeft );
		Gizmos.DrawLine( Right_Area.BottomLeft,		Right_Area.TopLeft );
	}
	#endif

	public override void Despawn ()
	{
		base.Despawn ();

		myState = StateTween.EXIT;
		
		Vector3 newDestiny = -myTweenPosition.from;
		myTweenPosition.from = this.transform.position;
		myTweenPosition.to   = newDestiny;
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();

		myFalseBridge.enabled = true;
		myFalseBridge.sprite = myBridge.GetComponent<SpriteRenderer>().sprite;
		
		myBridge.Despawn();
		
		myTweenPosition.PlayForward();
	}

	public override void FinishTween ()
	{
		base.FinishTween ();
		if(myState == StateTween.EXIT)
		{
			EntityManager.GetInstance().Pools[mS_EntityKey].Despawn(this.gameObject);
			if(myBridge.gameObject.activeInHierarchy)
			{
				myBridge.Despawn();
				myBridge = null;
			}
			else
			{

			}
		}

	}

	public override void SandClock (bool activate)
	{
		Debug.Log("Bridge Shouldnt be affected by sand clock ? ");
	}
}
