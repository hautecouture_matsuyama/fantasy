using UnityEngine;
using System.Collections;

public class SwapSharkFin : SwapEntity
{
	public RectVector destinyArea;

	public Vector2    mV2_InitialDirection;

	public float      mF_MovementSpeed;
	public Vector2    mV3_MovementVector;

	public bool mB_CanMove;

	public Rigidbody2D myRigidBody;

	public SharkFin_Handler myHandler;

	public SpriteRenderer myRenderer;

	public override void Initialize (string key)
	{
		base.Initialize (key);
	}

	public void Initialize(string key,bool right,SharkFin_Handler hand, Vector2 direction)
	{
		myHandler = hand;

		if(myRigidBody==null)
			myRigidBody = this.gameObject.GetComponent<Rigidbody2D>();

		Initialize(key);

        float xPosition;

        if (IngameManager.GetInstance().previousDirection == DIRECTION.EAST)
        {
            xPosition = -0.5f;
        }
        else
        {
            xPosition = 0.5f;
        }

        if (right)
        {
            mV2_InitialDirection = direction * mF_MovementSpeed;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x + xPosition,
                         this.transform.localPosition.y,
                        this.transform.localPosition.z);
        }
        else
        {
            mV2_InitialDirection = -direction * mF_MovementSpeed;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x + xPosition,
                                     this.transform.localPosition.y,
                                    this.transform.localPosition.z - 1f);
        }
	}

	public override void Initialize (string key, DIRECTION dir)
	{
		base.Initialize (key, dir);
	}

	public override void Despawn ()
	{
		base.Despawn ();
	}

	public override void SetState (int state)
	{
		base.SetState (state);
	}

	public override void SetTimer (float t)
	{
		base.SetTimer (t);
	}

	public void SetDestinyArea(RectVector vec)
	{
		destinyArea = vec;
	}

	public override void StartBehaviour ()
	{
		myRigidBody.velocity = mV2_InitialDirection;
	}

	public void InvertDirection()
	{
		myRigidBody.velocity = -temporalPos;
		/*
		this.transform.localScale = new Vector3( -this.transform.localScale.x,
		                                         this.transform.localScale.y,
		                                        this.transform.localScale.z);
		*/
	}

	Vector3 temporalPos = Vector3.zero;

	public override void FixedUpdate ()
	{
        float xPosition;

        if (IngameManager.GetInstance().previousDirection == DIRECTION.EAST)
        {
            xPosition = 0.5f;
        }
        else
        {
            xPosition = -0.5f;
        }

        Vector3 postPosition = new Vector3(this.transform.localPosition.x + xPosition,
                                     this.transform.localPosition.y,
                                    this.transform.localPosition.z);
        if (destinyArea.IsInside(postPosition) )
		{
            if (myRigidBody.velocity.y > 0)
                destinyArea = myHandler.GetNewDestiny(false);
            else
                destinyArea = myHandler.GetNewDestiny(true);
            /*
			// Reached destiny area
			if(myRigidBody.velocity.x != 0f)
			{
				if(myRigidBody.velocity.x > 0)
					destinyArea = myHandler.GetNewDestiny(false);
				else 
					destinyArea = myHandler.GetNewDestiny(true);
			}
			else
			{
				if(myRigidBody.velocity.y > 0)
					destinyArea = myHandler.GetNewDestiny(false);
				else 
					destinyArea = myHandler.GetNewDestiny(true);
			}
            */
            temporalPos = myRigidBody.velocity;
			myRigidBody.velocity = Vector3.zero;
			Invoke("InvertDirection",mF_ActionTimer);

		}
	}

	public void SetVelocity (float vel)
	{
		myRigidBody.velocity *= vel;
	}
}

