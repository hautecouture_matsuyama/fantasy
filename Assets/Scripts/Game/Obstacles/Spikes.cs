using UnityEngine;
using System.Collections;

public class Spikes : SwapEntity
{
	public bool mB_Activated = false;

	public Sprite ActiveSprite;
	public Sprite InactiveSprite;

	public SpriteRenderer myRenderer;

	private Spikes_Handler myHandler;
	private bool onlyOnce;

	public Animator myAnimator;

	public void Initialize(bool startActivated,float timer, float initialTimer,AXIS axis,Spikes_Handler h)
	{
		onlyOnce = false;
		myHandler = h;

		if(axis == AXIS.HORIZONTAL)
			this.transform.eulerAngles = Vector3.zero;
		else
		{
			if(IngameManager.GetInstance().previousDirection  == DIRECTION.WEST)
				this.transform.eulerAngles = Vector3.forward*90f;
			else
				this.transform.eulerAngles = Vector3.back*90f;
		}

		mB_Activated = startActivated;
		SetTimer(timer);

		//myRenderer.enabled = false;
		//mC_myCollider.enabled = false;
		if(mB_Activated)
		{
			//myRenderer.sprite = ActiveSprite;
			myAnimator.Play("On");
			SwapAudio.GetInstance().Play("Spikes_Out");
			mC_myCollider.enabled = true;
		}
		else
		{
			//myRenderer.sprite = InactiveSprite;
			myAnimator.Play("Off");
			SwapAudio.GetInstance().Play("Spikes_In");
			mC_myCollider.enabled = false;
		}

		Invoke("ChangeState",initialTimer);
	}

	void ChangeState()
	{
		mB_Activated = !mB_Activated;
		if(mB_Activated)
		{
			//myRenderer.sprite = ActiveSprite;
			myAnimator.Play("TurningOn");
			SwapAudio.GetInstance().Play("Spikes_Out");
			mC_myCollider.enabled = true;
		}
		else
		{
			//myRenderer.sprite = InactiveSprite;
			myAnimator.Play("TurningOff");
			SwapAudio.GetInstance().Play("Spikes_In");
			mC_myCollider.enabled = false;
			if(!onlyOnce)
			{
				myHandler.myFalseSpikesRender.sprite = null;
				onlyOnce = true;
			}
		}

        if(GameOverWindow.Instance.isGameOver)
        {
            return;
        }

        //TODO Play Sound?
        if (IngameManager.GetInstance().mB_DoingSandClock)
		{
			if(mB_Activated)
				Invoke("ChangeState",mF_ActionTimer*0.5f);
			else
				Invoke("ChangeState",mF_ActionTimer);
		}
		else
			Invoke("ChangeState",mF_ActionTimer);
	}

	public override void Despawn ()
	{
		base.Despawn ();
	}

	public override void StartBehaviour ()
	{
		base.StartBehaviour ();
	}

}

