﻿using UnityEngine;
using System.Collections.Generic;

public class Proyectile_Handler : ObstacleHandler 
{
	public Proyectile mC_ActiveProyectile = null;

	public Transform SpawnPoint;
	public Transform DespawnPoint;

	public RectVector destinyArea;

	public Vector3 mV3_MovementAxis;

	public List<Proyectile> mL_Proyectiles = new List<Proyectile>();

	public List<float> mL_Pattrons = new List<float>();

	private DIRECTION myDirection;

	public override void Initialize (AXIS axis)
	{
		Debug.Log("* Initializing Proyectiles ("+this.name+") Trap");

		mL_Proyectiles.Clear();

		if(destinyArea == null)
			destinyArea = new RectVector(0f,0f,0f,0f);

		base.Initialize (axis);

		myState = StateTween.ENTER;
		
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();
		myTweenPosition.PlayForward();

		if(axis == AXIS.HORIZONTAL)
		{
			mV3_MovementAxis = Vector3.right;
		}
		else
		{
			mV3_MovementAxis = Vector3.up;
		}

		myDirection = IngameManager.GetInstance().worldGen.currentRoom.doorDirection;

		// Get a fire pattron
		CreatePattron();
	}

	void CreatePattron()
	{
		mL_Pattrons.Clear();

		float previousTime = 0f;
		mL_Pattrons.Add(0f);
		while(mL_Pattrons.Count < 4)
		{
			int rand = Random.Range(0,5)%2;
			if( rand == 0)
				previousTime += 0.75f;
			else
				previousTime += 0.4f;
			mL_Pattrons.Add(previousTime);
		}
	}

	public override void Activate ()
	{
		isActive = true;

		Debug.Log("** Activating Proyectile ("+this.name+") Trap");
		base.Activate ();

		destinyArea.SetNewRect(DespawnPoint.position.x-0.25f,
		                       DespawnPoint.position.x+0.25f,
		                       DespawnPoint.position.y+0.25f,
		                       DespawnPoint.position.y-0.25f);

		foreach(float data in mL_Pattrons)
		{
			Invoke("LaunchProyectile",data);
		}
	}

	int it=0;
	void LaunchProyectile()
	{
		GameObject obj = EntityManager.GetInstance().Pools[ObstacleKey].Spawn();
		Proyectile proy = obj.GetComponent<Proyectile>();

		if(IngameManager.GetInstance().mB_isPlaying)
			SwapAudio.GetInstance().Play("Snowball");

		if(!IngameManager.GetInstance().mB_DoingSandClock)
			proy.Initialize(mV3_MovementAxis,this,destinyArea,mE_ObstacleAxis,myDirection);
		else
			proy.Initialize(mV3_MovementAxis*0.5f,this,destinyArea,mE_ObstacleAxis,myDirection);

		proy.transform.position = SpawnPoint.position;

		mL_Proyectiles.Add(proy);
		it++;
	}

	public override void Despawn ()
	{
		isActive = false;
		CancelInvoke();

		foreach(Proyectile p in mL_Proyectiles)
			p.Despawn();

		myState = StateTween.EXIT;
		
		Vector3 newDestiny = -myTweenPosition.from;
		myTweenPosition.from = this.transform.position;
		myTweenPosition.to   = newDestiny;
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();

		myTweenPosition.PlayForward();
	}

	public void DespawnProyectile(Proyectile p)
	{
		if(mL_Proyectiles.Contains(p))
		{
			p.Despawn();
			mL_Proyectiles.Remove(p);
		}
	}

	public override void FinishTween ()
	{
		base.FinishTween ();
	}

	void ContinueLaunching()
	{
		Invoke("LaunchProyectile", 0f );
	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine( destinyArea.TopLeft,		destinyArea.TopRight );
		Gizmos.DrawLine( destinyArea.TopRight,		destinyArea.BottomRight );
		Gizmos.DrawLine( destinyArea.BottomRight,	destinyArea.BottomLeft );
		Gizmos.DrawLine( destinyArea.BottomLeft,	destinyArea.TopLeft );
	}
	#endif

	public override void FixedUpdate ()
	{
		if(!isActive)
			return;
		if(mL_Proyectiles.Count < 3 && it >= 4)
		{
			ContinueLaunching();
		}
	}

	public override void SandClock (bool activate)
	{
		if(activate)
		{
			foreach(Proyectile data in mL_Proyectiles)
			{
				data.SetVelocity(0.5f);
			}
		}
		else
		{
			foreach(Proyectile data in mL_Proyectiles)
			{
				data.SetVelocity(2f);
			}
		}
	}
}
