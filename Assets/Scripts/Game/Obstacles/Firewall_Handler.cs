﻿using UnityEngine;
using System.Collections;

public class Firewall_Handler : ObstacleHandler 
{
	public Sprite defaultSprite; 

	public SpriteRenderer myFalseObstacle;

	public Fire myFire;

	public Vector2 mF_SpikesTimerRanges;
	public float   mF_SpikesTimer;
	
	public float   mF_InitialTimer;
	
	public bool mB_BegingActivated;

	public override void Initialize (AXIS axis)
	{
		base.Initialize (axis);

		mF_SpikesTimer = Random.Range(mF_SpikesTimerRanges.x,mF_SpikesTimerRanges.y);
		mF_InitialTimer = Random.Range(0f,mF_SpikesTimer);


		mB_BegingActivated = false;
		myFalseObstacle.sprite = null;
		if(Random.Range(0,2) == 0)
		{
			mB_BegingActivated = true;
			myFalseObstacle.sprite = defaultSprite;
		}

		myState = StateTween.ENTER;
		
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();
		myTweenPosition.PlayForward();
	}

	public override void Activate ()
	{
		base.Activate ();

		GameObject obj = EntityManager.GetInstance().Pools[ObstacleKey].Spawn();
		obj.transform.position = this.transform.position;
		myFire = obj.GetComponent<Fire>();

		if(!IngameManager.GetInstance().mB_DoingSandClock)
			myFire.Initialize(mE_ObstacleAxis,mB_BegingActivated,mF_InitialTimer,mF_SpikesTimer,this);
		else
			myFire.Initialize(mE_ObstacleAxis,mB_BegingActivated,mF_InitialTimer,mF_SpikesTimer*2f,this);
	}

	public override void Despawn ()
	{
		base.Despawn ();
		myFalseObstacle.sprite = myFire.GetComponent<SpriteRenderer>().sprite;

		myState = StateTween.EXIT;
		
		Vector3 newDestiny = -myTweenPosition.from;
		myTweenPosition.from = this.transform.position;
		myTweenPosition.to   = newDestiny;
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();

		myFire.Despawn();

		myTweenPosition.PlayForward();
	}

	public override void FinishTween()
	{
		base.FinishTween();
	}

	private float temporalSpeed;
	public override void SandClock (bool activate)
	{
		if(activate)
		{
			temporalSpeed = mF_SpikesTimer * 2f;
			myFire.SetTimer(temporalSpeed);
		}
		else
		{
			myFire.SetTimer(mF_SpikesTimer);
		}
	}
}
