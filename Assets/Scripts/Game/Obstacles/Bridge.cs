using UnityEngine;
using System.Collections;

public class Bridge : SwapEntity
{
	public Rigidbody2D myRigidbody;

	public float mF_MovementSpeed;
	public Vector2 mV2_MovementAxis;
	public Vector2 mV2_MovementVector;

	public RectVector destinyArea;

	public bool mB_NegativeMovement = false;

	private Bridge_Handler myHandler;

	public override void Initialize (string key)
	{
		base.Initialize (key);
	}

	public override void Initialize (string key, DIRECTION dir)
	{
		base.Initialize (key, dir);
	}

	public void Initialize(Vector2 movementAxis,Bridge_Handler h)
	{
		mV2_MovementAxis = movementAxis;
		myHandler = h;
		if( Random.Range(0,2) == 0)
		{
			mV2_MovementVector = mV2_MovementAxis * mF_MovementSpeed;
			mB_NegativeMovement = false;
		}
		else
		{
			mV2_MovementVector = -mV2_MovementAxis * mF_MovementSpeed;
			mB_NegativeMovement = true;
		}
	}
	
	public override void StartBehaviour ()
	{
		base.StartBehaviour ();

		SwapAudio.GetInstance().Play("Water",true);

		if(IngameManager.GetInstance().previousDirection == DIRECTION.EAST)
			mB_NegativeMovement = !mB_NegativeMovement;

		this.transform.eulerAngles = myHandler.transform.eulerAngles;

		destinyArea = myHandler.GetDestiny(mB_NegativeMovement);
		myRigidbody.velocity = mV2_MovementVector;
	}

	public override void Despawn ()
	{
		SwapAudio.GetInstance().Stop("Water");
		base.Despawn ();
	}

	public override void FixedUpdate ()
	{
		if(destinyArea.IsInside(this.transform.position))
		{
			mB_NegativeMovement = !mB_NegativeMovement;
			mV2_MovementVector = -mV2_MovementVector;
			myRigidbody.velocity = mV2_MovementVector;
			destinyArea = myHandler.GetDestiny(mB_NegativeMovement);
		}
	}
}

