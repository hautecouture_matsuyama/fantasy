using UnityEngine;
using System.Collections;

public class Proyectile : SwapEntity
{
	public float mF_Speed;

	public Vector3 mV_Speed;

	public Proyectile_Handler myHandler;

	public Rigidbody2D myRigidbody;

	public RectVector destinyArea;

	private AXIS myAxis;

	public void Initialize(Vector3 direction, Proyectile_Handler h,RectVector d,AXIS ax,DIRECTION dir)
	{
		myAxis = ax;

		if(myAxis == AXIS.HORIZONTAL)
			mV_Speed = -direction * mF_Speed;
		else
		{
			if(dir == DIRECTION.WEST)
			{
				mV_Speed = -direction * mF_Speed;
				//mV_Speed = direction * mF_Speed;
				this.transform.eulerAngles = Vector3.forward * 90f;
			}
			else
			{
				//mV_Speed = -direction * mF_Speed;
				mV_Speed = direction * mF_Speed;
				this.transform.eulerAngles = -Vector3.forward * 90f;
			}
		}

		myHandler = h;
		myRigidbody.velocity = mV_Speed;
		destinyArea = d;
	}

	public override void Despawn ()
	{
		base.Despawn ();
		myHandler = null;
	}

	public override void FixedUpdate ()
	{
		if(destinyArea.IsInside( this.transform.position ) )
		{
			myRigidbody.velocity = Vector3.zero;
			myHandler.DespawnProyectile(this);
		}
	}

	public void SetVelocity (float vel)
	{
		myRigidbody.velocity *= vel;
	}
}

