﻿using UnityEngine;
using System.Collections;

public class Spikes_Handler : ObstacleHandler 
{
	public Sprite ActiveSprite;
	public Sprite InactiveSprite;

	public SpriteRenderer myFalseSpikesRender;
	
	public Spikes mySpikes;

	public Vector2 mF_SpikesTimerRanges;
	public float   mF_SpikesTimer;

	public float   mF_InitialTimer;

	public bool mB_BegingActivated;

	public override void Initialize (AXIS axis)
	{
		Debug.Log("* Initializing Spikes Trap");
		base.Initialize (axis);

		myState = StateTween.ENTER;
		
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();
		myTweenPosition.PlayForward();

		mF_SpikesTimer = Random.Range(mF_SpikesTimerRanges.x,mF_SpikesTimerRanges.y);
		mF_InitialTimer = Random.Range(0f,mF_SpikesTimer);

		print ("Spikes Timer = "+mF_SpikesTimer);
		print ("Initial Timer = "+mF_InitialTimer);

		mB_BegingActivated = false;
		if( Random.Range(0,2) == 0)
			mB_BegingActivated = true;

		if(mB_BegingActivated)
		{
			myFalseSpikesRender.sprite = ActiveSprite;
		}
		else
		{
			myFalseSpikesRender.sprite = InactiveSprite;
		}
	}

	public override void Activate ()
	{
		Debug.Log("** Activating Spikes Trap");
		base.Activate ();

		myFalseSpikesRender.sprite = null;

		GameObject obj = EntityManager.GetInstance().Pools[ ObstacleKey ].Spawn();
		obj.transform.position = myFalseSpikesRender.transform.position;

		mySpikes = obj.GetComponent<Spikes>();

		if(!IngameManager.GetInstance().mB_DoingSandClock)
			mySpikes.Initialize(mB_BegingActivated,mF_SpikesTimer,mF_InitialTimer,mE_ObstacleAxis,this);
		else
			mySpikes.Initialize(mB_BegingActivated,mF_SpikesTimer*2f,mF_InitialTimer,mE_ObstacleAxis,this);

	}

	public override void Despawn ()
	{
		base.Despawn ();

		myState = StateTween.EXIT;
		
		Vector3 newDestiny = -myTweenPosition.from;
		myTweenPosition.from = this.transform.position;
		myTweenPosition.to   = newDestiny;
		if(!myTweenPosition.playedReverse)
			myTweenPosition.ResetToBeginning();

		//
		myFalseSpikesRender.sprite = mySpikes.myRenderer.sprite;

		mySpikes.Despawn();

		myTweenPosition.PlayForward();
	}

	public override void FinishTween ()
	{
		base.FinishTween ();
	}


	private float temporalSpeed;
	public override void SandClock (bool activate)
	{
		if(activate)
		{
			temporalSpeed = mF_SpikesTimer * 2f;
			mySpikes.SetTimer(temporalSpeed);
		}
		else
		{
			mySpikes.SetTimer(mF_SpikesTimer);
		}
	}
}
