using UnityEngine;
using System.Collections;

public class Fire : SwapEntity
{
	private Firewall_Handler myHandler;

	public bool mB_State;

	private bool YOLO;

	public void Initialize(AXIS ax,bool startActive,float initTimer,float timer,Firewall_Handler h)
	{
		myHandler = h;

		YOLO = false;

		if(ax == AXIS.HORIZONTAL)
			this.transform.eulerAngles = Vector3.zero;
		else
			this.transform.eulerAngles = Vector3.forward * 90f;

		SetTimer(timer);

		mB_State=startActive;
		if(mB_State)
		{
			mC_Animator.Play("Activate");
			mC_myCollider.enabled = true;
		}
		else
		{
			mC_Animator.Play("Off");
			mC_myCollider.enabled = false;
		}

		Invoke("ChangeState",initTimer);
	}

	void ChangeState()
	{
		mB_State = !mB_State;

		if(mB_State)
		{
			mC_Animator.Play("Activate");
			mC_myCollider.enabled = true;
		}
		else
		{
			if(!YOLO)
			{
				myHandler.myFalseObstacle.sprite = null;
				YOLO = true;
			}
			mC_Animator.Play("Off");
			mC_myCollider.enabled = false;
		}

		if(IngameManager.GetInstance().mB_DoingSandClock)
		{
			if(mB_State)
				Invoke("ChangeState",mF_ActionTimer*0.5f);
			else
				Invoke("ChangeState",mF_ActionTimer);
		}
		else
			Invoke("ChangeState",mF_ActionTimer);
	}

	public override void Despawn ()
	{
		base.Despawn ();
	}


}

