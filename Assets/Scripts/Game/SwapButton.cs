﻿using UnityEngine;
using System.Collections;

public class SwapButton : MonoBehaviour 
{
	public TweenScale myTweener;

	public bool CanTween = true;

	public void Shrink()
	{
		if(CanTween)
		{
			myTweener.PlayForward();
		}
		SwapAudio.GetInstance().Play("Button");
	}

	public void Enlarge()
	{
		if(CanTween)
			myTweener.PlayReverse();
	}

}
