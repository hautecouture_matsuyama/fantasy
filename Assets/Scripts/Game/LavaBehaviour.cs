﻿using UnityEngine;
//using UnityEditor.Animations;

public class LavaBehaviour : MonoBehaviour 
{
	public float mF_Delay;

	public static float mF_Speed;

	public Vector3 mV3_Movement;

	public Rigidbody2D myRigidbody;

	public TweenPosition myTween;

	public Vector3 HorizontalScale;
	public Vector3 VerticalScale;

	public SpriteRenderer myRenderer;
     
	private bool initializedArea = false;
	public RectVector finalArea = null;

    //public AnimationClip animationClip;
    public Animator doragonAnimator;
    //public AnimatorController[] doragonAnimators = new AnimatorController[4];

    public void Initialize(DIRECTION movement,float delay,float speed,GameObject newParent)
	{
		myRenderer.color = Color.white;

		doorCollision=0;

		mF_Delay = delay;
		mb_Move  = false;
	
		Vector3 position = Vector3.zero;
		if(movement == DIRECTION.NORTH)
		{
            //myRenderer.flipY = false;
            doragonAnimator.SetTrigger("doraBack");
            position = IngameManager.GetInstance().worldGen.currentRoom.SouthPosition.position;
			//Vector3.down * WorldGenerator.y_Threshold * 0.85f;//8f
			position.y -= 2f;
			this.transform.position    = position;
			//this.transform.eulerAngles = Vector3.zero;
			mV3_Movement = Vector3.up * mF_Speed * speed;

			finalArea.SetNewRect( position.x - 1f, position.x+1f,0.15f,0.5f);
		}
		else if(movement == DIRECTION.SOUTH)
		{
            //myRenderer.flipY = true;
            //doragonAnimator.SetInteger("draAni", 1);
            doragonAnimator.SetTrigger("doraFront");
            position = IngameManager.GetInstance().worldGen.currentRoom.NorthPosition.position;
			//Vector3.up * WorldGenerator.y_Threshold * 0.85f;//8f
			position.y += 2f;
			this.transform.position    = position;
			//this.transform.eulerAngles = Vector3.forward * 180f;
			mV3_Movement = Vector3.down * mF_Speed * speed;
		}
		else if(movement == DIRECTION.EAST)
		{
            myRenderer.flipX = false;
            doragonAnimator.SetTrigger("doraSide");
            if (IngameManager.GetInstance().worldGen.currentRoom.WallsSprite.transform.localEulerAngles.y != 0f)
				position  = IngameManager.GetInstance().worldGen.currentRoom.EastPosition.position;
			else
				position = IngameManager.GetInstance().worldGen.currentRoom.WestPosition.position;
			//position = IngameManager.GetInstance().worldGen.currentRoom.WestPosition.position;
			//Vector3.left * WorldGenerator.x_Threshold * 1.5f;//8f
			position.x -= 2f;
			this.transform.position = position;
			//this.transform.eulerAngles = -Vector3.forward * 90f;
			mV3_Movement = Vector3.right * mF_Speed * speed;
		}
		else
		{
            myRenderer.flipX = true;
            doragonAnimator.SetTrigger("doraSide");
            if (IngameManager.GetInstance().worldGen.currentRoom.WallsSprite.transform.localEulerAngles.y == 0f)
				position  = IngameManager.GetInstance().worldGen.currentRoom.EastPosition.position;
			else
				position = IngameManager.GetInstance().worldGen.currentRoom.WestPosition.position;
			//position = IngameManager.GetInstance().worldGen.currentRoom.EastPosition.position;
			//Vector3.right * WorldGenerator.x_Threshold * 1.5f;
			position.x += 2f;
			this.transform.position = position;
			//this.transform.eulerAngles = Vector3.forward * 90f;
			mV3_Movement = Vector3.left * mF_Speed * speed;
		}

		this.transform.parent = newParent.transform;

		Invoke("StartMoving",mF_Delay);
	}

	public void SandClockSpeed()
	{
		myRigidbody.velocity = myRigidbody.velocity * 0.5f;
	}

	public void NormalSpeed()
	{
		myRigidbody.velocity = mV3_Movement;
	}

	public void StartMoving()
	{
		mb_Move = true;
		myRigidbody.velocity = mV3_Movement;
	}

	public void ExitLava(Vector3 destiny)
	{
	}

	public void StopMovement()
	{
		myRigidbody.velocity = Vector3.zero;
		//LeanTween.alpha(this.gameObject,CONSTANTS.BlurredWhite.a,0.2f).setEase(LeanTweenType.easeOutQuint);
	}

	public void FreezeLava()
	{
		myRigidbody.velocity = Vector3.zero;
	}

	public bool mb_Move = false;

	// Update is called once per frame
	/*
	void FixedUpdate () 
	{
		if(mb_Move && IngameManager.GetInstance().mB_isPlaying)
		{
			myRigidbody.velocity = mV3_Movement;
		}
	}
	*/

	int doorCollision = 0;
	void OnTriggerEnter2D(Collider2D other) 
	{
		//We can select any of the three posible colliders and decide when start opening doors
		if(other.gameObject.layer == 8)
		{
			//We touched door
			//IngameManager.GetInstance().worldGen.StartDoorOpening();
		}
		else if(other.gameObject.layer == 10)
		{
			// We touched wall
			//Debug.Log("!!!Start Doors");
			if(doorCollision > 0 )
			{
				FreezeLava();
			}
			else
			{
				SwapAudio.GetInstance().Play("Avalanche");
				IngameManager.GetInstance().worldGen.StartDoorOpening();
				if(TutorialManager.doingTutorial)
					FreezeLava();
				if(IngameManager.GetInstance().mB_DoingSandClock)
					SandClockSpeed();
			}

			doorCollision++;
		}
		else if(other.gameObject.layer == 11)
		{
			// We touched hidder
		}

	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.red;
		Gizmos.DrawLine( finalArea.TopLeft,		finalArea.TopRight );
		Gizmos.DrawLine( finalArea.TopRight,	finalArea.BottomRight );
		Gizmos.DrawLine( finalArea.BottomRight,	finalArea.BottomLeft );
		Gizmos.DrawLine( finalArea.BottomLeft,	finalArea.TopLeft );
	}
	#endif

	void FixedUpdate()
	{
		if(finalArea.IsInside(this.transform.position))
		{
			//We have reached the maximum area
			StopMovement();
		}
	}
}
