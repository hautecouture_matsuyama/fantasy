using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI_Manager : Singleton<UI_Manager>
{
	protected UI_Manager () {}

	public GameObject mGO_UI;

	public UILabel mLabel_ScoreLabel;
	public UILabel mLabel_HighScore;

	public UILabel mLabel_CoinsLabel;

	public PowerUpBase[] mA_PowerUps;

	public UILabel FreezeLabel;
	public UILabel SandLabel;
	public UILabel ShieldLabel;

	public TweenPosition ScoreTween;
	public TweenPosition LowerBarTween;

	public LeTweensBase[] PowerUpsTweens;

	public TweenPosition CoinsTween;

	public LeTweensBase GoLeTween;
	public TweenColor GoTweenColor;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text crystalText;




    void Awake()
    {
        //ScoreTween.from = UICamera.mainCamera.ViewportToWorldPoint(new Vector3(0.5f,0f,0f));
        //ScoreTween.to = UICamera.mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 500f, 0f));
    }



	public void Initialize(int count,int power_up1=0,int power_up2=0,int power_up3=0)
	{


        mLabel_ScoreLabel.text = count.ToString();
        scoreText.text = count.ToString();
        LavaBehaviour.mF_Speed = 2;

        mLabel_HighScore.text = "BEST:"+GameManager.GetInstance().mI_HighScore.ToString();

        crystalText.text = IngameManager.GetInstance().mI_Coins.ToString();


        int f = GameManager.GetInstance().mI_FreezePowerUps;
		int s = GameManager.GetInstance().mI_ShieldPowerUps;
		int sc = GameManager.GetInstance().mI_SandClockPowerUps;

		if(f <= 0)
			FreezeLabel.text = "+";
		else
			FreezeLabel.text = f.ToString();

		if(s <= 0)
			ShieldLabel.text = "+";
		else
			ShieldLabel.text = s.ToString();

		if(sc <= 0)
			SandLabel.text = "+";
		else
			SandLabel.text = sc.ToString();

		ScoreTween.transform.position = ScoreTween.from;
		if(!ScoreTween.playedReverse)
			ScoreTween.ResetToBeginning();
		ScoreTween.PlayForward();

		LowerBarTween.transform.position = LowerBarTween.from;
		if(!LowerBarTween.playedReverse)
			LowerBarTween.ResetToBeginning();
		LowerBarTween.PlayForward();

		CoinsTween.transform.position = CoinsTween.from;
		if(!CoinsTween.playedReverse)
			CoinsTween.ResetToBeginning();
		CoinsTween.PlayForward();

		GoLeTween.transform.localScale = Vector3.zero;
		GoLeTween.GetComponent<UISprite>().color = GoTweenColor.from;


		PowerUpsTweens[0].transform.position = PowerUpsTweens[0].PositionsPoins[0];
		PowerUpsTweens[1].transform.position = PowerUpsTweens[1].PositionsPoins[0];
		PowerUpsTweens[2].transform.position = PowerUpsTweens[2].PositionsPoins[0];
		StartCoroutine("PowerUpsTweenDelayed");
	}

	public void FadeOutGo()
	{
		GoTweenColor.ResetToBeginning();
		GoTweenColor.PlayForward();
	}

	public void ShowGO()
	{
		showedTime = 0f;
		GoLeTween.transform.localScale = Vector3.zero;
		GoTweenColor.duration = 0.5f;
		GoLeTween.GetComponent<UISprite>().color = GoTweenColor.from;
		GoLeTween.Init();
		GoLeTween.OnEnter();

		InvokeRepeating("CheckGO",0.02f,0.02f);
	}

	float showedTime = 0f;
	void CheckGO()
	{
		showedTime += 0.02f;
		if(showedTime > 0.9f)
		{
			if(GoTweenColor.GetComponent<UISprite>().color != GoTweenColor.to)
			{
				GoTweenColor.duration = 0.3f;
				GoTweenColor.ResetToBeginning();
				GoTweenColor.PlayForward();
			}
			CancelInvoke("CheckGO");
		}
	}

	IEnumerator PowerUpsTweenDelayed()
	{
		yield return new WaitForSeconds(0.4f);
		PowerUpsTweens[0].OnEnter();
		yield return new WaitForSeconds(0.2f);
		PowerUpsTweens[1].OnEnter();
		yield return new WaitForSeconds(0.2f);
		PowerUpsTweens[2].OnEnter();
	}

	public void UpdateScore(int score)
	{
		mLabel_ScoreLabel.text = score.ToString();
        scoreText.text = score.ToString();

        SpeedUpToDragon(score);


        mLabel_HighScore.text = "BEST:"+GameManager.GetInstance().mI_HighScore.ToString();
	}


	public void UpdateCoins(int coins)
	{
		mLabel_CoinsLabel.text = coins.ToString();
        crystalText.text = coins.ToString();
	}

	public void SetState(bool state)
	{
		mGO_UI.SetActive(state);
	}

	public void FreezePowerUp()
	{
		if(GameManager.GetInstance().mI_FreezePowerUps > 0 && WorldGenerator.OPENING_DOORS)
		{
			GameManager.GetInstance().GoogleAnalyticsLog("PowerUpUsed","Freeze");
			IngameManager.GetInstance().FreezePowerUp();
			int t = GameManager.GetInstance().mI_FreezePowerUps;
			if(t <= 0)
				FreezeLabel.text = "+";
			else
				FreezeLabel.text = t.ToString();
		}
		else
		{

		}
	}

	public void SandClockPowerUp()
	{
		if(GameManager.GetInstance().mI_SandClockPowerUps > 0)
		{
			IngameManager.GetInstance().SandClockPowerUp();
			GameManager.GetInstance().GoogleAnalyticsLog("PowerUpUsed","SandClock");
			int t = GameManager.GetInstance().mI_SandClockPowerUps;
			if(t <= 0)
				SandLabel.text = "+";
			else
				SandLabel.text = t.ToString();
		}
		else
		{

		}
	}

	public void InmunityPowerUp()
	{
		if(GameManager.GetInstance().mI_ShieldPowerUps > 0)
		{
			IngameManager.GetInstance().InmunityShield();
			GameManager.GetInstance().GoogleAnalyticsLog("PowerUpUsed","Shield");
			int t = GameManager.GetInstance().mI_ShieldPowerUps;
			if(t <= 0)
				ShieldLabel.text = "+";
			else
				ShieldLabel.text = t.ToString();
		}
		else
		{

		}
	}


   
    //ドラゴン加速
    void SpeedUpToDragon(int score)
    {
        int idx = score / 5;

        switch (idx)
        {
            case 1:
                LavaBehaviour.mF_Speed = 2.1f;
                break;
            case 2:
                LavaBehaviour.mF_Speed = 2.3f;
                break;
            case 4:
                LavaBehaviour.mF_Speed = 2.5f;
                break;
            case 5:
                LavaBehaviour.mF_Speed = 2.8f;
                break;
            case 10:
                LavaBehaviour.mF_Speed = 3f;
                break;


        }


        // LavaBehaviour.mF_Speed
    }
}

