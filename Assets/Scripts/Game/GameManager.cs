﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SantaAssets;


public class GameManager : Singleton<GameManager>
{
	protected GameManager () {}

	public static bool HasFinishedLoading = false;

	public string mS_ZoneId = string.Empty;

	public List<string> mL_ZoneIds = new List<string>();
	public string[] AndoridZoneIds;
	public string[] IosZoneIds;

	public DateTime NextGiftDate;
	public DateTime NextVideoDate;

	public bool mB_Music;
	public bool mB_Sounds;
	public bool mB_Notifications;

	public bool mB_Rated = false;

	public GameObject     LoadingSplashScreen;
	//public SpriteRenderer DarkScreen;
	public SpriteRenderer SplashScreen;

	public List<string> mL_AvaibleZones = new List<string>();

	[HideInInspector]
	public bool mB_LoadedFromBeginning = false;

	private Dictionary<string,bool> mD_Characters = new Dictionary<string, bool>();
	private Dictionary<string,Sprite> mD_AvaibleCharacters = new Dictionary<string, Sprite>();
	private List<int> mL_CharsAvaibleIndexes = new List<int>();

	public int mI_SandClockPowerUps = 1;
	public int mI_ShieldPowerUps =   1;
	public int mI_FreezePowerUps = 1;

	public static short FIRST_VIDEO = 0;
	public short VIDEO_IT = 0;

	//public GoogleAnalyticsV3 googleAnalytics;

	public static bool ShowTutorial = true;

    public int[] characterPrices = new int[17] { 0, 0, 250, 250, 500, 500, 500, 500, 1000, 1000, 1000, 1000, 1500, 1500, 1500, 1500, 1500 };

    public GameObject nendBanner;

    void Start()//Awake
	{
		//Debug.Log("Game First");

		int height = Screen.height;
		int width  = Screen.width;
		float worldScreenHeight = Camera.main.orthographicSize*2f;
		float worldScreenWidth  = worldScreenHeight/height*width;

		Vector3 newScale = new Vector3( worldScreenWidth / SplashScreen.sprite.bounds.size.x,
		                                worldScreenHeight/ SplashScreen.sprite.bounds.size.y,
		                                1f);
		SplashScreen.transform.localScale = newScale;

		GameManager.GetInstance().Initialize();

        //nendBanner.SetActive(true);
    }
	
	public void Initialize()
	{
		DontDestroyOnLoad(this.gameObject);

		//SoundManager.GetInstance().InitInstance();
		SwapAudio.GetInstance().Initialize();
		//SwapAudio.GetInstance().SetMusicVolume(88f);
		//SwapAudio.GetInstance().SetFXVolume(80f);

		mB_LoadedFromBeginning = true;
        // Init Ads
        // Init social
        // Init stuff

        // LOAD DATA
        LoadCharacters();
		LoadPlayerInfo();


        OnLevelWasLoaded(1);
		//Invoke("LoadMain",1f);
	}

	//void LoadMain()
	//{
	//	LoadScene(1);
	//}

	public bool VideoIterator()
	{
		if(VIDEO_IT==0)
		{
			VIDEO_IT++;
			return true;
		}
		VIDEO_IT++;
		if(VIDEO_IT >= 3)
			VIDEO_IT=0;
		return false;
	}

	//public void LoadScene(int scene)
	//{
	//	if(Application.loadedLevel == scene || (Application.loadedLevel == 0 && scene == 1))
	//	{
	//		//Debug.Log ("Same scene is being loaded");
	//		OnLevelWasLoaded(scene);
	//		return;
	//	}
	//	Application.LoadLevel(scene);
	//}

	void OnLevelWasLoaded(int level) 
	{
		if(level == 0)
		{
			//Debug.Log("First scene was loaded");

			IngameManager.GetInstance().CalculateResolution();

			WindowManager.GetInstance().Initialize();
			EntityManager.GetInstance().Initialize();
			
			UI_Manager.GetInstance().SetState(false);

			WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);

			HasFinishedLoading = true;
		}
		if (level == 1)
		{
//			print("Woohoo");

            SwapAudio.GetInstance().Initialize();
            //SwapAudio.GetInstance().SetMusicVolume(88f);
            //SwapAudio.GetInstance().SetFXVolume(80f);
            SwapAudio.GetInstance().Play("Menu",true);

			IngameManager.GetInstance().CalculateResolution();

			WindowManager.GetInstance().Initialize();
			EntityManager.GetInstance().Initialize();
			
			UI_Manager.GetInstance().SetState(false);

			//DarkScreen.sprite = null;
			LoadingSplashScreen.SetActive(false);
			WindowManager.GetInstance().ShowWindow(WINDOWSTYPES.MAIN_MENU);

			HasFinishedLoading = true;

            
		}
	}

	void LoadCharacters()
	{
        PickCharacter_Window window = WindowManager.GetInstance().AllCharactersWindow;
        string charsString = PlayerPrefs.GetString("Characters");
		if(charsString.Contains("Pirate"))
			charsString = string.Empty;
		if( string.IsNullOrEmpty(charsString))
		{
            Dictionary<string, object> dic = new Dictionary<string, object>();
            for(int i=0;i<window.mA_AllCharactersSprites.Length;i++)
            {
                if(i==0 || i == 1)
                {
                    dic.Add(window.mA_AllCharactersSprites[i].name, "True");
                }
                else
                {
                    dic.Add(window.mA_AllCharactersSprites[i].name, "False");
                }
            }

            charsString =  MiniJSON.Json.Serialize(dic);
        }

        
		Dictionary<string,object> dictLoad;

		int it=0;
		unlockedChars = 0;
		
		dictLoad = (Dictionary<string,object>)MiniJSON.Json.Deserialize(charsString);
        if (window.mA_AllCharactersSprites.Count() != dictLoad.Count)
        {
            List<string> dictLoadKeys = dictLoad.Select(x => x.Key).ToList();
            foreach(string key in dictLoadKeys.Except(window.mA_AllCharactersSprites.Select(x => x.name)))
            {
                dictLoad.Remove(key);
            }

            foreach (string key in window.mA_AllCharactersSprites.Select(x => x.name).Except(dictLoadKeys))
            {
                dictLoad.Add(key,false);
            }
        }

        Debug.Log(charsString);
		foreach(KeyValuePair<string,object> KVP in dictLoad)
		{
			string key = KVP.Key;
			string obj = KVP.Value.ToString();

            if (window.mA_AllCharactersSprites.Where(x => x.name == key).Count() == 0)
            {
                continue;
            }
            bool state = false;
			if(obj == "True")
			{
				state = true;
				unlockedChars++;
			}
            

			if(mD_Characters.ContainsKey(key))
			{
				//Debug.LogError("There is already a key ("+key+") on mD_Characters");
				continue;
			}

			mD_Characters.Add(key,state);

			if(!state)
			{
				mL_CharsAvaibleIndexes.Add(it);
				mD_AvaibleCharacters.Add( window.mA_AllCharactersSprites[it].name ,window.mA_AllCharactersSprites[it]);
			}
			it++;
		}
        if(unlockedChars > characterPrices.Length)
        {
            UNLOCK_MONEY = characterPrices[characterPrices.Length-1];
        }
        else
        {
            UNLOCK_MONEY = characterPrices[unlockedChars];
        }

        SaveCharacters();
	}

	public void SaveCharacters()
	{
		Dictionary<string,object> dic = new Dictionary<string, object>();
		foreach(KeyValuePair<string,bool> data in mD_Characters)
		{
			dic.Add(data.Key,data.Value.ToString());
		}
		PlayerPrefs.SetString("Characters",MiniJSON.Json.Serialize(dic));
	}

	public static Sprite lastUnlockedCharacter;

    public void UnlockCharacter(string key,string index)
	{
        if (mD_Characters.ContainsKey(key))
        {
            mD_Characters[key] = true;
            if (mD_AvaibleCharacters.ContainsKey(index))
            {
                mD_AvaibleCharacters.Remove(index);
            }
            SaveCharacters();
            unlockedChars++;
            if (unlockedChars > characterPrices.Length)
            {
                UNLOCK_MONEY = characterPrices[characterPrices.Length - 1];
            }
            else
            {
                UNLOCK_MONEY = characterPrices[unlockedChars];
            }
            WindowManager.GetInstance().AllCharactersWindow.UnlockCharacter(key);
            return;
        }
        /*
		if(mD_Characters.ContainsKey(key))
		{
			mD_Characters[key] = true;
			if(mD_AvaibleCharacters.ContainsKey(index))
			{
				mD_AvaibleCharacters.Remove(index);
			}
			SaveCharacters();
			unlockedChars++;
            if (unlockedChars > characterPrices.Length)
            {
                UNLOCK_MONEY = characterPrices[characterPrices.Length - 1];
            }
            else
            {
                UNLOCK_MONEY = characterPrices[unlockedChars];
            }
            WindowManager.GetInstance().AllCharactersWindow.UnlockCharacter(key);
            return;
		}
        */
    }

	public bool GetUnlockedChar(string character)
	{
		if(mD_Characters.ContainsKey(character))
		{
			return mD_Characters[character];
		}
		return false;
	}

	public int mI_HighScore = 0;
	public int mI_Money = 0;
	public int unlockedChars = 0;
	public static int UNLOCK_MONEY = 0;
	public static bool SHOW_ADS = true;
    public static Dictionary<string, object> playerInfo;

    public void LoadPlayerInfo()
	{
        string musicKey = PlayerPrefs.GetString("musicKey", "on");

        if (musicKey == "on")
        {
            mB_Music = true;
        }
        else
        {
            mB_Music = false;
        }

        if (!mB_Music)
        {
            SwapAudio.GetInstance().SetMusicVolume(0f);
        }

        string soundKey = PlayerPrefs.GetString("soundKey", "on");

        if (soundKey == "on")
        {
            mB_Sounds = true;
        }
        else
        {
            mB_Sounds = false;
        }

        if (!mB_Sounds)
        {
            SwapAudio.GetInstance().SetFXVolume(0f);
        }

        string charsString = PlayerPrefs.GetString("Playerinfo");
		Debug.Log(charsString);
		if( string.IsNullOrEmpty(charsString))
		{
			Debug.Log("No saved data from player");
			mI_HighScore = 0;
			mI_Money     = 0;
			return;
		}
		Dictionary<string,object> dictLoad;
		if(charsString != string.Empty)
			dictLoad = (Dictionary<string,object>)MiniJSON.Json.Deserialize(charsString);
		else
			return;

        //mB_Music = false;
        //mB_Sounds = false;

        playerInfo = dictLoad;

        string h = string.Empty;
		string c = string.Empty;
		string d = string.Empty;
		string r = string.Empty;
		if(dictLoad.ContainsKey("HighScore"))
		{
			h = dictLoad["HighScore"].ToString();
			if( !int.TryParse(h,out mI_HighScore) )
				Debug.LogError("Highscore has an incorrect format");
			//Debug.Log("Loaded Highscore = "+mI_HighScore);
		}
		if(dictLoad.ContainsKey("Coins"))
		{
			c = dictLoad["Coins"].ToString();
			if( !int.TryParse(c,out mI_Money) )
				Debug.LogError("Money has an incorrect format");
		}
		if(dictLoad.ContainsKey("NextGift"))
		{
			d = dictLoad["NextGift"].ToString();
			if(!DateTime.TryParse(d,out NextGiftDate))
				Debug.LogError("Next Date has an incorrect format");
		}
		if(dictLoad.ContainsKey("PlayerAnim"))
		{
			d = dictLoad["PlayerAnim"].ToString();
            Debug.Log("Spritename " + d);
            Sprite[] tList = WindowManager.GetInstance().AllCharactersWindow.mA_AllCharactersSprites;
			foreach(Sprite data in tList)
			{
                Debug.Log(data.name);
				if(data.name == d)
				{
                    Debug.Log("found fucking sprite " + d);
					IngameManager.GetInstance().mC_CurrentSprite = data;
                    Debug.Log("Renderer name " + IngameManager.GetInstance().playerController.myRenderer.gameObject.name);
					IngameManager.GetInstance().playerController.myRenderer.sprite = data;
                    IngameManager.GetInstance().playerController.myRenderer.material.mainTexture = IngameManager.GetInstance().playerController.myRenderer.sprite.texture;
                    IngameManager.GetInstance().playerController.myAnimator.Play("Idle");
					break;
				}
			}

            //if(IngameManager.GetInstance().playerController.myRenderer.sprite == null)
            //{
            //    IngameManager.GetInstance().playerController.myRenderer.sprite = IngameManager.GetInstance().mC_DefaultSprite;
            //}
		}

		if(dictLoad.ContainsKey("Tutorial"))
		{
			d = dictLoad["Tutorial"].ToString().ToLower();
			if(d == "false")
				ShowTutorial = false;
			else
				ShowTutorial = true;
		}

		if(dictLoad.ContainsKey("ShowAds"))
		{
			d = dictLoad["ShowAds"].ToString().ToLower();
			if(d == "false")
				SHOW_ADS = false;
			else
				SHOW_ADS = true;
		}

		if(dictLoad.ContainsKey("FirstVideo"))
		{
			d = dictLoad["FirstVideo"].ToString();
			FIRST_VIDEO = short.Parse(d);
		}
		if(dictLoad.ContainsKey("VideoIt"))
		{
			d = dictLoad["VideoIt"].ToString();
			VIDEO_IT = short.Parse(d);
		}

		if(dictLoad.ContainsKey("Shield"))
		{
			d = dictLoad["Shield"].ToString();
			mI_ShieldPowerUps = int.Parse(d);
		}
		if(dictLoad.ContainsKey("Freeze"))
		{
			d = dictLoad["Freeze"].ToString();
			mI_FreezePowerUps = int.Parse(d);
		}
		if(dictLoad.ContainsKey("Sand"))
		{
			d = dictLoad["Sand"].ToString();
			mI_SandClockPowerUps = int.Parse(d);
		}

		//mB_Music = true;
        /*
		if(dictLoad.ContainsKey("Music"))
		{
			h = dictLoad["Music"].ToString();
			if(h == "False")
				mB_Music = false;
		}

		mB_Sounds = true;
		if(dictLoad.ContainsKey("Sounds"))
		{
			h = dictLoad["Sounds"].ToString();
			if(h == "False")
				mB_Sounds = false;
		}
        */
		//mB_Music = !mB_Music;
		//mB_Sounds = !mB_Sounds;
        /*
		if(!mB_Sounds)
		{
			SwapAudio.GetInstance().SetFXVolume(0f);
		}
        */
		//TurnMusic();
		//TurnSounds();


		mB_Rated = false;
		if(dictLoad.ContainsKey("Rate"))
		{
			r = dictLoad["Rate"].ToString();
			if(r == "True")
				mB_Rated = true;
		}
	}

	public void SavePlayerInfo()
	{
		Dictionary<string,object> dic = new Dictionary<string, object>();
		//Debug.Log("Saved Highscore = "+mI_HighScore);
		dic.Add("HighScore",mI_HighScore);
		dic.Add("Coins" ,	mI_Money);
		dic.Add("NextGift", NextGiftDate.ToString());

		dic.Add("FirstVideo",FIRST_VIDEO.ToString());
		dic.Add("VideoIt",VIDEO_IT.ToString());
		dic.Add("ShowAds",SHOW_ADS.ToString());
		dic.Add("Tutorial",ShowTutorial.ToString());

        // Set character animator
        Debug.Log("Setting default player anim " + IngameManager.GetInstance().playerController.myRenderer.sprite.name);
		dic.Add("PlayerAnim",IngameManager.GetInstance().playerController.myRenderer.sprite.name);

		dic.Add("Shield",mI_ShieldPowerUps.ToString());
		dic.Add("Sand",mI_SandClockPowerUps.ToString());
		dic.Add("Freeze",mI_FreezePowerUps.ToString());

		dic.Add("Rate",		mB_Rated.ToString());
		//dic.Add("Music",	mB_Music.ToString());
		//dic.Add("Sounds",	mB_Sounds.ToString());
		PlayerPrefs.SetString("Playerinfo",MiniJSON.Json.Serialize(dic));
	}

	public void UpdateHighScore(int amount)
	{
		mI_HighScore = amount;
		SavePlayerInfo();
	}

	public void UpdateMoney(int amount)
	{
		mI_Money = amount;
		SavePlayerInfo();
	}

	DateTime GetDate(DateTime current)
	{
		int year	= current.Year;
		int month	= current.Month;
		int day     = current.Day;
		int hour	= current.Hour;
		int min		= current.Minute;

		if(current.Minute + 30 >= 60)
		{
			min = (current.Minute+30)%60;
			if(hour+1 >= 24)
			{
				hour = 0;//(hour+1)%24;
				int monC = month;
				if(monC== 1 || monC == 3 || monC == 5 || monC == 7 || monC == 8 || monC == 10 || monC == 12)
				{
					if(day+1 > 31)
					{
						day = 1;
						month++;
					}
					else
					{
						day++;
					}
				}
				else if(monC==2)
				{
					if(year%4==0)
					{
						if(day+1 > 29)
						{
							day=1;
							month++;
						}
						else if(day+1>28)
						{
							day=1;
							month++;
						}
						else
						{
							day++;
						}
					}
				}
				else
				{
					if(day+1 > 30)
					{
						day = 1;
						month++;
					}
					else
					{
						day++;
					}
				}
				if(month>12)
				{
					month = 1;
					year++;
				}
			}
			else
			{
				hour++;
			}
		}
		else
		{
			min += 30;
		}

		return new DateTime(year, month, day,
		                    hour, min  , current.Second);
	}

	public void SetNotification(string notMSG)
	{
		if(!mB_Notifications)
		{
			NextGiftDate = DateTime.UtcNow.AddMinutes( CONSTANTS.MINUTES_SURPRISE );
			print(NextGiftDate);
			//Save Date
			SavePlayerInfo();
			return;
		}
#if UNITY_IOS
		UnityEngine.iOS.LocalNotification not = new UnityEngine.iOS.LocalNotification();
		NextGiftDate = DateTime.Now.AddMinutes( CONSTANTS.MINUTES_SURPRISE );// GetDate(DateTime.Now);
		not.alertBody = notMSG;
		not.fireDate = NextGiftDate;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(not);
#endif
	}

	public bool TurnMusic()
	{
		mB_Music = !mB_Music;
		float volume;
        if (mB_Music)
        {
            volume = 88f;
        }
        else
        {
            volume = 0f;
        }
		SwapAudio.GetInstance().SetMusicVolume(volume);
		SavePlayerInfo();
		return mB_Music;
	}

	public bool TurnSounds()
	{
		mB_Sounds = !mB_Sounds;
		float volume;
        if (mB_Sounds)
        {
            volume = 80f;
        }
        else
        {
            volume = 0f;
        }
		SwapAudio.GetInstance().SetFXVolume(volume);
		SavePlayerInfo();
		return mB_Sounds;
	}

	public void SendToRate()
	{
		string url = string.Empty;
#if UNITY_IOS
		url	= "https://www.youtube.com/";
#elif UNITY_ANDROID
		url = "https://www.youtube.com/";
#else
		url = "www.google.com.mx";
#endif
		Application.OpenURL(url);
		mB_Rated = true;
		SavePlayerInfo();
	}

	public Dictionary<string,Sprite> getAvaibleCharacters()
	{
		return mD_AvaibleCharacters;
	}

	public void SpendSandClock()
	{
		mI_SandClockPowerUps--;
		SavePlayerInfo();
	}

	public void SpendShield()
	{
		mI_ShieldPowerUps--;
		SavePlayerInfo();
	}

	public void SpendFreeze()
	{
		mI_FreezePowerUps--;
		SavePlayerInfo();
	}

	#region PLUGINS
	int chartIt=0;
	bool videoShowed = false;
	void Localytics_OnLocalyticsDidDismissInAppMessage()
	{
		//Debug.Log("Localytics_OnLocalyticsDidDismissInAppMessage");
	}

	void Localytics_OnLocalyticsDidDisplayInAppMessage()
	{
		//Debug.Log("Localytics_OnLocalyticsDidDisplayInAppMessage");
	}

	void Localytics_OnLocalyticsWillDismissInAppMessage()
	{
		//Debug.Log("Localytics_OnLocalyticsWillDismissInAppMessage");
	}

	void Localytics_OnLocalyticsWillDisplayInAppMessage()
	{
		//Debug.Log("Localytics_OnLocalyticsWillDisplayInAppMessage");
	}



	public bool CanSeeVideoReward()
	{
		//if(DateTime.Compare(DateTime.UtcNow,GameManager.GetInstance().NextGiftDate.AddMinutes( CONSTANTS.MINUTES_SURPRISE )) > 0)
		if(VIDEO_IT == 0)
		{
			//if(DateTime.Compare(DateTime.UtcNow,NextVideoDate.AddMinutes( CONSTANTS.MINUTES_REWARD )) > 0)
			return true;
		}
		//Debug.Log("You cant see a video yet");
		return false;
	}

	void CheckADC()
	{
		CheckStatus(mL_ZoneIds[0]);
		CheckStatus(mL_ZoneIds[1]);
		CheckStatus(mL_ZoneIds[2]);
		CheckStatus(mL_ZoneIds[3]);
	}

	void CheckStatus(string zoneID)
	{
		//UNITY_IOS
#if !UNITY_EDITOR 
		//string status = AdColony.StatusForZone(zoneID);
		//Debug.Log("*** Ad Colony Status for zone ("+zoneID+") = "+status);
#endif
	}

	bool videoStart = false;
	DateTime VideoStart;
	DateTime VideoFinished;
	/// <summary>
	/// Play an AdColony video.
	/// </summary>
	/// <param name="zoneId">Zone identifier. Default is Debug/Dev zone id</param>
	public void ADcolonyVideo(string zoneId = "vz2ae853ded69741839a")
	{
#if !UNITY_ANDROID && !UNITY_IOS
		Debug.LogWarning("Ad Colony is disabled for the current platform");
		return;
#endif
		if(mL_AvaibleZones.Count > 0)
		{
			zoneId = mL_AvaibleZones[0];
		}
		else
		{
			Debug.LogWarning("No video avaible");
		}

#if !UNITY_EDITOR
		/*
		if(AdColony.IsVideoAvailable(zoneId))//mS_ZoneId
		{
			Debug.Log("Play AdColony Video");
			GoogleAnalyticsLog("VideoAd","Watch");
			// Call AdColony.ShowVideoAd with that zone to play an interstitial video.
			// Note that you should also pause your game here (audio, etc.) AdColony will not
			// pause your app for you.
			//AdColony.ShowVideoAd(zoneId);//mS_ZoneId

			videoStart = true;
			VideoStart = DateTime.UtcNow;

			SavePlayerInfo();
		}
		else
		{
			// Use other Plug-In Video
			Debug.Log("Zone Video is unavaible, reproducing default parameter");
			//AdColony.ShowVideoAd(zoneId);//mS_ZoneId
		}
		*/
#endif
	}

	void ADCStatusChanged(bool status,string zone)
	{
		if(status)
		{
			Debug.Log("Zone ("+zone+") can now be showed");
			if(mL_AvaibleZones.Contains(zone))
				Debug.Log("Video Zone ("+zone+") is already added");
			else
				mL_AvaibleZones.Add(zone);
		}
		else
		{
			Debug.Log("Zone ("+zone+") isnt avaible");
			if(mL_AvaibleZones.Contains(zone))
				mL_AvaibleZones.Remove(zone);
			else
				Debug.LogWarning("Zone ("+zone+") isnt in the list !!!");
		}
	}

	public bool IsVideoAvaible()
	{
		//Check First AdColony
		if(mL_AvaibleZones.Count > 0)
			return true;
		return false;
	}


	bool VideoLasted()
	{
		/*
		 * t1
			The first object to compare.
		 * t2
			The second object to compare.
		Less than zero
			t1 is earlier than t2.
		Zero
			t1 is the same as t2.
		Greater than zero
			t1 is later than t2.
		 */
		if(DateTime.Compare(VideoStart,VideoFinished) < 0)
		{
			//Video lasted less than 5 seconds
			if(DateTime.Compare( VideoStart.AddSeconds( 3 ), VideoFinished) > 0)
			{
				//Debug.Log("Video lasted less than 3 seconds");
				return false;
			}
			//Debug.Log("Video lasted more than 3 seconds");
			return true;
		}
		//Debug.Log("Rare stuff");
		return false;
	}

	void OnVideoStarted()
	{
		//Debug.Log(" *** Ad Colony Video started");
	}

	void OnVideoFinished(bool ad_was_shown)
	{
		if(videoStart)
		{
			VideoFinished = DateTime.UtcNow;
			videoStart = false;
			if(VideoLasted())
			{
				mB_Reward = true;
				// Give reward ?
				//Debug.Log("        *** Video was shown");
				if(currDelegate.isValid)
				{
					if(videoShowed == false) 
					{
						if(currDelegate.methodName == "RewardVideo")
						{
							ExecuteDelegate();
						}
					}
					else
						Debug.LogWarning("Delegate isnt RewardVideo method");
				}
				else
				{
					//Debug.Log("Delegate wasnt valid or set");
				}
			}
			return;
		}
		//Debug.Log("On Video Finished ("+ad_was_shown.ToString()+")");
		// Resume your app here.
	}
	
	[HideInInspector]
	public bool mB_Reward = false;

    /*
	void didCompleteRewardedVideo(CBLocation location, int reward) 
	{
		//Debug.Log("didCompleteRewardedVideo, give reward");
		mB_Reward = true;
		if(currDelegate.isValid)
		{
			//Debug.Log("A delegate is active :)");
			if(currDelegate.methodName == "RewardVideo")
				ExecuteDelegate();
			else
				Debug.LogWarning("Delegate isnt RewardVideo method");
		}
		else
			Debug.Log("There isnt any delegate assigned");
	}

	void didCloseRewardedVideo(CBLocation location) 
	{
		//Debug.Log("didCloseRewardedVideo, cancel reward");
		mB_Reward = false;
		if(currDelegate.methodName == "RewardVideo")
			ExecuteDelegate();
		else
			Debug.LogWarning("Delegate isnt RewardVideo method");
	}

	void didDisplayRewardedVideo(CBLocation location)
	{
		//Debug.Log("didDisplayRewardedVideo: " + location);
	}

	void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error) {
		//Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
	}
	
	void didDismissRewardedVideo(CBLocation location) {
		//Debug.Log("didDismissRewardedVideo: " + location);
	}

	void willDisplayVideo(CBLocation location) {
		//Debug.Log("willDisplayVideo: " + location);
	}

	void didCacheRewardedVideo(CBLocation location) {
		//Debug.Log("didCacheRewardedVideo: " + location);
	}

	void didCacheIntersitial(CBLocation location)
	{
		Debug.Log("Intersitial cached");
	}

	void didFailedtoLoadIntersitial(CBLocation location, CBImpressionError error)
	{
		Debug.Log(string.Format("didFailToLoadIntersitial: {0} at location {1}", error, location));
	}

	bool shouldDisplayRewardedVideo(CBLocation location) 
	{
		//Debug.Log("shouldDisplayRewardedVideo @" + location + " : " + mB_Reward);
		return mB_Reward;
	}
    */
	public void LocalyticsLog(string _event)
	{
		#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID )
		/*
		Localytics.TagScreen(_event);
		Localytics.Upload();
		*/
		#endif
	}

	public void GoogleAnalyticsLog(string action,string log)
	{
#if UNITY_EDITOR
		Debug.Log(string.Format("Google Analtycis Log: Action ->{0}, Log->{1}",action,log) );
#else
		//googleAnalytics.LogEvent("Swipe Santa Out",action,log,1);
#endif
	}

	#endregion

	#region CONTROL

	EventDelegate currDelegate;
	public void SetDelegate(EventDelegate del)
	{
		currDelegate = del;
	}

	public void ExecuteDelegate()
	{
		currDelegate.Execute();
		currDelegate.Clear();
	}

	#endregion

	#region IAP

	public void RestorePurchases()
	{
		//SoomlaStore.RestoreTransactions();
		return;
#if !UNITY_IOS
		Debug.Log("Restore Purchases not avaible for current platform");//OpenIAB.restoreTransactions();
#endif
	}

	public static string charToUnlock = string.Empty;
	public void CallStore(string keyStore)
	{
		//SoomlaStore.BuyMarketItem(keyStore,"Complete!");
		//OpenIAB.purchaseProduct(keyStore);
	}

	public void UnlockAllCharacters()
	{
        List<string> keys = new List<string>(mD_Characters.Keys);
        foreach(string key in keys)
        {
            mD_Characters[key] = true;
        }
		SaveCharacters();
	}

	public void DisableAds()
	{
		SHOW_ADS = false;
		SavePlayerInfo();
	}

	/*
	public void billingSupported()
	{
		Debug.Log("Billing is supported :)");
	}

	public void billingNoSupported(string msg)
	{
		Debug.LogError("Billing not supported MSG error -> "+msg);
	}

	public void onPurchaseSucceed(Purchase pur)
	{
		if(pur.Sku == CONSTANTS.NO_ADS)
		{
			DisableAds();
		}
		else if(pur.Sku == CONSTANTS.ALL_CHARS)
		{
			UnlockAllCharacters();
		}
		else if(pur.Sku == CONSTANTS.ALL_CHARS_NO_ADS)
		{
			DisableAds();
			UnlockAllCharacters();
		}
		else if(pur.Sku == CONSTANTS.ONE_CHAR)
		{
			Debug.Log("Unlocking char ->"+charToUnlock);
			UnlockCharacter(charToUnlock,charToUnlock);
		}
		else if(pur.Sku == CONSTANTS.POWER_PACK)
		{
			mI_FreezePowerUps    += 10;
			mI_SandClockPowerUps += 10;
			mI_ShieldPowerUps    += 10;
			SavePlayerInfo();
		}
		else
		{
			Debug.LogError("Unknown purchase |||| "+pur);
		}
	}

	public void onPurchaseFailed(int dul, string message)
	{
		Debug.LogError("Purchase failed ("+dul+") with message -> "+message);
	}

	public void restoreFailed(string msg)
	{
		Debug.LogError("Restore Failed with msg ->"+msg);
	}

	public void restoreSucceeded()
	{
		Debug.Log("Restore was made succesfully");
		//Restore purchases here
	}
	*/

        /*
	public void onMarketPurchaseStarted(PurchasableVirtualItem pvi) 
	{
		//Debug.Log("Market purchase started with itemId ="+pvi.ItemId);
		// pvi - the PurchasableVirtualItem whose purchase operation has just started
		
		// ... your game specific implementation here ...
	}
    */

        /*
	public void onMarketPurchase(PurchasableVirtualItem pvi, string payload,Dictionary<string, string> extra) 
	{
		Debug.Log("Market purchase with itemId ="+pvi.ItemId);

		if(pvi.ItemId.ToLower() == SantaStoreAssets.ALL_CHARS.ToLower())
		{
			UnlockAllCharacters();
		}
		else if(pvi.ItemId.ToLower() == SantaStoreAssets.ALL_CHARS_NO_ADS.ToLower())
		{
			UnlockAllCharacters();
			SaveCharacters();
		}
		else if(pvi.ItemId.ToLower() == SantaStoreAssets.NO_ADS.ToLower())
		{
			DisableAds();
		}
		else if(pvi.ItemId.ToLower() == SantaStoreAssets.ONE_CHAR.ToLower())
		{
			//Debug.Log("Unlocking char ->"+charToUnlock);
			UnlockCharacter(charToUnlock,charToUnlock);
		}
		else if(pvi.ItemId.ToLower() == SantaStoreAssets.POWER_PACK.ToLower())
		{
			mI_FreezePowerUps    += 5;
			mI_SandClockPowerUps += 5;
			mI_ShieldPowerUps    += 5;
			SavePlayerInfo();

			if(IngameManager.GetInstance().mB_isPlaying)
			{
				// Update labels
				UI_Manager.GetInstance().FreezeLabel.text = mI_FreezePowerUps.ToString();
				UI_Manager.GetInstance().SandLabel.text = mI_SandClockPowerUps.ToString();
				UI_Manager.GetInstance().ShieldLabel.text = mI_ShieldPowerUps.ToString();

				// Resume game
				IngameManager.GetInstance().StoreResume();
			}
		}
		else
		{
			Debug.LogError("Unknown item buyed");
		}
		// pvi - the PurchasableVirtualItem that was just purchased
		// payload - a text that you can give when you initiate the purchase operation and
		//    you want to receive back upon completion
		// extra - contains platform specific information about the market purchase
		//    Android: The "extra" dictionary will contain: 'token', 'orderId', 'originalJson', 'signature', 'userId'
		//    iOS: The "extra" dictionary will contain: 'receiptUrl', 'transactionIdentifier', 'receiptBase64', 'transactionDate', 'originalTransactionDate', 'originalTransactionIdentifier'
		
		// ... your game specific implementation here ...
	}
    */

        /*
	public void onMarketPurchaseCancelled(PurchasableVirtualItem pvi) 
	{
		// pvi - the PurchasableVirtualItem whose purchase operation was cancelled
		//Debug.Log("Market purchase canceled with itemId ="+pvi.ItemId);
		// ... your game specific implementation here ...

		if(pvi.ID == CONSTANTS.POWER_PACK || pvi.ItemId == CONSTANTS.POWER_PACK || 
		   pvi.Name == CONSTANTS.POWER_PACK )
		{
			if(IngameManager.GetInstance().mB_isPlaying)
			{
				// Resume game
				IngameManager.GetInstance().StoreResume();
			}
		}
	}
    */

	public void onRestoreTransactionsStarted() 
	{
		// ... your game specific implementation here ...
		//Debug.Log("onRestoreTransactionsStarted");
	}

	public void onRestoreTransactionsFinished(bool success) 
	{
		//Debug.Log("onRestoreTransactionsFinished with state="+success.ToString());
		// success - true if the restore transactions operation has succeeded
		
		// ... your game specific implementation here ...
	}
	#endregion


#if UNITY_EDITOR
	void DebugFillCharacters()
	{
		mD_Characters.Add("PirateBandana",true);
		mD_Characters.Add("PirateCaptain",false);
		mD_Characters.Add("Pirate_Moustache",false);
		mD_Characters.Add("Pirate_Chef",false);
		SaveCharacters();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.R))
		{
			UpdateMoney(0);
			UpdateHighScore(0);
		}
		if(Input.GetKeyDown(KeyCode.V))
		{
			PlayerPrefs.SetString("Playerinfo",string.Empty);
		}
		if(Input.GetKeyDown(KeyCode.F))
		{
			PlayerPrefs.SetString("Characters",string.Empty);
		}
	}
#endif
}
