﻿using UnityEngine;
using System.Collections;

public class Math2D 
{
	public enum Quadrant
	{
		NONE = 0,
		NORTH_WEST,
		NORTH_EAST,
		SOUTH_EAST,
		SOUTH_WEST
	}

	static public float Map(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	/// <summary>
	/// Returns the Difference between A and B
	/// </summary>
	/// <returns>The range.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	static public float GetRange(float a,float b)
	{
		a = Mathf.Abs(a);
		b = Mathf.Abs(b);
		float range = 1f;
		if(a > b)
			range = a-b;
		else
			range = b-a;

		return range;
	}

	static public Vector2 GetZeroPositionDisplacement(Quadrant quad,Vector2 Position)
	{
		Vector2 _offset = new Vector2(0f,0f);
		switch(quad)
		{
		case Quadrant.SOUTH_EAST:
			_offset = new Vector2( -Position.x, Mathf.Abs(Position.y) );
			break;
		case Quadrant.SOUTH_WEST:
			_offset = new Vector2( Mathf.Abs(Position.x), Mathf.Abs(Position.y) );
			break;
		case Quadrant.NORTH_EAST:
			_offset = new Vector2( -Position.x, -Position.y );
			break;
		case Quadrant.NORTH_WEST:
			_offset = new Vector2( Mathf.Abs(Position.x), -Position.y );
			break;
		}
		return _offset;
	}

	/// <summary>
	/// Gets the rotation z.
	/// </summary>
	/// <returns>The rotation z.</returns>
	/// <param name="TargetPosition">Target position.</param>
	/// <param name="MyPosition">My position.</param>
	static public float GetRotationZAxis(Vector2 TargetPosition,Vector2 MyPosition)
	{
		float Z = 0f;

		Quadrant zone = Quadrant.NONE;

		//Position the Target in Position 0,0 (X,Y)
		if( TargetPosition.x < 0f && TargetPosition.y < 0f )
			zone = Quadrant.SOUTH_WEST;
		else if( TargetPosition.x < 0f && TargetPosition.y > 0f)
			zone = Quadrant.NORTH_WEST;
		else if( TargetPosition.x > 0f && TargetPosition.y > 0f)
			zone = Quadrant.NORTH_EAST;
		else if( TargetPosition.x > 0f && TargetPosition.y < 0f)
			zone = Quadrant.SOUTH_EAST;

		Vector2 offset = GetZeroPositionDisplacement(zone,TargetPosition);
	
		TargetPosition += offset;
		MyPosition     += offset;

		//Calculate the Distance

		//c² = a²+b² - 2ab cosC
		// 2ab cosC = 0 -> C = 90º -> cos90 = 0
		float distance = Mathf.Sqrt( ( Mathf.Pow( MyPosition.x,2) + Mathf.Pow(MyPosition.y,2) ) );

		// Calculate AngleZ
		// A = sin-1 ( a*sinC - c )
		// sincC = sin90 = 1
		Z = Mathf.Asin( MyPosition.x - distance );

		// Make the Conversion
		Z = - (180f+Z);

		return Z;
	}

	/// <summary>
	/// Gets the rotation z.
	/// </summary>
	/// <returns>The rotation z.</returns>
	/// <param name="TargetPosition">Target position.</param>
	/// <param name="MyPosition">My position.</param>
	static public Vector3 GetRotationZ(Vector2 TargetPosition,Vector2 MyPosition)
	{
		float Z = 0f;
		
		Quadrant zone = Quadrant.NONE;
		
		//Position the Target in Position 0,0 (X,Y)
		if( TargetPosition.x < 0f && TargetPosition.y < 0f )
			zone = Quadrant.SOUTH_WEST;
		else if( TargetPosition.x < 0f && TargetPosition.y > 0f)
			zone = Quadrant.NORTH_WEST;
		else if( TargetPosition.x > 0f && TargetPosition.y > 0f)
			zone = Quadrant.NORTH_EAST;
		else if( TargetPosition.x > 0f && TargetPosition.y < 0f)
			zone = Quadrant.SOUTH_EAST;
		
		Vector2 offset = GetZeroPositionDisplacement(zone,TargetPosition);
		
		TargetPosition += offset;
		MyPosition     += offset;
		
		//Calculate the Distance
		
		//c² = a²+b² - 2ab cosC
		// 2ab cosC = 0 -> C = 90º -> cos90 = 0
		float distance = Mathf.Sqrt( ( Mathf.Pow( MyPosition.x,2) + Mathf.Pow(MyPosition.y,2) ) );
		
		// Calculate AngleZ
		// A = sin-1 ( a*sinC - c )
		// sincC = sin90 = 1
		float SIN = MyPosition.x / distance;
 		Z = Mathf.Asin( SIN );
		Z *= Mathf.Rad2Deg;
		// Make the Conversion

		if(MyPosition.y > 0f)
		{
			if(offset.y <= 0f)
			{
				Z = 180-Z;
			}
			else
			{
				Z = - (180f+Z);
			}
		}

		//Z = - (1

		Vector3 Rotation = new Vector3(0f,0f,Z);

		return Rotation;
	}

	static public Vector3 SetX(float x,Vector3 pos)
	{
		return  new Vector3(x,pos.y,pos.z);
	}

	static public Vector3 SetY(float y,Vector3 pos)
	{
		return  new Vector3(pos.x,y,pos.z);
	}

	static public Vector3 SetZ(float z,Vector3 pos)
	{
		return  new Vector3(pos.x,pos.y,z);
	}
}
