﻿using System.Collections;
using GlobalData;

public class State
{
	string name;
	GameEntity 	owner;
	int 	miState;

	public string STATE_NAME
	{
		get{return name;}
		set{name = value;}
	}

	public int STATE
	{
		get{return miState;}
		set{ miState = value;}
	}

	public GameEntity ENTITY  
	{
		get{return owner;}
		set{owner = value;}
	}

	public virtual void Enter(){}
	public virtual void Execute(float delta){}
	public virtual void Exit(){}
}

