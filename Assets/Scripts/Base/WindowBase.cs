﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindowBase : MonoBehaviour 
{
	private Transform myTransform = null;
	private GameObject myGameobject = null;

	public WINDOWSTYPES myWindowType;


	public virtual void Initialize()
	{
		myTransform = this.gameObject.GetComponent<Transform>() as Transform;
		myGameobject = this.gameObject;
	}

	public virtual void OnEnter()
	{
		myGameobject.SetActive(true);
	}

	public virtual void OnExit()
	{
		myGameobject.SetActive(false);
	}

	public virtual void Close()
	{
		WindowManager.GetInstance().HideWindow(myWindowType);
	}

}
