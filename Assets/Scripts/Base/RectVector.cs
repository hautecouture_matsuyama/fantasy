﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RectVector 
{
	//Vector
	public float Left;
	public float Right;
	public float Top;
	public float Bottom;

	public Vector2 TopLeft;
	public Vector2 TopRight;
	public Vector2 BottomLeft;
	public Vector2 BottomRight;

	public Vector2 GlobalPosition;

	///Constructor by static values
	public RectVector(float _left,float _right,float _top,float _bottom)
	{
		Left   = _left;
		Right  = _right;
		Top    = _top;
		Bottom = _bottom;

		TopLeft  = new Vector2(_left,_top);
		TopRight = new Vector2(_right,_top);
		BottomLeft = new Vector2(_left,_bottom);
		BottomRight = new Vector2(_right,_bottom);

		GlobalPosition = GetCentralPosition();
	}

	///Constructor by another RecVector
	public RectVector(RectVector recVec)
	{
		Left   = recVec.Left;
		Right  = recVec.Right;
		Top    = recVec.Top;
		Bottom = recVec.Bottom;

		TopLeft = recVec.TopLeft;
		TopRight= recVec.TopRight;
		BottomLeft = recVec.BottomLeft;
		BottomRight= recVec.BottomRight;

		GlobalPosition = recVec.GlobalPosition;
	}

	/// <summary>
	/// Sets the new rect.
	/// </summary>
	/// <param name="_left">_left.</param>
	/// <param name="_right">_right.</param>
	/// <param name="_top">_top.</param>
	/// <param name="_bottom">_bottom.</param>
	public void SetNewRect(float _left,float _right,float _top,float _bottom)
	{
		Left   = _left;
		Right  = _right;
		Top    = _top;
		Bottom = _bottom;

		TopLeft  = new Vector2(_left,_top);
		TopRight = new Vector2(_right,_top);
		BottomLeft = new Vector2(_left,_bottom);
		BottomRight = new Vector2(_right,_bottom);

		GlobalPosition = GetCentralPosition();
	}

	/// <summary>
	/// Check if the vector is inside
	/// </summary>
	/// <returns><c>true</c> if this instance is inside the specified point; otherwise, <c>false</c>.</returns>
	/// <param name="point">Point.</param>
	public bool IsInside(Vector3 point)
	{
		if( point.x >= Left && point.x <= Right)
		{
			if(point.y >= Bottom && point.y <= Top)
			{
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Determines whether this RectVector is inside the specified RVector.
	/// </summary>
	/// <returns><c>true</c> if this RectVector is inside the specified RVector; otherwise, <c>false</c>.</returns>
	/// <param name="rectvector">Rectvector.</param>
	public bool IsInside(RectVector RVector)
	{
		{//Check TopLeft
			if(TopLeft.x >= RVector.Left && TopLeft.x <= RVector.Right)
				if(TopLeft.y >= RVector.Bottom && TopLeft.y <= RVector.Top)
					return true;
		}
		{//Check TopRight
			if(TopRight.x >= RVector.Left && TopRight.x <= RVector.Right)
				if(TopRight.y >= RVector.Bottom && TopRight.y <= RVector.Top)
					return true;
		}
		{//Check BottomLeft
			if(BottomLeft.x >= RVector.Left && BottomLeft.x <= RVector.Right)
				if(BottomLeft.y >= RVector.Bottom && BottomLeft.y <= RVector.Top)
					return true;
		}
		{//Check BottomRight
			if(BottomRight.x >= RVector.Left && BottomRight.x <= RVector.Right)
				if(BottomRight.y >= RVector.Bottom && BottomRight.y <= RVector.Top)
					return true;
		}

		{//Check Top
			if(RVector.Top >= Bottom && RVector.Top <= Top)
				if(RVector.GlobalPosition.x >= Left && RVector.GlobalPosition.x <= Right)
					return true;
		}
		{//Check Bottom
			if(RVector.Bottom >= Bottom && RVector.Bottom <= Top)
				if(RVector.GlobalPosition.x >= Left && RVector.GlobalPosition.x <= Right)
					return true;
		}
		{//Check Left
			if(RVector.Left >= Left && RVector.Left <= Right)
				if(RVector.GlobalPosition.y >= Bottom && RVector.GlobalPosition.y <= Top)
					return true;
		}
		{//Check Right
			if(RVector.Right >= Left && RVector.Right <= Right)
				if(RVector.GlobalPosition.y >= Bottom && RVector.GlobalPosition.y <= Top)
					return true;
		}

		return false;
	}

	public Vector3 GetCentralPosition()
	{
		return new Vector3(((Left+Right)*0.5f),((Top+Bottom)*0.5f),1f);
	}
}
