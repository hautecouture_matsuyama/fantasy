using UnityEngine;
using System.Collections;

public class NavGraphNode : GraphNode
{
	/// <summary>
	/// The Vector3 position of this Node
	/// </summary>
	public Vector3 mV3_Position;

	/// <summary>
	/// The Vector2 position of this Node
	/// </summary>
	public Vector2 mV2_Position;

	/// <summary>
	/// The Vector3 GraphNodePosition
	/// </summary>
	public Vector3 mV3_GraphNodePosition;

	/// <summary>
	/// The Vector2 GraphNodePosition
	/// </summary>
	public Vector2 mV2_GraphNodePosition;
}

