using UnityEngine;
using System.Collections;

public class GraphEdge
{
	/// <summary>
	/// Index of base Node
	/// </summary>
	public int mI_From;

	/// <summary>
	/// Index of deestiny Node
	/// </summary>
	public int mI_To;

	/// <summary>
	/// The cost of traversing the edge
	/// </summary>
	public float mF_Cost;

	/// <summary>
	/// The Source Node
	/// </summary>
	public NavGraphNode mN_SourceNode;

	/// <summary>
	/// The Target Node
	/// </summary>
	public NavGraphNode mN_TargetNode;


	public GraphEdge(int from,int to,float cost)
	{
		mI_From = from;
		mI_To   = to;
		mF_Cost = cost;
	}

	public GraphEdge(int from,int to)
	{
		mI_From = from;
		mI_To = to;
		mF_Cost = 0f;
	}

	public GraphEdge(GraphEdge other)
	{
		mI_From =  	other.mI_From;
		mI_To   = other.mI_To;
		mF_Cost = other.mF_Cost;
	}

	public void SetFrom(int f){ mI_From = f;}

	public void SetTo(int t){ mI_To = t;}

	public void SetCost(float c){ mF_Cost = c; }

	public void SetValues(int f,int t,float c)
	{
		mI_From = f;
		mI_To   = t;
		mF_Cost = c;
	}
}

