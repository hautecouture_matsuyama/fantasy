using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SparseGraph : ScriptableObject
{
	public List<NavGraphNode> mL_NodesList = new List<NavGraphNode>();

	public List<GraphEdge> mL_Edges = new List<GraphEdge>();

	public int mI_NextIndex = 0;

	public virtual void Initialize()
	{
		NavGraphNode[] allNodes = FindObjectsOfType<NavGraphNode>();
		foreach(NavGraphNode data in allNodes)
		{
			mL_NodesList.Add(data);
		}
	}

	public NavGraphNode GetNode(int index)
	{
		if(mL_NodesList.Count > index)
			return mL_NodesList[index];

		Debug.LogWarning("Invalid Index");
		return null;
	}

	public void AddNewNode(NavGraphNode node)
	{
		node.mI_Index = mI_NextIndex;
		mI_NextIndex++;
		mL_NodesList.Add(node);
	}

	public void RemoveNode(NavGraphNode node)
	{
		if(mL_NodesList.Contains(node))
		{
			int index = mL_NodesList.IndexOf(node);
			// Just set it as invalid
			mL_NodesList[index].mI_Index = -1;
			return;
		}
	}

	// Deep First Search Algorithm

	/// <summary>
	/// DFS the specified target from a specified source.
	/// </summary>
	/// <param name="source">Source Node Index.</param>
	/// <param name="target">Target Node Index.</param>
	public virtual bool DFS(int source,int target)
	{
		Dictionary<int,List<NavGraphNode>> AdjacencyNodes = new Dictionary<int, List<NavGraphNode>>();


		List<NavGraphNode> mL_VisitedNodes = new List<NavGraphNode>();

		// Add Source Node
		NavGraphNode sourceNode = this.GetNode(source);
		mL_VisitedNodes.Add( sourceNode );

		int it=1;

		while(it != this.mL_NodesList.Count)
		{


			it++;
		}
		return true;
	}

	/// <summary>
	/// DFS the specified target node from a specified source node
	/// </summary>
	/// <param name="source">Source Node.</param>
	/// <param name="target">Target Target</param>
	public virtual bool DFS(NavGraphNode source,NavGraphNode target)
	{
		return true;
	}

	/// <summary>
	/// Get the DFS path.
	/// </summary>
	/// <returns>The DFS path.</returns>
	/// <param name="source">Source node index.</param>
	/// <param name="target">Target node index.</param>
	public virtual List<NavGraphNode> Get_DFS_Path(int source,int target)
	{
		return null;
	}

	/// <summary>
	/// Gets the DFS path.
	/// </summary>
	/// <returns>The DFS path.</returns>
	/// <param name="source">Source Node.</param>
	/// <param name="target">Target Node.</param>
	public virtual List<NavGraphNode> Get_DFS_Path(NavGraphNode source, NavGraphNode target)
	{
		return null;
	}

	/// <summary>
	/// Astar the specified source and target.
	/// </summary>
	/// <param name="source">Source node index.</param>
	/// <param name="target">Target node index.</param>
	public virtual bool Astar(int source,int target)
	{
		return false;
	}

	/// <summary>
	/// Astar the specified source and target.
	/// </summary>
	/// <param name="source">Source node.</param>
	/// <param name="target">Target node.</param>
	public virtual bool Astar(NavGraphNode source,NavGraphNode target)
	{
		return false;
	}

	/// <summary>
	/// Gets the astar list.
	/// </summary>
	/// <returns>The astar list.</returns>
	/// <param name="source">Source Node index.</param>
	/// <param name="target">Target Node index.</param>
	public virtual List<NavGraphNode> Get_Astar(int source,int target)
	{
		return null;
	}

	/// <summary>
	/// Get_s the astar.
	/// </summary>
	/// <returns>The astar list.</returns>
	/// <param name="source">Source node.</param>
	/// <param name="target">Target node.</param>
	public virtual List<NavGraphNode> Get_Astar(NavGraphNode source,NavGraphNode target)
	{
		return null;
	}

	/// <summary>
	/// Dijstra the specified source and target.
	/// </summary>
	/// <param name="source">Source node index.</param>
	/// <param name="target">Target node index.</param>
	public virtual bool Dijstra(int source,int target)
	{
		return false;
	}
	
	/// <summary>
	/// Dijstra the specified source and target.
	/// </summary>
	/// <param name="source">Source node.</param>
	/// <param name="target">Target node.</param>
	public virtual bool Dijstra(NavGraphNode source,NavGraphNode target)
	{
		return false;
	}
	
	/// <summary>
	/// Gets the Dijstra list.
	/// </summary>
	/// <returns>The astar list.</returns>
	/// <param name="source">Source Node index.</param>
	/// <param name="target">Target Node index.</param>
	public virtual List<NavGraphNode> Get_Dijstra(int source,int target)
	{
		return null;
	}
	
	/// <summary>
	/// Get_s the Dijstra.
	/// </summary>
	/// <returns>The Dijstra list.</returns>
	/// <param name="source">Source node.</param>
	/// <param name="target">Target node.</param>
	public virtual List<NavGraphNode> Get_Dijstra(NavGraphNode source,NavGraphNode target)
	{
		return null;
	}
}

