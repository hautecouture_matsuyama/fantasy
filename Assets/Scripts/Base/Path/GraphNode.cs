﻿using UnityEngine;
using System.Collections;

public class GraphNode : ScriptableObject //MonoBehaviour
{
	public int mI_Index = -1;

	public int mI_Cost = -1;

	public void SetCost(int c)
	{
		mI_Cost = c;
	}

	public void SetIndex(int v)
	{
		mI_Index = v;
	}

	public int GetIndex()
	{	
		return mI_Index;
	}

	public int GetCost()
	{
		return mI_Cost;
	}

	public bool Visited = false;

	private GraphNode parentNode = null;

	public void SetParent(GraphNode p){ parentNode = p; }
	public GraphNode GetParent(){return parentNode;}
}
