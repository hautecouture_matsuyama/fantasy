using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour
{
	public Vector2 A;
	public Vector2 B;

	public float Thickness;

	public GameObject CornerChild, MiddleChild, FinalChild;

	private SpriteRenderer CornerRenderer, MiddleRenderer, FinalRenderer;

	/*
	void Awake()
	{

	}
	*/
	public Line(Vector2 a,Vector2 b,float thick)
	{
		A = a;
		B = b;
		Thickness = thick;
	}

	public void Set(Vector2 a,Vector2 b,float thick)
	{
		A = a;
		B = b;
		Thickness = thick;
	}

	private void GetComponents()
	{
		CornerRenderer = CornerChild.GetComponent<SpriteRenderer>();
		MiddleRenderer = MiddleChild.GetComponent<SpriteRenderer>();
		FinalRenderer  = FinalChild.GetComponent<SpriteRenderer>();
	}

	public void SetColor(Color color)
	{
		if(CornerRenderer == null || MiddleRenderer == null || FinalRenderer == null )
			GetComponents();

		CornerRenderer.color = color;
		MiddleRenderer.color = color;
		FinalRenderer.color = color;
	}

	public void Draw()
	{
		Vector2 difference = B - A;
		float rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
		
		//Set the scale of the line to reflect length and thickness
		MiddleChild.transform.localScale = new Vector3(100 * (difference.magnitude / MiddleRenderer.sprite.rect.width), 
		                                             Thickness, 
		                                             MiddleChild.transform.localScale.z);
		
		CornerChild.transform.localScale = new Vector3(CornerChild.transform.localScale.x, 
		                                                 Thickness, 
		                                                 CornerChild.transform.localScale.z);
		
		FinalChild.transform.localScale = new Vector3(FinalChild.transform.localScale.x, 
		                                               Thickness, 
		                                               FinalChild.transform.localScale.z);
		
		//Rotate the line so that it is facing the right direction
		MiddleChild.transform.rotation = Quaternion.Euler(new Vector3(0,0, rotation));
		CornerChild.transform.rotation = Quaternion.Euler(new Vector3(0,0, rotation));
		FinalChild.transform.rotation = Quaternion.Euler(new Vector3(0,0, rotation + 180));
		
		//Move the line to be centered on the starting point
		MiddleChild.transform.position = new Vector3 (A.x, A.y, MiddleChild.transform.position.z);
		CornerChild.transform.position = new Vector3 (A.x, A.y, CornerChild.transform.position.z);
		FinalChild.transform.position = new Vector3 (A.x, A.y, FinalChild.transform.position.z);
		
		//Need to convert rotation to radians at this point for Cos/Sin
		rotation *= Mathf.Deg2Rad;
		
		//Store these so we only have to access once
		float lineChildWorldAdjust = MiddleChild.transform.localScale.x * MiddleRenderer.sprite.rect.width / 2f;
		float startCapChildWorldAdjust = CornerChild.transform.localScale.x *CornerRenderer.sprite.rect.width / 2f;
		float endCapChildWorldAdjust = FinalChild.transform.localScale.x * FinalRenderer.sprite.rect.width / 2f;
		
		//Adjust the middle segment to the appropriate position
		MiddleChild.transform.position += new Vector3 (.01f * Mathf.Cos(rotation) * lineChildWorldAdjust, 
		                                             .01f * Mathf.Sin(rotation) * lineChildWorldAdjust,
		                                             0);
		
		//Adjust the start cap to the appropriate position
		CornerChild.transform.position -= new Vector3 (.01f * Mathf.Cos(rotation) * startCapChildWorldAdjust, 
		                                                 .01f * Mathf.Sin(rotation) * startCapChildWorldAdjust,
		                                                 0);
		
		//Adjust the end cap to the appropriate position
		FinalChild.transform.position += new Vector3 (.01f * Mathf.Cos(rotation) * lineChildWorldAdjust * 2, 
		                                               .01f * Mathf.Sin(rotation) * lineChildWorldAdjust * 2,
		                                               0);
		FinalChild.transform.position += new Vector3 (.01f * Mathf.Cos(rotation) * endCapChildWorldAdjust, 
		                                               .01f * Mathf.Sin(rotation) * endCapChildWorldAdjust,
		                                               0);
	}
}

