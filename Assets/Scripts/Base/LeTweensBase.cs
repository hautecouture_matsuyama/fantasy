﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LeTweensBase : MonoBehaviour 
{

	private int TweenPhase = 0;

	public TweenPosition mT_Position;
	public TweenScale    mT_Scale;
	public TweenRotation mT_Rotation;

	public Vector3[] PositionsPoins;
	public Vector3[] ScalePoints;
	public Vector3[] RotationPoints;

	public float[] PositionDurations;

	public AnimationCurve[] AnimationCurces;

	private bool useCustomAnimationCurves = false;
	private bool reverseTween = false;

	public EventDelegate[] OnReverseNotify;
	public EventDelegate OnFinishNotify;

	private int MaxAnimationLenght;

	public virtual void Init()
	{
		TweenPhase = 0;
		reverseTween = false;
		if(mT_Position)
			MaxAnimationLenght = PositionsPoins.Length;
		else if(mT_Scale)
			MaxAnimationLenght = ScalePoints.Length;

		if(AnimationCurces.Length > 0)
			useCustomAnimationCurves = true;
	}

	public virtual void RevertTween()
	{
		if(PositionsPoins.Length > 0)
			TweenPhase   = PositionsPoins.Length-1;
		else if(ScalePoints.Length > 0)
			TweenPhase = ScalePoints.Length - 1;
			 
		reverseTween = true;
	}

	public virtual void LeTween()
	{
		int nextPhase = 1;
		if(reverseTween)
		{
			TweenPhase--;
			nextPhase = -1;
		}
		else
			TweenPhase++;

		if( ( (TweenPhase+nextPhase) >= MaxAnimationLenght ) || (TweenPhase+nextPhase) < 0)
		{
			if(reverseTween)
			{
				if(OnReverseNotify.Length > 0)
				{
					foreach(EventDelegate eve in OnReverseNotify)
					{
						if(eve != null)
						{
							eve.Execute();
						}
					}
					return;
				}
				return;
			}


			if(OnFinishNotify != null)
			{
				OnFinishNotify.Execute();
			}

			return;
		}

		if(mT_Position)
		{
			mT_Position.from = PositionsPoins[TweenPhase];
			mT_Position.to   = PositionsPoins[TweenPhase+nextPhase];
			if(TweenPhase < PositionDurations.Length)
				mT_Position.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Position.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Position.ResetToBeginning();
			mT_Position.PlayForward();
		}
		if(mT_Scale)
		{
			mT_Scale.from = ScalePoints[TweenPhase];
			mT_Scale.to   = ScalePoints[TweenPhase+nextPhase];
			if(TweenPhase < PositionDurations.Length)
				mT_Scale.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Scale.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Scale.ResetToBeginning();
			mT_Scale.PlayForward();
		}
	}


	public virtual void OnEnter()
	{
		Init();
		if(mT_Position)
		{
			mT_Position.from = PositionsPoins[TweenPhase];
			mT_Position.to   = PositionsPoins[TweenPhase+1];
			if(TweenPhase < PositionDurations.Length)
				mT_Position.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Position.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Position.ResetToBeginning();
			mT_Position.PlayForward();
		}
		if(mT_Scale)
		{
			mT_Scale.from = ScalePoints[TweenPhase];
			mT_Scale.to   = ScalePoints[TweenPhase+1];
			if(TweenPhase < PositionDurations.Length)
				mT_Scale.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Scale.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Scale.ResetToBeginning();
			mT_Scale.PlayForward();
		}
	}

	public virtual void OnExit()
	{
		RevertTween();
		if(mT_Position)
		{
			mT_Position.from = PositionsPoins[TweenPhase];
			mT_Position.to   = PositionsPoins[TweenPhase-1];
			if(TweenPhase < PositionDurations.Length)
				mT_Position.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Position.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Position.ResetToBeginning();
			mT_Position.PlayForward();
		}
		if(mT_Scale)
		{
			mT_Scale.from = ScalePoints[TweenPhase];
			mT_Scale.to   = ScalePoints[TweenPhase-1];
			if(TweenPhase < PositionDurations.Length)
				mT_Scale.duration = PositionDurations[TweenPhase];
			if(useCustomAnimationCurves)
			{
				if(TweenPhase < AnimationCurces.Length)
					mT_Position.animationCurve = AnimationCurces[TweenPhase];
			}
			mT_Scale.ResetToBeginning();
			mT_Scale.PlayForward();
		}
	}
}
