using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Observer : MonoBehaviour
{
	public enum OBSERVER_BEHAVIOUR
	{
		VALUE_CHANGE,
		VALUE_PROXIMITY
	};

	public OBSERVER_BEHAVIOUR Behaviour;
	
	public Object ValueToObserve;

	public float ValueOffset;

	public float DeltaValue;

	public bool UseVector3;
	public Vector3 DeltaValue_V3;

	public bool Active = false;

	public void SetObserver(bool state)
	{
		Active = state;
	}

	void Update()
	{
		if(Active)
		{
			switch(Behaviour)
			{
			case OBSERVER_BEHAVIOUR.VALUE_CHANGE:
				break;
			case OBSERVER_BEHAVIOUR.VALUE_PROXIMITY:
				break;
			}
		}
	}

	void ValueProx()
	{
		if(UseVector3)
		{

		}
		else
		{
			/*float delta = Math2D.GetRange(DeltaValue,(float)ValueToObserve);
			if( delta <= ValueOffset)
			{
				//Notify
			}*/
		}
	}
}

