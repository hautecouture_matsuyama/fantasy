﻿using System.Collections;
using System.Collections.Generic;

public class BData
{
	int data;
}

public class FSM
{
	State						actualState = null;
	State						lastState = null;
	Dictionary<int,State> 		dicStates = null;
	bool 						FSMactive = false;


	public int ACTUAL_STATE
	{
		get
		{
			if(actualState != null)
				return actualState.STATE;
			return -1;
		}
		private set{}
	}

	public int LAST_STATE
	{
		get
		{
			if(lastState != null)
				return lastState.STATE;
			return -1;
		}
		private set{}
	}
	
	public FSM(State initialState)
	{
		dicStates = new Dictionary<int, State>();
		dicStates.Add(initialState.STATE,initialState);
		actualState = initialState;
		lastState = null;
		FSMactive = true;
		actualState.Enter();
	}
	
	public FSM()
	{
		dicStates = new Dictionary<int, State>();
	}


	public void AddState(State state)
	{
		dicStates.Add(state.STATE,state);
	}

	public void StartFSM(State state)
	{
		actualState = state;
		lastState = null;
		FSMactive = true;
		actualState.Enter();
	}

	public void ChangeState(int istate)
	{
		if(dicStates.ContainsKey(istate))
		{
			actualState.Exit();
			lastState = actualState;
			State state = dicStates[istate];
			actualState = state;
			actualState.Enter();
		}
	}
	
	public void ReturnToLastState()
	{
		actualState.Exit();
		State state = actualState;
		actualState = lastState;
		actualState.Enter();
		lastState = state;
	}
	
	public void CustomUpdate(float delta)
	{
		if(FSMactive)
		{
			if(actualState != null)
			{
				actualState.Execute(delta);
			}
		}
	}
	
	
	public void Release()
	{
		dicStates.Clear();
	}

}
