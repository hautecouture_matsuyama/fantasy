﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseEntity : MonoBehaviour 
{
	public bool isActive;

	public string mS_EntityKey;

	/// <summary>
	/// This ID is specally used only for this object in special
	/// </summary>
	public int mI_EntityID;

	/// <summary>
	/// This is a group id
	/// </summary>
	public int mI_EntityGroupID;

	//INPUTS
	public virtual void OnTouched(Vector3 touchPos)
	{
	}
	public virtual void OnDragged(Vector3 dragPos)
	{
	}
	
	public virtual void OnDropped(Vector3 dropPos)
	{
	}
	
	public virtual void OnClick()
	{
		
	}

	public virtual void ReceiveMessage(int msg,GameEntity sender)
	{

	}

}
