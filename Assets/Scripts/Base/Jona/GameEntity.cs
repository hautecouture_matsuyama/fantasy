using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEntity : BaseEntity
{
	protected internal FSM		stateMachine = null;
	
	public FSM FINITE_STATE_MACHINE
	{
		get{return stateMachine;}
		set {}
	}

	// Implement FixedUpdate
	public virtual void FixedUpdate()
	{
		if(stateMachine != null)
			stateMachine.CustomUpdate(Time.fixedDeltaTime);
	}

}

