﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityManager : Singleton<EntityManager>
{
	protected EntityManager () { }

	public struct MSG_INFO
	{
		public float delay;
		public float timeSinceInvoked;


		public GameEntity sender;
		public int receivers;
		public int msg;
	};

	//Pools
	public List<EntityObject> m_PoolsPrefabs = new List<EntityObject>();
	public Dictionary<string,Prefab_Pool> Pools;


	// Entities
	public Dictionary<int,List<GameEntity>> mD_Entities;

	public List<MSG_INFO> mL_MSG_QUEUE;


	public void Initialize()
	{
		DontDestroyOnLoad(this.gameObject);

		mL_MSG_QUEUE = new List<MSG_INFO>();
		mD_Entities = new Dictionary<int, List<GameEntity>>();

		// Create Pools
		CreatePools();
		if(m_PoolsPrefabs!=null)
			if(m_PoolsPrefabs.Count>0)
		{
			EntityObject it = null;
			for(int i=0;i<m_PoolsPrefabs.Count;i++)
			{
				it = m_PoolsPrefabs[i];
				CreatePrefabPool(it.Prefab,it.PreloadedItems,it.name);
			}
		}
		// Fill Dictionary

		//foreach(KeyValuePair<string,Prefab_Pool> KVP in Pools)
		//	print(KVP.Key);

	}

	/// <summary>
	/// Initializer of the Dictionaries for the pools
	/// </summary>
	private void CreatePools()
	{
		if(Pools == null)
			Pools = new Dictionary<string, Prefab_Pool>();
		if(m_PoolsPrefabs == null)
			m_PoolsPrefabs = new List<EntityObject>();
	}

	/// <summary>
	/// Creates a prefab pool and save the parent GameObject reference and the component into <see cref="Pools"/>.
	/// </summary>
	/// <param name="prefab">Prefab For the Pool to use.</param>
	/// <param name="amount">Amount of Preloaded Instances.</param>
	/// <param name="name">Name (optional) if not set, the name will be the prefab name.</param>
	public void CreatePrefabPool(GameObject prefab,int amount,string name)
	{
		CreatePools();
		if(string.IsNullOrEmpty(name))
		{
			Debug.Log("The pool name is empty or null");
			return;
		}
		if(Pools.ContainsKey(name))
		{
			Debug.Log("The pool name: "+name+" already exist");
			return;
		}
		if(name == null || name == string.Empty)
		{
			name = "Pool_"+prefab.name;
		}
		Prefab_Pool newpool = new Prefab_Pool("Pool_"+name,prefab,amount);
		Pools.Add(name,newpool);
		newpool.GetPoolParent().transform.parent = this.transform;
		//m_PoolsPrefabs.Add(newpool.GetPoolParent());
	}

	/// <summary>
	/// Despawns all the pools.
	/// </summary>
	public void DespawnAllPools()
	{
		foreach(KeyValuePair<string,Prefab_Pool> KVP in Pools)
		{
			Pools[KVP.Key].DespawnAll();
		}
	}



	public void AddEntity(GameObject obj)
	{
		GameEntity entity = obj.GetComponent<GameEntity>();
		if(entity == null)
		{
			Debug.LogWarning("Entity __"+obj.name+"__ doesnt have GameEntity class, Adding one");
			obj.AddComponent<GameEntity>();
			entity = obj.GetComponent<GameEntity>();
			if(entity == null)
				return;
		}

		int id = entity.mI_EntityGroupID;
		if(mD_Entities.ContainsKey(id))
		{
			List<GameEntity> _list = mD_Entities[id];
			_list.Add(entity);
			mD_Entities[id] = _list;
		}
		else
		{
			List<GameEntity> _list = new List<GameEntity>();
			_list.Add(entity);
			mD_Entities.Add(id,_list);
		}
	}



	/// <summary>
	/// Dispatchs a message to a complete set of receivers .
	/// </summary>
	/// <param name="delay">Delay.</param>
	/// <param name="sender">Sender.</param>
	/// <param name="receivers">Receivers.</param>
	/// <param name="message">Message.</param>
	public void DispatchMessage(float delay, GameEntity sender, int receivers, int message)
	{
		MSG_INFO newMSG = new MSG_INFO();

		newMSG.delay = delay;
		newMSG.timeSinceInvoked = Time.time;
		newMSG.receivers = receivers;
		newMSG.sender = sender;
		newMSG.msg = message;

		if(IsInvoking("DispatchedMesasge"))
		{
			ArrangeQueue(newMSG);
		}

		Invoke("DispatchedMesasge",delay);
	}

	private void DispatchedMesasge()
	{
		MSG_INFO MSG = mL_MSG_QUEUE[0];

		if(mD_Entities.ContainsKey(MSG.receivers))
		{
			foreach(GameEntity data in mD_Entities[MSG.receivers])
				if(data.isActive)
					data.ReceiveMessage(MSG.msg,MSG.sender);
		}
		else
		{
			Debug.LogError("ERROR: Receivers _"+MSG.receivers+"_ doesnt exist in mD_Entities");
		}
		mL_MSG_QUEUE.RemoveAt(0);
	}

	void ArrangeQueue(MSG_INFO newMSG)
	{
		mL_MSG_QUEUE.Add(newMSG);
		float nextDispatchedMSG = 10000000f;
		MSG_INFO nextMSG;

		int c = 0;

		List<MSG_INFO> tempList = new List<MSG_INFO>();

		while(true)
		{
			if(c == mL_MSG_QUEUE.Count)
				break;
			nextMSG = mL_MSG_QUEUE[c];
			for(int it = c;it < mL_MSG_QUEUE.Count ; it++)
			{
				float delta = mL_MSG_QUEUE[it].delay - (Time.time - mL_MSG_QUEUE[it].timeSinceInvoked);
				if(delta < nextDispatchedMSG)
				{
					nextMSG = mL_MSG_QUEUE[it];
				}
			}
			tempList.Add(nextMSG);
			c++;
		}
		mL_MSG_QUEUE = tempList;

	}

}

[System.Serializable]
public class EntityObject
{
	
	public string name = string.Empty;
	public GameObject Prefab = null;
	public int PreloadedItems = 1;
}

public class Prefab_Pool : Pool_Base<GameObject> {
	
	/// <summary>
	/// The Prefab Object reference used for this pool
	/// </summary>
	protected GameObject prefab;
	/// <summary>
	/// The parent GameObject of the pool
	/// </summary>
	protected GameObject parent;
	
	/// <summary>
	/// Gets the GameObject Prefab used for the pool
	/// </summary>
	public GameObject GetPrefab()
	{
		return prefab;
	}
	
	public GameObject GetPoolParent()
	{
		return parent;
	}
	
	/// <summary>
	/// Deletes all pool objects.
	/// </summary>
	public override void DelteAllInternalObjects()
	{
		base.DelteAllInternalObjects();
		foreach(Object data in _spawned)
		{
			Object.Destroy(data);
		}
		_spawned.Clear();
		foreach(Object data in _despawend)
		{
			Object.Destroy(data);
		}
		_despawend.Clear();
	}
	
	/// <summary>
	/// Initializes a new instance of the <see cref="PrefabPool"/> class.
	/// </summary>
	/// <param name="Prefab">Prefab to use.</param>
	/// <param name="amount">Preload Prefab Amount.</param>
	public Prefab_Pool(string name,GameObject Prefab, int amount) 
	{
		parent = new GameObject(name);
		//parent = Parent;
		prefab = Prefab;
		base.CreatePoolBase(amount);
	}
	
	/// <summary>
	/// Spawn in the specified pos and rot.
	/// </summary>
	/// <param name="pos">Spawn Position.</param>
	/// <param name="rot">Spawn Rotation.</param>
	public GameObject Spawn(Vector3 pos, Quaternion rot) 
	{
		GameObject spawned = base.Spawn();
		spawned.transform.position = pos;
		spawned.transform.rotation = rot;
		return spawned;
	}
	
	/// <summary>
	/// Event occured just before the Spawn fucntion. Allow to perform actiones before returning the spawned object.
	/// </summary>
	/// <param name="clone">Spawned object.</param>
	protected override void OnSpawned (GameObject clone)
	{
		base.OnSpawned (clone);
		clone.SetActive(true);
	}
	
	/// <summary>
	/// Event occured just before the Despawn fucntion. Allow to perform actiones before desspawning the object.
	/// </summary>
	/// <param name="clone">Clone.</param>
	protected override void OnDespawned (GameObject clone)
	{
		base.OnDespawned (clone);
		clone.SetActive(false);
	}
	
	/// <summary>
	/// Allow to create the specific objetct type Object for the pool to manage.
	/// </summary>
	/// <returns>The instance.</returns>
	protected override GameObject CreateInstance ()
	{
		GameObject t = base.CreateInstance ();
		t = UnityEngine.GameObject.Instantiate(prefab) as GameObject;
		t.SetActive(false);
		t.transform.parent = parent.transform;
		t.name = t.name+"_"+(_despawend.Count+_spawned.Count);
		return t;
	}
	
	public void CreateNewInstance()
	{
		GameObject t = CreateInstance();
		_despawend.Add(t);
		OnDespawned(t);
	}
	
	public void PausePrefabPoolObjects(bool pause)
	{
		for(int i=0;i<_spawned.Count;i++)
		{
			//(_spawned[i] as Transform).enabled=true;
			if(_spawned[i].GetComponent<Rigidbody2D>() != null)
				_spawned[i].GetComponent<Rigidbody2D>().velocity=Vector2.zero;
		}
	}
	
}


public class Pool_Base<T> {
	
	/// <summary>
	/// The _spawned T objects type list.
	/// </summary>
	protected List<T> _spawned;
	/// <summary>
	/// The _despawend T objects type list.
	/// </summary>
	protected List<T> _despawend;
	/// <summary>
	/// The amount of instances to preload.
	/// </summary>
	protected int m_iAmountInstances;
	
	/// <summary>
	/// Deletes all pool objects.
	/// </summary>
	public virtual void DelteAllInternalObjects(){}
	
	/// <summary>
	/// Creates the pool base.
	/// </summary>
	/// <param name="Amount">Amount to preload.</param>
	protected void CreatePoolBase(int Amount = 0)
	{
		if(m_iAmountInstances<0)
			throw new UnityException("Pool size invalid");
		_spawned = new List<T>();
		_despawend = new List<T>();
		m_iAmountInstances = Amount;
		Populate();
	}
	
	/// <summary>
	/// Populate the pool with the number of instances preloaded.
	/// </summary>
	private void Populate()
	{
		return;
		for(int i=0;i<m_iAmountInstances;i++)
		{
			T clone = CreateInstance();
			_despawend.Add(clone);
		}
	}
	
	/// <summary>
	/// Returns the T object type available or creates a new one.
	/// </summary>
	public T Spawn()
	{
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			if(_despawend.Count != 0)
			{
				//The pool contains at least one objects to spawn.
				T toSpawn = _despawend[0];
				_spawned.Add(toSpawn);
				_despawend.RemoveAt(0);
				OnSpawned(toSpawn);
				return toSpawn;
			}
			else
			{
				//The pool creates a new object to handle.
				T NewClone = CreateInstance();

				// Add entity
				EntityManager.GetInstance().AddEntity(NewClone as GameObject);

				_spawned.Add(NewClone);
				OnSpawned(NewClone);
				return NewClone;
			}
		}
	}
	
	/// <summary>
	/// Despawn the T object type from the lists.
	/// </summary>
	/// <param name="instance">Instance to Despawn.</param>
	public void Despawn(T instance)
	{
		if(instance == null)
			return;
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			if(_spawned.Contains(instance))
			{
				//The object exists in the pool and proceed to Despawn.
				_spawned.Remove(instance);
				_despawend.Add(instance);
				OnDespawned(instance);
			}
		}
	}
	
	/// <summary>
	/// Despawns all the objects of the selected pool.
	/// </summary>
	public void DespawnAll()
	{
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			for(int i=0;i<_spawned.Count;i++)
			{
				T toDespawn = _spawned[i];
				_despawend.Add(toDespawn);
				OnDespawned(toDespawn);
			}
			_spawned.Clear();
		}
	}
	
	/// <summary>
	/// Special override function that allow to create the specific objetct type T for the pool to manage.
	/// </summary>
	/// <returns>The instance.</returns>
	protected virtual T CreateInstance()
	{
		return default(T);
	}
	
	/// <summary>
	/// Event occured just before the Spawn fucntion. Allow to perform actiones before returning the spawned object.
	/// </summary>
	/// <param name="clone">Spawned object.</param>
	protected virtual void OnSpawned(T clone) { }
	
	/// <summary>
	/// Event occured just before the Despawn fucntion. Allow to perform actiones before desspawning the object.
	/// </summary>
	protected virtual void OnDespawned(T clone) { }
	
}

