﻿using UnityEngine;
using System.Collections;

public enum WINDOWSTYPES
{
	MODAL = 0,
	MAIN_MENU,
	PICK_CHARACTER,
	GAME_OVER,
	OPTIONS,
	UNLOCKED,
	SHOP,
	TUTORIAL
}

public enum SCENES
{
	INIT = 0,
	MAIN_MENU = 1,
	LOADING = 2,
	GAME = 3,
	EDITOR = 4
}

public enum MOVEMENT
{
	UP = 0,
	RIGHT = 1,
	DOWN = 2,
	LEFT = 3
}

public enum KILL
{
	NONE,
	FIRE,
	SHOCK,
	HIT
}

public enum DIRECTION
{
	NORTH =0,
	EAST  =1,
	SOUTH =2,
	WEST  =3
}

public enum StateTween
{
	ENTER,
	EXIT
}

public enum AXIS
{
	HORIZONTAL = 0,
	VERTICAL   = 1
}

public enum PROMOTION_TYPE
{
	NONE,
	GIFT,
	UNLOCK,
	WATCH,
	RATE
}

namespace GlobalData
{

}

public class CONSTANTS 
{
#if UNITY_IOS
	public static string ONE_CHAR 			= "snowtimeONECHAR";
	public static string POWER_PACK 		= "snowtimePOWPACK";
	public static string NO_ADS 			= "snowtimeNOADS";
	public static string ALL_CHARS 			= "snowtimeALLCHARS";
	public static string ALL_CHARS_NO_ADS 	= "snowtimeALLCHARSNOADS";
#else
	public static string ONE_CHAR 			= "snowtimeonechar";
	public static string POWER_PACK 		= "snowtimepowpack";
	public static string NO_ADS 			= "snowtimenoads";
	public static string ALL_CHARS 			= "snowtimeallchars";
	public static string ALL_CHARS_NO_ADS 	= "snowtimeallcharsnoads";
#endif
	public static int MusicSources = 2;
	public static int SFX_Sources  = 8;

	public static int UNLOCK_MONEY = 500;

	public static int MINUTES_SURPRISE = 10;//10
	public static int MINUTES_REWARD = 15;// 15

	public static int VIDEO_REWARD = 200;

	public static string PlayerProgress = "";
	public static string CharactersProgress = "{\"Alexander\":\"False\",\"Sophia\":\"False\",\"Ava\":\"False\",\"Mason\":\"False\",\"Isabella\":\"False\",\"Liam\":\"False\",\"William\":\"False\",\"Snowman\":\"False\",\"Michael\":\"False\",\"Olivia\":\"True\",\"Emma\":\"False\",\"Jacob\":\"False\",\"Ethan\":\"True\",\"Rockey\":\"False\",\"Noah\":\"False\"}";

	public static float VerticalTransitionSpeed = 0.25f;
	public static float HorizontalTransitionSped = 0.15f;

	public static Color BlurredWhite = new Color(1f,1f,1f,0.4f);
	public static Color EnterWhite = new Color(1f,1f,1f,0.5f);//0.7

	public static string[] CHAR_UNLOCK_KEYS = new string[15] {"Ethan","Olivia","Noah","Isabella","Alexander",
														"Liam","Jacob","Sophia","Mason","Ava",
														"Michael","Emma","William","Rockey",
														"Snowman" };

  //public static int[] CHARACTER_PRICES = new int[16] {0, 250, 250, 500, 500, 500, 500, 1000,1000,1000,1000,1000,1500,1500,1500,1500};
	//public static int[] CHARACTER_PRICES = new int[17] {0, 0, 250, 250, 500, 500, 500, 500, 1000,1000,1000,1000,1500,1500,1500,1500,1500};
}
