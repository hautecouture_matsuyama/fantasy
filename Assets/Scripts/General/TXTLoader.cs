﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TXTLoader : MonoBehaviour {


	// Use this for initialization
	void Start () {

		/*path=Directory.GetCurrentDirectory();
		path+="\\Assets\\Resources\\Level.txt";
		File.CreateText(path);*/
	}
	
	static public string LoadTxt(string fileName)
	{
		TextAsset dataLevel = Resources.Load(fileName) as TextAsset;
		if(dataLevel == null)
			return string.Empty;
		return dataLevel.text;
	}

	static public string LoadAudioTXT(string fileName)
	{
		TextAsset dataLevel = Resources.Load("Audio/"+fileName) as TextAsset;
		if(dataLevel == null)
			return string.Empty;
		return dataLevel.text;
	}

	static public string LoadRelativeTxt(string fileName,string subPath)
	{
		TextAsset text = new TextAsset();
		Object[] allMaps = Resources.LoadAll ("MapLevels");
		if (allMaps == null)
			return null;

		foreach ( Object node in allMaps)
		{
			if(node.name.ToString() == fileName)
			{
				text = node as TextAsset;
				return text.text;
			}
		}
		return null;
	}

	static public  void saveTxt(string fileName,string toSave)
	{
		string path;
		path=Directory.GetCurrentDirectory();
		path+="/Assets/Resources/"+fileName+".txt";
		using(StreamWriter sw = File.CreateText(path))
		{
			sw.WriteLine(toSave);
		}
	}

	static public  void saveRelativeTxt(string subpath, string fileName,string toSave)
	{
		string path;
		path=Directory.GetCurrentDirectory();

		if(subpath != null)
			path+="/Assets/Resources/"+subpath+fileName+".txt";
		else
			path+="/Assets/Resources/"+fileName+".txt";

		using(StreamWriter sw = File.CreateText(path))
		{
			sw.WriteLine(toSave);
		}
	}

	static public bool Exists(string toFind)
	{
		TextAsset dataLevel = Resources.Load(toFind) as TextAsset;
		if(dataLevel != null)
		{
			return true;
		}
		return false;
	}

	static public bool ExistsLevelMap(string toFind)
	{
		Object[] allMaps = Resources.LoadAll ("MapLevels");
		if (allMaps == null)
			return false;
		foreach ( Object node in allMaps)
		{
			if(node.name.ToString() == toFind)
			{
				return true;
			}
		}
		return false;
	}
}
