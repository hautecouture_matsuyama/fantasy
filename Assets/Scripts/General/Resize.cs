﻿using UnityEngine;
using System.Collections;

public class Resize : MonoBehaviour {

	public enum Aspects { StrechtMaximum_16_9= 0 };

	public Aspects aspect;
	public float OrthographicFinalSize = 0.85f;

	public void ResizeBackground(Aspects streacthMax)
	{
		//16:9 = 257 height
		//4:3 = 343 height
		//3:2 = 305 height
		/*
		float Strecht = 0.0f;
		switch(streacthMax)
		{
		case Aspects.StrechtMaximum_16_9:
			Strecht = 257;
			//OrthographicFinalSize= 0.85f;
			break;
		default:
			Strecht = 257;
			break;
		}
		*/

		Camera.main.transform.position = new Vector3(1f,1f,-10f);
		this.transform.position = Vector3.zero;

		SpriteRenderer spr = GetComponent<SpriteRenderer>();
		//float width = spr.bounds.size.x;
		float height = spr.bounds.size.y;

		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		//float worldScreenWidth = worldScreenHeight / 257 * Screen.width;
		float BackgroundSize = (worldScreenHeight / height);
		this.gameObject.transform.localScale = new Vector3(BackgroundSize,BackgroundSize,0);
		
		if (Camera.main.aspect >= 1.7)
		{
			Debug.Log("Aspect: 16:9");
			Camera.main.orthographicSize *= OrthographicFinalSize;
			//scale = 0.85f;
		}
		else if (Camera.main.aspect >= 1.5)
		{
			Debug.Log("Aspect: 3:2");
			Camera.main.orthographicSize *= OrthographicFinalSize;
			//scale = 0.85f;
		}
		else
		{
			Debug.Log("Aspect: 4:3");
		}
		
		Vector3 pos = new Vector3(
			-Camera.main.orthographicSize * Camera.main.aspect,
			Camera.main.orthographicSize,
			Camera.main.transform.position.z);
		Camera.main.gameObject.transform.position = pos;
	}
}
