﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Facebook.MiniJSON;
using System;
using System.IO;
using MiniJSON;

public class JSONDefinitionManager : MonoBehaviour
{
	static public void LoadAudioClipsJSON(string AudioJson, out Dictionary<string, AudioClip> d_music,
	                                      out Dictionary<string, AudioClip> d_sound,
	                                      out Dictionary<string,string> d_keys)
	{
		Dictionary<string, AudioClip> music = new Dictionary<string, AudioClip>();
		Dictionary<string, AudioClip> sound = new Dictionary<string, AudioClip>();
		
		Dictionary<string, string>    keys  = new Dictionary<string, string>();
		
		string Text = TXTLoader.LoadAudioTXT(AudioJson);
		Dictionary<string, object> dic = new Dictionary<string, object>();
		if(Text != string.Empty)
		{
			dic = (Dictionary<string, object>)Json.Deserialize(Text);
		}
		
		object clip = new object();
		Dictionary<string, object> dicFromPrefs = dic;
		//Add Music
		if (dicFromPrefs.TryGetValue("music", out clip))
		{
			Dictionary<string, object> tempDic = clip as Dictionary<string, object>;
			
			foreach (KeyValuePair<string, object> KVP in tempDic)
			{
				string k2 = KVP.Value.ToString();
				music.Add(KVP.Key.ToString(), (AudioClip)Resources.Load("Audio/Music/"+k2));
				keys.Add(KVP.Key.ToString(),k2);
			}
		}
		//Add Sound
		if (dicFromPrefs.TryGetValue("sound", out clip))
		{
			Dictionary<string, object> tempDic = clip as Dictionary<string, object>;
			foreach (KeyValuePair<string, object> KVP in tempDic)
			{
				string k2 = KVP.Value.ToString();
				sound.Add(KVP.Key.ToString(), (AudioClip)Resources.Load("Audio/SFX/"+k2));
				keys.Add(KVP.Key.ToString(),k2);
			}
		}
		d_music = music;
		d_sound = sound;
		d_keys  = keys;
	}

	static public void LoadGameDialogues(string jsonName, out Dictionary<string,object> dialogues)
	{
		string Text = TXTLoader.LoadAudioTXT(jsonName);
		if(string.IsNullOrEmpty(Text))
		{
			dialogues=null;
			return;
		}
		dialogues = (Dictionary<string, object>)Json.Deserialize(Text);
	}
}
