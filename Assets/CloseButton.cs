﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CloseButton : MonoBehaviour {

    public GameObject RankingCanvas;

    public void CloseRanking()
    {
        RankingCanvas.SetActive(false);

    }
}
