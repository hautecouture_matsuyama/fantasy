﻿using UnityEngine;
using System.Collections;

public class Ingame_UI : MonoBehaviour {

    [SerializeField]
    private GameObject GameCanvas;

    void OnEnable()
    {
        GameCanvas.SetActive(true);
    }

    void OnDisable()
    {
        GameCanvas.SetActive(false);
    }


}
