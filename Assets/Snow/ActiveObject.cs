﻿using UnityEngine;
using System.Collections;

public class ActiveObject : MonoBehaviour {

    [SerializeField]
    private GameObject Object;

    public void Activate()
    {
        Object.SetActive(true);
    }

}
