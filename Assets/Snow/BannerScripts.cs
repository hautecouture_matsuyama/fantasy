﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BannerScripts : MonoBehaviour{

    public static BannerScripts Instance;

    [SerializeField]
    private RectTransform banner;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }


    public void HideBanner()
    {
        banner.anchoredPosition = new Vector2(1, 150);
    }

    public void ShowBanner()
    {
        banner.anchoredPosition = new Vector2(1, -76);
    }


}
