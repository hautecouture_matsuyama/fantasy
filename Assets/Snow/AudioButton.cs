﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioButton : MonoBehaviour {

    [SerializeField]
    private Image buttonImage;

    [SerializeField]
    private Sprite sound_on;

    [SerializeField]
    private Sprite sound_off;


    public void OnClick()
    {
        if(AudioListener.volume == 1)
        {
            AudioListener.volume = 0;
            buttonImage.sprite = sound_off;
        }
        else
        {
            AudioListener.volume = 1;
            buttonImage.sprite = sound_on;
        }


    }

}
